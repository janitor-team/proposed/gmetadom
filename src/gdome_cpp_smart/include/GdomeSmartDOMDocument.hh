
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMDocument_hh__
#define __GdomeSmartDOMDocument_hh__

#include "GdomeSmartDOMNode.hh"

namespace GdomeSmartDOM {

class Document : public Node
{
protected:
  explicit Document(GdomeDocument* obj, bool) : Node((GdomeNode*) obj, true) { }
public:
  explicit Document(GdomeDocument* = 0);
    Document(const Document&);
  Document(const Node&);
  ~Document();
Document& operator=(const Document&);
  bool operator==(const Document& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const Document& obj) const { return !(*this == obj); }
  
  // Attributes  
  class DocumentType get_doctype(void) const;
  class DOMImplementation get_implementation(void) const;
  class Element get_documentElement(void) const;
  
  // Methods
  class Element createElement(const class GdomeString& tagName) const;
  class DocumentFragment createDocumentFragment(void) const;
  class Text createTextNode(const class GdomeString& data) const;
  class Comment createComment(const class GdomeString& data) const;
  class CDATASection createCDATASection(const class GdomeString& data) const;
  class ProcessingInstruction createProcessingInstruction(const class GdomeString& target, const class GdomeString& data) const;
  class Attr createAttribute(const class GdomeString& name) const;
  class EntityReference createEntityReference(const class GdomeString& name) const;
  class NodeList getElementsByTagName(const class GdomeString& tagname) const;
  class Node importNode(const class Node& importedNode, const bool deep) const;
  class Element createElementNS(const class GdomeString& namespaceURI, const class GdomeString& qualifiedName) const;
  class Attr createAttributeNS(const class GdomeString& namespaceURI, const class GdomeString& qualifiedName) const;
  class NodeList getElementsByTagNameNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  class Element getElementById(const class GdomeString& elementId) const;
  
  // Friend classes
  friend class Node;
  friend class DOMImplementation;
};

}

#endif // __GdomeSmartDOMDocument_hh__

