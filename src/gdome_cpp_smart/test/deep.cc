/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <config.h>

#include <gdome.h>
#include <cstdlib>
#include <iostream>

#include "GdomeSmartDOM.hh"

using namespace GdomeSmartDOM;
using namespace std;

class Handler : public EventListener
{
public:
  void handleEvent(const Event& ev);
};

void
Handler::handleEvent(const Event& ev)
{
  //cout << "an event occurred" << endl;
}

void
doTest(int m, const char* name)
{
  cout << "testing " << m << " modifications on " << name << endl;
  try {
    DOMImplementation di;
    Document doc = di.createDocumentFromURI(name, 0);
    di.disableEvent(doc, "*");
    
    Element p = doc.get_documentElement();
    for (int j = 0; j < 250; j++) {
      Element el = doc.createElement("deep");
      p.appendChild(el);
      p = el;
    }

    EventTarget et(p);
    et.addEventListener("DOMSubtreeModified", *new Handler, false);

    for (int j = 0; j < m; j++)
      {
	p.setAttribute("my-own-attribute", "my-own-value");
      }
  } catch (DOMException exc) {
    cerr << "exc: " << exc.code << " " << exc.msg << endl;
  }
}

int
main(int argc, char* argv[])
{
  if (argc < 3) {
    cerr << "usage: test <n> <file>" << endl;
    return 1;
  }

  doTest(atoi(argv[1]), argv[2]);
}

