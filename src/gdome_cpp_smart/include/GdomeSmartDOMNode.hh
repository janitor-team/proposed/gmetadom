
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMNode_hh__
#define __GdomeSmartDOMNode_hh__

namespace GdomeSmartDOM {

class Node
{
protected:
  explicit Node(GdomeNode* obj, bool) : gdome_obj(obj) { }
public:
  explicit Node(GdomeNode* = 0);
    Node(const Node&);
  ~Node();

  Node(const class EventTarget&);
Node& operator=(const Node&);
  bool operator==(const Node& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const Node& obj) const { return !(*this == obj); }
  
  operator GdomeNode*() const { return reinterpret_cast<GdomeNode*>(gdome_obj); }
  enum NodeType {
    ELEMENT_NODE = 1,
    ATTRIBUTE_NODE = 2,
    TEXT_NODE = 3,
    CDATA_SECTION_NODE = 4,
    ENTITY_REFERENCE_NODE = 5,
    ENTITY_NODE = 6,
    PROCESSING_INSTRUCTION_NODE = 7,
    COMMENT_NODE = 8,
    DOCUMENT_NODE = 9,
    DOCUMENT_TYPE_NODE = 10,
    DOCUMENT_FRAGMENT_NODE = 11,
    NOTATION_NODE = 12
  };

  // Attributes  
  class GdomeString get_nodeName(void) const;
  class GdomeString get_nodeValue(void) const;
  void set_nodeValue(const class GdomeString&) const;
  unsigned short get_nodeType(void) const;
  class Node get_parentNode(void) const;
  class NodeList get_childNodes(void) const;
  class Node get_firstChild(void) const;
  class Node get_lastChild(void) const;
  class Node get_previousSibling(void) const;
  class Node get_nextSibling(void) const;
  class NamedNodeMap get_attributes(void) const;
  class Document get_ownerDocument(void) const;
  class GdomeString get_namespaceURI(void) const;
  class GdomeString get_prefix(void) const;
  void set_prefix(const class GdomeString&) const;
  class GdomeString get_localName(void) const;
  
  // Methods
  class Node insertBefore(const class Node& newChild, const class Node& refChild) const;
  class Node replaceChild(const class Node& newChild, const class Node& oldChild) const;
  class Node removeChild(const class Node& oldChild) const;
  class Node appendChild(const class Node& newChild) const;
  bool hasChildNodes(void) const;
  class Node cloneNode(const bool deep) const;
  void normalize(void) const;
  bool isSupported(const class GdomeString& feature, const class GdomeString& version) const;
  bool hasAttributes(void) const;
  
  // Friend classes
  friend class Document;
  friend class NodeList;
  friend class CharacterData;
  friend class Attr;
  friend class Element;
  friend class Text;
  friend class Comment;
  friend class CDATASection;
  friend class DocumentType;
  friend class Notation;
  friend class ProcessingInstruction;
  friend class Entity;
  friend class EntityReference;
  friend class DocumentFragment;
  friend class NamedNodeMap;
  friend class EventTarget;
  friend class MutationEvent;
  friend class DOMImplementation;
      GdomeNode* gdome_object(void) const;
    void* id(void) const;
protected:
  GdomeNode* gdome_obj;
};

}

#endif // __GdomeSmartDOMNode_hh__

