/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <gdome-events.h>

#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMEventListener.hh"
#include "GdomeSmartDOMMutationEvent.hh"
#include "GdomeSmartDOMHelper.hh"

void my_gdome_event_listener_callback(GdomeEventListener* evntl, GdomeEvent* evnt, GdomeException* exc)
{
  g_return_if_fail(evntl != NULL);
  g_return_if_fail(evnt != NULL);
  g_return_if_fail(exc != NULL);

  *exc = 0;

  GdomeSmartDOM::EventListener* listener = reinterpret_cast<GdomeSmartDOM::EventListener*>(gdome_evntl_get_priv(evntl));
  g_return_if_fail(listener != NULL);

  try {
    GdomeMutationEvent* mevnt = gdome_cast_mevnt(evnt);
    g_return_if_fail(mevnt != NULL);
    listener->handleEvent(GdomeSmartDOM::MutationEvent(mevnt));
  } catch (GdomeSmartDOM::DOMException exc_) {
    *exc = exc_.code;
  }
}


