
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_t_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeText* obj_ = Text_val(v);
  g_assert(obj_ != NULL);
  gdome_t_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_t_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeText* obj1_ = Text_val(v1);
  GdomeText* obj2_ = Text_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeText*
Text_val(value v)
{
  GdomeText* res_ = *((GdomeText**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_Text(GdomeText* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/Text",
    ml_gdome_t_finalize,
    ml_gdome_t_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeText*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeText**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_t_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeText* obj_;
  
  obj_ = gdome_cast_t((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("Text");
  gdome_t_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "Text casting from Node");
  CAMLreturn(Val_Text(obj_));
}

    
value
ml_gdome_t_splitText(value self, value p_offset)
{
  CAMLparam2(self, p_offset);
  
  GdomeException exc_;
  GdomeText* res_;
  res_ = gdome_t_splitText(Text_val(self), Int_val(p_offset), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Text.splitText");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Text(res_));
}

