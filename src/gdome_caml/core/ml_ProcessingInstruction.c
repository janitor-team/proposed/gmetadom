
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_pi_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeProcessingInstruction* obj_ = ProcessingInstruction_val(v);
  g_assert(obj_ != NULL);
  gdome_pi_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_pi_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeProcessingInstruction* obj1_ = ProcessingInstruction_val(v1);
  GdomeProcessingInstruction* obj2_ = ProcessingInstruction_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeProcessingInstruction*
ProcessingInstruction_val(value v)
{
  GdomeProcessingInstruction* res_ = *((GdomeProcessingInstruction**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_ProcessingInstruction(GdomeProcessingInstruction* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/ProcessingInstruction",
    ml_gdome_pi_finalize,
    ml_gdome_pi_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeProcessingInstruction*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeProcessingInstruction**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_pi_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeProcessingInstruction* obj_;
  
  obj_ = gdome_cast_pi((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("ProcessingInstruction");
  gdome_pi_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "ProcessingInstruction casting from Node");
  CAMLreturn(Val_ProcessingInstruction(obj_));
}

    
value
ml_gdome_pi_get_target(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_pi_target(ProcessingInstruction_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "ProcessingInstruction.get_target");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_pi_get_data(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_pi_data(ProcessingInstruction_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "ProcessingInstruction.get_data");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}

value
ml_gdome_pi_set_data(value self, value p_v)
{
  CAMLparam2(self, p_v);
  GdomeException exc_;
  
  GdomeDOMString* p_v_ = DOMString_val(p_v);
  gdome_pi_set_data(ProcessingInstruction_val(self), p_v_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "ProcessingInstruction.set_data");
  CAMLreturn(Val_unit);
}

