
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <gdome-events.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_n_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeEventTarget* obj_ = EventTarget_val(v);
  g_assert(obj_ != NULL);
  gdome_n_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_n_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeEventTarget* obj1_ = EventTarget_val(v1);
  GdomeEventTarget* obj2_ = EventTarget_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeEventTarget*
EventTarget_val(value v)
{
  GdomeEventTarget* res_ = *((GdomeEventTarget**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_EventTarget(GdomeEventTarget* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Events/EventTarget",
    ml_gdome_n_finalize,
    ml_gdome_n_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeEventTarget*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeEventTarget**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_n_addEventListener(value self, value p_type, value p_listener, value p_useCapture)
{
  CAMLparam4(self, p_type, p_listener, p_useCapture);
  
  GdomeException exc_;
  
  GdomeDOMString* p_type_ = DOMString_val(p_type);gdome_n_addEventListener(EventTarget_val(self), p_type_, EventListener_val(p_listener), Bool_val(p_useCapture), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "EventTarget.addEventListener");
  CAMLreturn(Val_unit);
}


value
ml_gdome_n_removeEventListener(value self, value p_type, value p_listener, value p_useCapture)
{
  CAMLparam4(self, p_type, p_listener, p_useCapture);
  
  GdomeException exc_;
  
  GdomeDOMString* p_type_ = DOMString_val(p_type);gdome_n_removeEventListener(EventTarget_val(self), p_type_, EventListener_val(p_listener), Bool_val(p_useCapture), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "EventTarget.removeEventListener");
  CAMLreturn(Val_unit);
}


value
ml_gdome_n_dispatchEvent(value self, value p_evt)
{
  CAMLparam2(self, p_evt);
  
  GdomeException exc_;
  GdomeBoolean res_;
  res_ = gdome_n_dispatchEvent(EventTarget_val(self), Event_val(p_evt), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "EventTarget.dispatchEvent");
  CAMLreturn(Val_bool(res_));
}

