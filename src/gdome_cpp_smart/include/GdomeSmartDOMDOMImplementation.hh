/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMDOMImplementation_hh__
#define __GdomeSmartDOMDOMImplementation_hh__

#include <gdome.h>

namespace GdomeSmartDOM {

class DOMImplementation
{
public:
  DOMImplementation(void);
  DOMImplementation(const DOMImplementation&);
  ~DOMImplementation();

  DOMImplementation& operator=(const DOMImplementation&);

  bool hasFeature(const class GdomeString&, const class GdomeString&);
  class DocumentType createDocumentType(const class GdomeString&, const class GdomeString&, const class GdomeString&);
  class Document createDocument(const class GdomeString&, const class GdomeString&, const class DocumentType&);
  class Document createDocumentFromURI(const char*, unsigned long = GDOME_LOAD_PARSING);
  class Document createDocumentFromMemory(const char*, unsigned long = GDOME_LOAD_PARSING);
  bool           saveDocumentToFile(const class Document&, const char*, unsigned long = GDOME_SAVE_STANDARD);
  bool           saveDocumentToMemory(const class Document&, class GdomeString&, unsigned long = GDOME_SAVE_STANDARD);
  void           enableEvent(const class Document&, const char*);
  void           disableEvent(const class Document&, const char*);
  bool           eventIsEnabled(const class Document&, const char*);

  GdomeDOMImplementation* gdome_object(void) const;

  friend class Document;

protected:
  DOMImplementation(GdomeDOMImplementation* di, bool) : gdome_obj(di) { }
  DOMImplementation(GdomeDOMImplementation*);
  GdomeDOMImplementation* gdome_obj;
};

}

#endif // __GdomeSmartDOMImplementation_hh__

