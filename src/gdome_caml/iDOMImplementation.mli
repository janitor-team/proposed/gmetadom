(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *)

external create : unit -> TDOMImplementation.t = "ml_gdome_di_create"

external hasFeature : this:TDOMImplementation.t -> feature:TDOMString.t -> version:TDOMString.t -> bool = "ml_gdome_di_hasFeature"

external createDocumentType : this:TDOMImplementation.t -> qualifiedName:TDOMString.t -> publicId:TDOMString.t -> systemId:TDOMString.t -> TDocumentType.t = "ml_gdome_di_createDocumentType"

external createDocument : this:TDOMImplementation.t -> namespaceURI:TDOMString.t option -> qualifiedName:TDOMString.t -> doctype:[> `DocumentType] GdomeT.t option -> TDocument.t = "ml_gdome_di_createDocument"

type validatingMode =
   Parsing
 | Validating
 | Recovering
;;

val createDocumentFromURI :
  ?validatingMode:validatingMode ->
  ?keepEntities:bool ->
  unit -> this:TDOMImplementation.t -> uri:string -> TDocument.t

val createDocumentFromMemory :
  ?validatingMode:validatingMode ->
  ?keepEntities:bool ->
  unit -> this:TDOMImplementation.t -> doc:TDOMString.t -> TDocument.t
  
val saveDocumentToFile :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t -> name:string ->
  ?indent:bool -> unit -> bool
  
val saveDocumentToFileEnc :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t -> name:string -> encoding:string ->
  ?indent:bool -> unit -> bool

val saveDocumentToMemory :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  ?indent:bool -> unit -> string

val saveDocumentToMemoryEnc :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t -> encoding:string ->
  ?indent:bool -> unit -> string

val enableEvent :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  name:string -> unit

val disableEvent :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  name:string -> unit

val eventIsEnabled :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  name:string -> bool

