<?xml version="1.0"?>

<!--
 This file is part of GMetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
               2001 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the project home page
 http://gmetadom.sourceforge.net
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>

<xsl:output method="text"/>
<xsl:param name="module" select="'Core'"/>
<xsl:param name="uriprefix" select="''"/>
<xsl:param name="annotations" select="''"/>

<xsl:template match="interface">
<xsl:text>(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani &lt;luca.padovani@cs.unibo.it&gt;
 *               2002 Claudio Sacerdoti Coen &lt;sacerdot@cs.unibo.it&gt;
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to &lt;luca.padovani@cs.unibo.it&gt;
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)
</xsl:text>
 <xsl:text>type t = [</xsl:text>
 <xsl:apply-templates select="." mode="list-ancestors"/>
 <xsl:text>] GdomeT.t
</xsl:text>
</xsl:template>

<xsl:template match="interface" mode="list-ancestors">
 <!-- There must be a solution better than next if -->
 <xsl:if test="@name = 'Node'">
  <xsl:text>`EventTarget | </xsl:text>
 </xsl:if>
 <xsl:if test="@inherits">
  <xsl:apply-templates mode="list-ancestors" select="document(concat($uriprefix, concat('/', concat(@inherits, '.xml'))))/interface"/>
  <xsl:text> | </xsl:text>
 </xsl:if>
 <xsl:text>`</xsl:text>
 <xsl:value-of select="@name"/>
</xsl:template>

</xsl:stylesheet>
