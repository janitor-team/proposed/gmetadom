<?xml version="1.0" encoding="UTF-8"?>

<!--
 This file is part of GMetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
               2002 Claudio Sacerdoti Coen <sacerdotcs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the project home page
 http://gmetadom.sourceforge.net
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>

<xsl:output method="html"/>
<xsl:param name="uriprefix" select="''"/>
<xsl:param name="date" select="''"/>

<xsl:template match="interface">
<html xmlns="http://www.w3.org/1999/xhtml">

<title><xsl:value-of select="@name"/></title>

<head>
<link href="gmetadom.css" rel="stylesheet" type="text/css" />
</head>

<body>

<h1 class="classname"><a class="onbackground" href="index.html">I</a>::<xsl:value-of select="@name"/></h1>

<xsl:if test="descr">
  <table border="2" cellspacing="0" cellpadding="4" width="100%">
  <tr><td class="descr">
    <xsl:apply-templates select="descr"/>
  </td></tr>
  </table>

  <br /><br />
</xsl:if>

<xsl:if test="@inherits">
  <table border="2" cellspacing="0" cellpadding="4" width="100%">
  <tr><td class="head"><em class="sectionname">Inherits from</em></td></tr>
  <tr><td class="prototype">
    <xsl:element name="a">
      <xsl:attribute name="href"><xsl:value-of select="@inherits"/>.html</xsl:attribute>
      <em class="code"><xsl:value-of select="@inherits"/></em>
    </xsl:element>
  </td></tr>
  </table>

  <br /><br />
</xsl:if>

<br /><br />

<table border="2" cellspacing="0" cellpadding="4" width="100%">
<tr><td class="head"><em class="sectionname">Cast Methods</em></td></tr>
<tr><td class="prototype">
<xsl:apply-templates select="." mode="cast">
  <xsl:with-param name="name" select="@name"/>
</xsl:apply-templates>
</td></tr>
<tr><td class="descr">
These methods can be used to cast an object to a given type.
If the downcasting would result into an unsafe operation, a <tt>DOMCastException</tt> exception is raised.
</td></tr>
</table>

<br /><br />

<xsl:if test="attribute">
<br /><br />

<table border="2" cellspacing="0" cellpadding="4" width="100%">
<tr><td class="head"><em class="sectionname">Attributes</em></td></tr>
  <xsl:apply-templates select="attribute"/>
</table>
</xsl:if>

<xsl:if test="method">
<br /><br />
<table border="2" cellspacing="0" cellpadding="4" width="100%">
<tr><td class="head"><em class="sectionname">Methods</em></td></tr>
  <xsl:apply-templates select="method"/>
</table>
</xsl:if>

<hr />
<small class="trailer">
  This file has been generated automatically
  <xsl:if test="string-length($date) &gt; 0">
  on <xsl:value-of select="$date"/>
  </xsl:if>
  as part of
  <a class="onbackground" href="http://www.cs.unibo.it/~lpadovan/gmetadom">GMetaDOM</a>.
  For more information see the home page of the project, or contact
  the main author: <a class="onbackground" href="mailto:luca.padovani@cs.unibo.it">luca.padovani@cs.unibo.it</a>.
</small>
</body>
</html>
</xsl:template>

<xsl:template match="attribute">
  <tr><td class="prototype">
  <xsl:call-template name="returnType">
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
  <em class="getattrname">get_<xsl:value-of select="@name"/></em> : <em class="typename">T<xsl:value-of select="../@name"/>.t</em> -&gt; <em class="typename"><xsl:call-template name="attrType"><xsl:with-param name="name" select="@name"/><xsl:with-param name="type" select="@type"/></xsl:call-template>
  
  <br />

  <xsl:if test="not(@readonly)"><em class="setattrname">set_<xsl:value-of select="@name"/></em> : <em class="typename">T<xsl:value-of select="../@name"/>.t</em> -&gt; <xsl:call-template name="attrType"><xsl:with-param name="name" select="@name"/><xsl:with-param name="type" select="@type"/></xsl:call-template> -&gt; <em class="typename">unit</em>

  <br />

  </xsl:if>
  </td></tr>
  <tr><td class="descr">
  <xsl:apply-templates select="descr"/>
  </td></tr>
</xsl:template>

<xsl:template match="method">
  <tr><td class="prototype">
  <xsl:call-template name="returnType"><xsl:with-param name="type" select="returns/@type"/></xsl:call-template><xsl:value-of select="' '"/><em class="methodname"><xsl:value-of select="@name"/></em>(<xsl:apply-templates select="parameters"/>) const<!--throw(<xsl:apply-templates select="raises/exception"/>)-->
  </td></tr>
  <tr><td class="descr">
  <xsl:apply-templates select="parameters" mode="descr"/>
  <b>Description:</b>
  <xsl:apply-templates select="descr"/>
  <xsl:apply-templates select="raises" mode="descr"/>
  </td></tr>
</xsl:template>

<xsl:template match="parameters">
  <xsl:choose>
    <xsl:when test="count(param) = 0"><em class="typename">void</em></xsl:when>
    <xsl:otherwise><xsl:apply-templates select="param"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="parameters" mode="descr">
  <xsl:if test="count(param) &gt; 0">
    <b>Parameters:</b>
    <dl>
      <xsl:apply-templates select="param" mode="descr"/>
    </dl>
  </xsl:if>
</xsl:template>

<xsl:template match="param">
  <xsl:if test="@attr = 'in'">const </xsl:if>
  <xsl:call-template name="paramType"><xsl:with-param name="type" select="@type"/></xsl:call-template>
  <xsl:value-of select="@name"/>
  <xsl:if test="position() &lt; last()">, </xsl:if>
</xsl:template>

<xsl:template match="param" mode="descr">
  <dt><em class="code"><xsl:value-of select="@name"/></em></dt>
  <dd><xsl:apply-templates select="descr"/></dd>
</xsl:template>

<xsl:template match="raises" mode="descr">
  <xsl:if test="count(exception) &gt; 0">
    <b>Exceptions:</b>
    <dl>
      <xsl:apply-templates select="exception" mode="descr"/>
    </dl>
  </xsl:if>
</xsl:template>

<xsl:template match="exception" mode="descr">
  <dt>
    <xsl:element name="a">
      <xsl:attribute name="href"><xsl:value-of select="@name"/>.html</xsl:attribute>
      <em class="code"><xsl:value-of select="@name"/></em>
    </xsl:element>
  </dt>
  <dd><xsl:apply-templates select="descr"/></dd>
</xsl:template>

<xsl:template name="paramType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type='void'"><em class="typename">void</em></xsl:when>
    <xsl:when test="$type='boolean'"><em class="typename">bool</em></xsl:when>
    <xsl:when test="$type='unsigned short'"><em class="typename">unsigned short</em></xsl:when>
    <xsl:when test="$type='unsigned long'"><em class="typename">unsigned long</em></xsl:when>
    <xsl:when test="$type='DOMTimeStamp'"><em class="typename">DOMTimeStamp</em></xsl:when>
    <xsl:otherwise>
      <xsl:element name="a">
        <xsl:attribute name="href"><xsl:value-of select="$type"/>.html</xsl:attribute>
	<em class="typename"><xsl:value-of select="$type"/></em>
      </xsl:element>&amp;
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="returnType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'void'"><em class="typename">void</em></xsl:when>
    <xsl:when test="$type='boolean'"><em class="typename">bool</em></xsl:when>
    <xsl:when test="$type='unsigned short'"><em class="typename">unsigned short</em></xsl:when>
    <xsl:when test="$type='unsigned long'"><em class="typename">unsigned long</em></xsl:when>
    <xsl:when test="$type='DOMTimeStamp'"><em class="typename">DOMTimeStamp</em></xsl:when>
    <xsl:otherwise>
      <xsl:element name="a">
        <xsl:attribute name="href"><xsl:value-of select="$type"/>.html</xsl:attribute>
        <em class="typename"><xsl:value-of select="$type"/></em>
      </xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="exception">
  <xsl:value-of select="@name"/>
  <xsl:if test="position() &lt; last()">, </xsl:if>
</xsl:template>

<xsl:template match="interface" mode="cast">
  <xsl:param name="name" select="''"/>
  <em class="methodname"><xsl:value-of select="$name"/></em>(const
<xsl:element name="a">
  <xsl:attribute name="href"><xsl:value-of select="@name"/>.html</xsl:attribute>
  <em class="typename"><xsl:value-of select="@name"/></em>
</xsl:element>&amp;)
  <br />
  <xsl:if test="@inherits">
    <xsl:apply-templates select="document(concat($uriprefix, concat('/', concat(@inherits, '.xml'))))/interface" mode="cast">
      <xsl:with-param name="name" select="$name"/>
    </xsl:apply-templates>
  </xsl:if>
</xsl:template>

<xsl:template match="descr">
  <xsl:apply-templates select="*|text()"/>
</xsl:template>

<xsl:template match="p">
<p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="note">
<small>
  <b>Note:</b>
  <xsl:apply-templates/>
</small>
</xsl:template>

<xsl:template match="code">
<em class="code"><xsl:apply-templates/></em>
</xsl:template>

</xsl:stylesheet>

