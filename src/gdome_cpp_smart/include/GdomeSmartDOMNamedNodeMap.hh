
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMNamedNodeMap_hh__
#define __GdomeSmartDOMNamedNodeMap_hh__

namespace GdomeSmartDOM {

class NamedNodeMap
{
protected:
  explicit NamedNodeMap(GdomeNamedNodeMap* obj, bool) : gdome_obj(obj) { }
public:
  explicit NamedNodeMap(GdomeNamedNodeMap* = 0);
    NamedNodeMap(const NamedNodeMap&);
  ~NamedNodeMap();
NamedNodeMap& operator=(const NamedNodeMap&);
  bool operator==(const NamedNodeMap& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const NamedNodeMap& obj) const { return !(*this == obj); }
  
  operator GdomeNamedNodeMap*() const { return reinterpret_cast<GdomeNamedNodeMap*>(gdome_obj); }

  // Attributes  
  unsigned long get_length(void) const;
  
  // Methods
  class Node getNamedItem(const class GdomeString& name) const;
  class Node setNamedItem(const class Node& arg) const;
  class Node removeNamedItem(const class GdomeString& name) const;
  class Node item(const unsigned long index) const;
  class Node getNamedItemNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  class Node setNamedItemNS(const class Node& arg) const;
  class Node removeNamedItemNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  
  // Friend classes
  friend class Document;
  
  friend class Node;
  friend class DocumentType;
      GdomeNamedNodeMap* gdome_object(void) const;
  
protected:
  GdomeNamedNodeMap* gdome_obj;
};

}

#endif // __GdomeSmartDOMNamedNodeMap_hh__

