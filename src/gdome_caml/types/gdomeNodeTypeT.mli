(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *)

type t =
  NOT_USED_0			(*  0 *)
| ELEMENT_NODE			(*  1 *)
| ATTRIBUTE_NODE		(*  2 *)
| TEXT_NODE			(*  3 *)
| CDATA_SECTION_NODE		(*  4 *)
| ENTITY_REFERENCE_NODE		(*  5 *)
| ENTITY_NODE			(*  6 *)
| PROCESSING_INSTRUCTION_NODE	(*  7 *)
| COMMENT_NODE			(*  8 *)
| DOCUMENT_NODE			(*  9 *)
| DOCUMENT_TYPE_NODE		(* 10 *)
| DOCUMENT_FRAGMENT_NODE	(* 11 *)
| NOTATION_NODE			(* 12 *)

