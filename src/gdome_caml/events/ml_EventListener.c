/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <assert.h>
#include <gdome.h>

#include <gdome-events.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/callback.h>
#include "mlgdomevalue.h"

static void
ml_gdome_evntl_callback(GdomeEventListener* self, GdomeEvent* event, GdomeException* exc)
{
  GdomeException exc_ = 0;
  value* cb;

  g_return_if_fail(self != NULL);
  g_return_if_fail(event != NULL);
  g_return_if_fail(exc != NULL);

  cb = gdome_evntl_get_priv(self);
  g_return_if_fail(cb != NULL);

  gdome_evnt_ref(event, &exc_);
  g_assert(exc_ == 0);

  /* we don't care about exception because the Ocaml mechanism
   * is exactly what we want
   */
  callback(*cb, Val_Event(event));
}

static void
ml_gdome_evntl_priv_callback(GdomeEventListener* self)
{
  value* cb = gdome_evntl_get_priv(self);
  g_assert(cb != NULL);
  remove_global_root(cb);
  g_free(cb);
}

static void
ml_gdome_evntl_finalize(value v)
{
  value* cb;
  GdomeException exc_;
  GdomeEventListener* listener = EventListener_val(v);
  g_assert(listener != NULL);
  cb = gdome_evntl_get_priv(listener);
  g_assert(cb != NULL);
  gdome_evntl_unref(listener, &exc_);
  g_assert(exc_ == 0);
}

value
ml_gdome_evntl_create(value callback)
{
  CAMLparam1(callback);

  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/events/EventListener",
    ml_gdome_evntl_finalize,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  // the following version uses the deprecated priv field for storing
  // the callback value !
  value* cb = g_new(value, 1);
  value v = alloc_custom(&ops, sizeof(GdomeEventListener*), 0, 1);
  GdomeEventListener* listener = *((GdomeEventListener**) Data_custom_val(v)) = gdome_evntl_aux_mkref(ml_gdome_evntl_callback, cb,
		  ml_gdome_evntl_priv_callback);
  *cb = callback;
  register_global_root(cb);

  CAMLreturn(v);
}

GdomeEventListener*
EventListener_val(value v)
{
  GdomeEventListener* listener = *((GdomeEventListener**) Data_custom_val(v));
  g_assert(listener != NULL);
  return listener;
}

