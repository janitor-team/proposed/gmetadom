
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external of_Node : [> `Node] GdomeT.t -> TDocument.t = "ml_gdome_doc_of_n"

  
external get_doctype : this:[> `Document] GdomeT.t -> TDocumentType.t option = "ml_gdome_doc_get_doctype"


external get_implementation : this:[> `Document] GdomeT.t -> TDOMImplementation.t = "ml_gdome_doc_get_implementation"


external get_documentElement : this:[> `Document] GdomeT.t -> TElement.t = "ml_gdome_doc_get_documentElement"


external createElement : this:[> `Document] GdomeT.t -> tagName:TDOMString.t -> TElement.t = "ml_gdome_doc_createElement"
external createDocumentFragment : this:[> `Document] GdomeT.t -> TDocumentFragment.t = "ml_gdome_doc_createDocumentFragment"
external createTextNode : this:[> `Document] GdomeT.t -> data:TDOMString.t -> TText.t = "ml_gdome_doc_createTextNode"
external createComment : this:[> `Document] GdomeT.t -> data:TDOMString.t -> TComment.t = "ml_gdome_doc_createComment"
external createCDATASection : this:[> `Document] GdomeT.t -> data:TDOMString.t -> TCDATASection.t = "ml_gdome_doc_createCDATASection"
external createProcessingInstruction : this:[> `Document] GdomeT.t -> target:TDOMString.t -> data:TDOMString.t -> TProcessingInstruction.t = "ml_gdome_doc_createProcessingInstruction"
external createAttribute : this:[> `Document] GdomeT.t -> name:TDOMString.t -> TAttr.t = "ml_gdome_doc_createAttribute"
external createEntityReference : this:[> `Document] GdomeT.t -> name:TDOMString.t -> TEntityReference.t = "ml_gdome_doc_createEntityReference"
external getElementsByTagName : this:[> `Document] GdomeT.t -> tagname:TDOMString.t -> TNodeList.t = "ml_gdome_doc_getElementsByTagName"
external importNode : this:[> `Document] GdomeT.t -> importedNode:[> `Node] GdomeT.t -> deep:bool -> TNode.t = "ml_gdome_doc_importNode"
external createElementNS : this:[> `Document] GdomeT.t -> namespaceURI:TDOMString.t option -> qualifiedName:TDOMString.t -> TElement.t = "ml_gdome_doc_createElementNS"
external createAttributeNS : this:[> `Document] GdomeT.t -> namespaceURI:TDOMString.t option -> qualifiedName:TDOMString.t -> TAttr.t = "ml_gdome_doc_createAttributeNS"
external getElementsByTagNameNS : this:[> `Document] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> TNodeList.t = "ml_gdome_doc_getElementsByTagNameNS"
external getElementById : this:[> `Document] GdomeT.t -> elementId:TDOMString.t -> TElement.t option = "ml_gdome_doc_getElementById"