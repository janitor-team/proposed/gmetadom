
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

CharacterData::CharacterData(GdomeCharacterData* obj) : Node((GdomeNode*) obj) { }

CharacterData::CharacterData(const CharacterData& obj) : Node(obj) { }

CharacterData::CharacterData(const Node& obj) : Node((GdomeNode*) gdome_cast_cd(obj.gdome_obj)) { }

CharacterData& CharacterData::operator=(const CharacterData& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_cd_unref((GdomeCharacterData*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to CharacterData failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_cd_ref((GdomeCharacterData*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to CharacterData failed");
  }

  return *this;
}

CharacterData::~CharacterData()
{
}

GdomeString CharacterData::get_data() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_cd_data((GdomeCharacterData*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::get_data");
  return res_;
}

void
CharacterData::set_data(const GdomeString& v) const
{
  GdomeException exc_ = 0;
  gdome_cd_set_data((GdomeCharacterData*) gdome_obj, static_cast<GdomeDOMString*>(v), &exc_);
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::set_data");
}

unsigned long CharacterData::get_length() const
{
  GdomeException exc_ = 0;
  unsigned long res_ = gdome_cd_length((GdomeCharacterData*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::get_length");
  return res_;
}

GdomeString CharacterData::substringData(const unsigned long offset, const unsigned long count) const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_cd_substringData((GdomeCharacterData*) gdome_obj, offset, count, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::substringData");
  return res_;
}

void CharacterData::appendData(const GdomeString& arg) const
{
  GdomeException exc_ = 0;
  gdome_cd_appendData((GdomeCharacterData*) gdome_obj, static_cast<GdomeDOMString*>(arg), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::appendData");
  
}

void CharacterData::insertData(const unsigned long offset, const GdomeString& arg) const
{
  GdomeException exc_ = 0;
  gdome_cd_insertData((GdomeCharacterData*) gdome_obj, offset, static_cast<GdomeDOMString*>(arg), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::insertData");
  
}

void CharacterData::deleteData(const unsigned long offset, const unsigned long count) const
{
  GdomeException exc_ = 0;
  gdome_cd_deleteData((GdomeCharacterData*) gdome_obj, offset, count, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::deleteData");
  
}

void CharacterData::replaceData(const unsigned long offset, const unsigned long count, const GdomeString& arg) const
{
  GdomeException exc_ = 0;
  gdome_cd_replaceData((GdomeCharacterData*) gdome_obj, offset, count, static_cast<GdomeDOMString*>(arg), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "CharacterData::replaceData");
  
}



}

