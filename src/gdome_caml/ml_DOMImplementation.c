/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <gdome.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include "mlgdomevalue.h"

static void
ml_gdome_DOMImplementation_finalize(value v)
{
  GdomeException exc_;
  GdomeDOMImplementation* obj_ = DOMImplementation_val(v);
  if (obj_ != NULL) gdome_di_unref(obj_, &exc_);
  g_assert(exc_ == 0);
}

GdomeDOMImplementation*
DOMImplementation_val(value v)
{
  GdomeDOMImplementation* obj = *((GdomeDOMImplementation**) Data_custom_val(v));
  g_assert(obj != NULL);
  return obj;
}

value
Val_DOMImplementation(GdomeDOMImplementation* obj)
{
  static struct custom_operations ops = {
    "GdomeDOMImplementation",
    ml_gdome_DOMImplementation_finalize,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeDOMImplementation*), 0, 1);
  *((GdomeDOMImplementation**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_di_create()
{
  return Val_DOMImplementation(gdome_di_mkref());
}


value
ml_gdome_di_hasFeature(value self, value feature, value version)
{
  CAMLparam3(self, feature, version);
  GdomeException exc_;
  GdomeBoolean res_;
  res_ = gdome_di_hasFeature(DOMImplementation_val(self), DOMString_val(feature), DOMString_val(version), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.hasFeature");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_di_createDocumentType(value self, value qualifiedName, value publicId, value systemId)
{
  CAMLparam4(self, qualifiedName, publicId, systemId);
  GdomeException exc_;
  GdomeDocumentType* res_;
  res_ = gdome_di_createDocumentType(DOMImplementation_val(self), DOMString_val(qualifiedName), DOMString_val(publicId), DOMString_val(systemId), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.createDocumentType");
  CAMLreturn(Val_DocumentType(res_));
}


value
ml_gdome_di_createDocument(value self, value namespaceURI, value qualifiedName, value doctype)
{
  CAMLparam4(self, namespaceURI, qualifiedName, doctype);
  GdomeException exc_;
  GdomeDocument* res_;
  res_ = gdome_di_createDocument(DOMImplementation_val(self), ptr_val_option(namespaceURI,DOMString_val), DOMString_val(qualifiedName), ptr_val_option(doctype,DocumentType_val), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.createDocument");
  CAMLreturn(Val_Document(res_));
}

value
ml_gdome_di_createDocumentFromURI(value self, value uri, value mode)
{
  CAMLparam3(self, uri, mode);
  GdomeException exc_;
  GdomeDocument* res_;
  res_ = gdome_di_createDocFromURI(DOMImplementation_val(self), String_val(uri), Int_val(mode), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.createDocumentFromURI");
  if (res_ == 0) throw_impl_exception("DOMImplementation.createDocumentFromURI: cannot parse document");
  CAMLreturn(Val_Document(res_));
}

value
ml_gdome_di_createDocumentFromMemory(value self, value doc, value mode)
{
  CAMLparam3(self, doc, mode);
  GdomeException exc_;
  GdomeDocument* res_;
  res_ = gdome_di_createDocFromMemory(DOMImplementation_val(self), String_val(doc), Int_val(mode), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.createDocumentFromMemory");
  if (res_ == 0) throw_impl_exception("DOMImplementation.createDocumentFromMemory: cannot parse document");
  CAMLreturn(Val_Document(res_));
}

value
ml_gdome_di_saveDocumentToFile(value self, value doc, value name, value mode)
{
  CAMLparam4(self, doc, name, mode);
  GdomeException exc_;
  GdomeBoolean res_;
  res_ = gdome_di_saveDocToFile(DOMImplementation_val(self), Document_val(doc), String_val(name), Int_val(mode), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.saveDocToFile");
  CAMLreturn(Val_bool(res_));
}

value
ml_gdome_di_saveDocumentToFileEnc(value self, value doc, value name, value encoding, value mode)
{
  CAMLparam5(self, doc, name, encoding, mode);
  GdomeException exc_;
  GdomeBoolean res_;
  res_ = gdome_di_saveDocToFileEnc(DOMImplementation_val(self), Document_val(doc), String_val(name), String_val(encoding), Int_val(mode), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.saveDocToFileEnc");
  CAMLreturn(Val_bool(res_));
}

value
ml_gdome_di_saveDocumentToMemory(value self, value doc, value mode)
{
  CAMLparam3(self, doc, mode);
  GdomeException exc_;
  char *c_string = NULL;
  CAMLlocal1(caml_string);
  gdome_di_saveDocToMemory(DOMImplementation_val(self),
                           Document_val(doc),
                           &c_string,
                           Int_val(mode),
                           &exc_);
  /*
   * ### XXX The buffer returned by gdome_di_saveDocToMemory should
   * really be freed using a GDome function that calls the free() from
   * the DOM library that allocated the buffer.  For now, just use
   * free(), since libxml2 normally uses malloc() to allocate the
   * memory.
   */
#define my_free free

  if (exc_ != 0) {
    if (c_string) {
      my_free(c_string);
    }
    throw_exception(exc_, "DOMImplementation.saveDocToMemory");
  }
  if (!c_string) raise_out_of_memory();
  caml_string = copy_string(c_string);

  my_free(c_string);

  CAMLreturn(caml_string);
}

value
ml_gdome_di_saveDocumentToMemoryEnc(value self, value doc, value encoding, value mode)
{
  CAMLparam4(self, doc, encoding, mode);
  GdomeException exc_;
  char *c_string = NULL;
  CAMLlocal1(caml_string);
  gdome_di_saveDocToMemoryEnc(DOMImplementation_val(self),
			      Document_val(doc),
			      &c_string,
			      String_val(encoding),
			      Int_val(mode),
			      &exc_);
  /*
   * ### XXX The buffer returned by gdome_di_saveDocToMemory should
   * really be freed using a GDome function that calls the free() from
   * the DOM library that allocated the buffer.  For now, just use
   * free(), since libxml2 normally uses malloc() to allocate the
   * memory.
   */
#define my_free free

  if (exc_ != 0) {
    if (c_string) {
      my_free(c_string);
    }
    throw_exception(exc_, "DOMImplementation.saveDocToMemoryEnc");
  }
  if (!c_string) raise_out_of_memory();
  caml_string = copy_string(c_string);

  my_free(c_string);

  CAMLreturn(caml_string);
}

value
ml_gdome_di_enableEvent(value self, value doc, value name)
{
  CAMLparam3(self, doc, name);
  GdomeException exc_;
  gdome_di_enableEvent(DOMImplementation_val(self), Document_val(doc), String_val(name), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.enableEvent");
  CAMLreturn(Val_unit);
}

value
ml_gdome_di_disableEvent(value self, value doc, value name)
{
  CAMLparam3(self, doc, name);
  GdomeException exc_;
  gdome_di_disableEvent(DOMImplementation_val(self), Document_val(doc), String_val(name), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.disableEvent");
  CAMLreturn(Val_unit);
}

value
ml_gdome_di_eventIsEnabled(value self, value doc, value name)
{
  CAMLparam3(self, doc, name);
  GdomeException exc_;
  GdomeBoolean res_ = gdome_di_eventIsEnabled(DOMImplementation_val(self), Document_val(doc), String_val(name), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DOMImplementation.eventIsEnabled");
  CAMLreturn(Val_bool(res_));
}

