/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <config.h>

#include <cstdlib>
#include <cassert>

#include <gdome.h>
#include <iostream>

#include "GdomeSmartDOM.hh"

using namespace GdomeSmartDOM;
using namespace std;

class Handler : public EventListener
{
public:
  void handleEvent(const Event& ev);
};

void
Handler::handleEvent(const Event& ev)
{
  cout << "an event occurred" << endl;
}

void
navigate(unsigned indent, const Node& node)
{
  if (!node) {
    for (unsigned i = 0; i < indent; i++) cout << ' ';
    cout << "(null)" << endl;
    return;
  }
  
  GdomeNode* gdome_node = node.gdome_object();
  cout << "Gdome object: " << gdome_node << " unique id: " << node.id() << endl;
  if (gdome_node)
    {
      GdomeException exc = 0;
      gdome_n_unref(gdome_node, &exc);
      assert(exc == 0);
    }
	  
  unsigned short type = node.get_nodeType();
  switch (type) {
  case Node::DOCUMENT_NODE:
    {
      Document doc(node);
      Element root = doc.get_documentElement();
      navigate(indent, root);
    }
    break;
  case Node::ELEMENT_NODE:
    {
      Element elem(node);
  
      for (unsigned i = 0; i < indent; i++) cout << ' ';
      cout << "<" << node.get_nodeName();
      NamedNodeMap nmap = node.get_attributes();
      for (unsigned i = 0; i < nmap.get_length(); i++) {
	Attr attr = nmap.item(i);
	cout << " ";
	navigate(0, attr);
      }
      if (!node.get_firstChild())
	cout << "/>" << endl;
      else {
	cout << ">" << endl;      
	for (Node p = elem.get_firstChild(); p; p = p.get_nextSibling())
	  navigate(indent + 2, p);
	for (unsigned i = 0; i < indent; i++) cout << ' ';
	cout << "</" << elem.get_tagName() << ">" << endl;
      }
    }
    break;
  case Node::ATTRIBUTE_NODE:
    {
      cout << node.get_nodeName() << "=\"" << node.get_nodeValue() << "\"";
    }
    break;
  case Node::CDATA_SECTION_NODE:
  case Node::TEXT_NODE:
    {
      GdomeString s = node.get_nodeValue();
      if (!s.null() && !s.empty()) {
	for (unsigned i = 0; i < indent; i++) cout << ' ';
	UTF8String utf8s(s);
	cout << "UTF8:  ";
	for (unsigned i = 0; i < utf8s.length(); i++) cout << (short) utf8s[i] << " ";
	cout << endl;
	for (unsigned i = 0; i < indent; i++) cout << ' ';
	UTF16String utf16s(s);
	cout << "UTF16: ";
	for (unsigned i = 0; i < utf16s.length(); i++) cout << utf16s[i] << " ";
	cout << endl;
	for (unsigned i = 0; i < indent; i++) cout << ' ';
	UCS4String ucs4s(s);
	cout << "UCS4:  ";
	for (unsigned i = 0; i < ucs4s.length(); i++) cout << ucs4s[i] << " ";
	cout << endl;
      }
    }
    break;
  case Node::COMMENT_NODE:
    {
      GdomeString s = node.get_nodeValue();
      for (unsigned i = 0; i < indent; i++) cout << ' ';      
      cout << "<!--" << s << "-->" << endl;
    }
    break;
  default:
    cout << "unrecognized node type " << node.get_nodeType() << endl;
    break;
  }
}

void
doTest(int m, char* argv[])
{
  try {
    for (int i = 0; i < m; i++) {
      DOMImplementation di;
      cout << endl << "********** file: " << argv[i] << endl;
      Document doc = di.createDocumentFromURI(argv[i], 0);

      EventTarget et(doc);
      et.addEventListener("DOMSubtreeModified", *new Handler, false);

      Element root = doc.get_documentElement();
      root.setAttribute("my-own-attribute", "my-own-value");

      navigate(0, doc);
    }
  } catch (DOMException exc) {
    cerr << "exc: " << exc.code << " " << exc.msg << endl;
  }
}

int
main(int argc, char* argv[])
{
  if (argc < 3) {
    cerr << "usage: test <n> <file>" << endl;
    return 1;
  }

  for (int i = 0; i < atoi(argv[1]); i++) doTest(argc - 2, argv + 2);
}

