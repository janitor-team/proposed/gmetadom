(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *)

external create : unit -> TDOMImplementation.t = "ml_gdome_di_create"

external hasFeature : this:TDOMImplementation.t -> feature:TDOMString.t -> version:TDOMString.t -> bool = "ml_gdome_di_hasFeature"

external createDocumentType : this:TDOMImplementation.t -> qualifiedName:TDOMString.t -> publicId:TDOMString.t -> systemId:TDOMString.t -> TDocumentType.t = "ml_gdome_di_createDocumentType"

external createDocument : this:TDOMImplementation.t -> namespaceURI:TDOMString.t option -> qualifiedName:TDOMString.t -> doctype:[> `DocumentType] GdomeT.t option -> TDocument.t = "ml_gdome_di_createDocument"

type validatingMode =
   Parsing
 | Validating
 | Recovering
;;

let mode_of_validatingMode_and_keepEntities vm ke =
 let n_vm =
  match vm with
     Parsing -> 0
   | Validating -> 1
   | Recovering -> 2
 in
 let n_ke =
  if ke then 0 else 4
 in
  n_vm lor n_ke
;;

external raw_createDocumentFromURI : this:TDOMImplementation.t -> uri:string -> mode:int -> TDocument.t = "ml_gdome_di_createDocumentFromURI"
let createDocumentFromURI ?(validatingMode=Parsing) ?(keepEntities=false) () =
 raw_createDocumentFromURI
  ~mode:(mode_of_validatingMode_and_keepEntities validatingMode keepEntities)
;;

external raw_createDocumentFromMemory : this:TDOMImplementation.t -> doc:TDOMString.t -> mode:int -> TDocument.t = "ml_gdome_di_createDocumentFromMemory"
let createDocumentFromMemory ?(validatingMode=Parsing) ?(keepEntities=false) ()=
 raw_createDocumentFromMemory
  ~mode:(mode_of_validatingMode_and_keepEntities validatingMode keepEntities)
;;
  
external raw_saveDocumentToFile : this:TDOMImplementation.t -> doc:[> `Document] GdomeT.t -> name:string -> mode:int -> bool = "ml_gdome_di_saveDocumentToFile"
let saveDocumentToFile ~this ~doc ~name ?(indent=false) () =
 raw_saveDocumentToFile ~this ~doc ~name ~mode:(if indent then 1 else 0)
;;
  
external raw_saveDocumentToFileEnc : this:TDOMImplementation.t -> doc:[> `Document] GdomeT.t -> name:string -> encoding:string -> mode:int -> bool = "ml_gdome_di_saveDocumentToFileEnc"
let saveDocumentToFileEnc ~this ~doc ~name ~encoding ?(indent=false) () =
 raw_saveDocumentToFileEnc ~this ~doc ~name ~encoding ~mode:(if indent then 1 else 0)
;;

external raw_saveDocumentToMemory :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  mode:int ->
  string = "ml_gdome_di_saveDocumentToMemory"

let saveDocumentToMemory ~this ~doc ?(indent=false) () =
 raw_saveDocumentToMemory ~this ~doc ~mode:(if indent then 1 else 0)
;;

external raw_saveDocumentToMemoryEnc :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  encoding:string ->
  mode:int ->
  string = "ml_gdome_di_saveDocumentToMemoryEnc"

let saveDocumentToMemoryEnc ~this ~doc ~encoding ?(indent=false) () =
 raw_saveDocumentToMemoryEnc ~this ~doc ~encoding ~mode:(if indent then 1 else 0)
;;

external enableEvent :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  name:string ->
  unit = "ml_gdome_di_enableEvent"

external disableEvent :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  name:string ->
  unit = "ml_gdome_di_disableEvent"

external eventIsEnabled :
  this:TDOMImplementation.t ->
  doc:[> `Document] GdomeT.t ->
  name:string ->
  bool = "ml_gdome_di_eventIsEnabled"

