<?xml version="1.0"?>

<!--
 This file is part of GMetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
               2001 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the project home page
 http://gmetadom.sourceforge.net
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>
<xsl:import href="xml2mldoc.xsl"/>

<xsl:output method="text"/>
<xsl:param name="module" select="'Core'"/>
<xsl:param name="uriprefix" select="''"/>
<xsl:param name="annotations" select="''"/>

<xsl:template match="interface">
 <xsl:if test="@inherits">
  <xsl:text>
</xsl:text>
 </xsl:if>
 <xsl:apply-templates select="." mode="cast">
   <xsl:with-param name="name" select="@name"/>
   <xsl:with-param name="inherits" select="@inherits"/>
   <xsl:with-param name="lcname">
    <xsl:call-template name="toLowerCase">
     <xsl:with-param name="string" select="@name"/>
    </xsl:call-template>
   </xsl:with-param>
 </xsl:apply-templates>
</xsl:template>

<xsl:template match="interface" mode="cast">
  <xsl:param name="name" select="''"/>
  <xsl:param name="lcname" select="''"/>
  
  <xsl:if test="@inherits">
   <xsl:variable name="parent" select="document(concat($uriprefix, concat('/', concat(@inherits, '.xml'))))/interface"/>
   <xsl:choose>
    <xsl:when test="$parent/@inherits">
     <xsl:apply-templates select="$parent" mode="cast">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="lcname" select="$lcname"/>
     </xsl:apply-templates>
    </xsl:when>
    <xsl:otherwise>
    <xsl:variable name="lcinherits">
     <xsl:call-template name="toLowerCase">
      <xsl:with-param name="string" select="@inherits"/>
     </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="comment">Dynamic cast: if the argument of the constructor is not an instance of the [<xsl:value-of select="$lcname"/>] class, a GdomeInit.DOMCastException is raised.</xsl:variable>
    <xsl:call-template name="justify">
     <xsl:with-param name="str" select="normalize-space($comment)"/>
     <xsl:with-param name="indent" select="0"/>
    </xsl:call-template>
    <xsl:text>class </xsl:text>
    <xsl:value-of select="$lcname"/>_of_<xsl:value-of select="$lcinherits"/>
    <xsl:text> :
 </xsl:text>
    <xsl:text> &lt; as_</xsl:text>
    <xsl:value-of select="@inherits"/>
    <xsl:text> : [&gt; `</xsl:text>
    <xsl:value-of select="@inherits"/>
    <xsl:text>] GdomeT.t; .. &gt; -&gt; </xsl:text>
    <xsl:value-of select="$lcname"/>
    <xsl:text>
;;
</xsl:text>
  </xsl:otherwise>
  </xsl:choose>
 </xsl:if>
</xsl:template>

</xsl:stylesheet>
