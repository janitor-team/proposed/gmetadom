<?xml version="1.0"?>

<!--
 This file is part of GMetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
               2001 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the project home page
 http://gmetadom.sourceforge.net
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>
<xsl:import href="xml2mldoc.xsl"/>

<xsl:output method="text"/>
<xsl:param name="module" select="'Core'"/>
<xsl:param name="uriprefix" select="''"/>
<xsl:param name="annotations" select="''"/>

<xsl:template match="interface">
<xsl:variable name="prefix">
  <xsl:call-template name="gdomePrefixOfType">
    <xsl:with-param name="type" select="@name"/>
  </xsl:call-template>
</xsl:variable>

<xsl:variable name="lcmodule">
 <xsl:call-template name="toLowerCase">
  <xsl:with-param name="string" select="$module"/>
 </xsl:call-template>
</xsl:variable>

<xsl:text>


</xsl:text>
<xsl:apply-templates select="descr" mode="indent0"/>
<xsl:text>and </xsl:text>
<xsl:value-of select="$lcmodule"/><xsl:text> : </xsl:text>
 <xsl:text>T</xsl:text><xsl:value-of select="@name"/><xsl:text>.t -&gt;
  (*** (new </xsl:text>
<xsl:value-of select="$lcmodule"/>
<xsl:text>) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
</xsl:text>
<xsl:if test="@name = 'Node'">
 <!-- There must be a solution better than this if -->
 <xsl:text>   inherit eventTarget
</xsl:text>
</xsl:if>
<xsl:if test="@inherits">
 <xsl:variable name="lcinherits">
  <xsl:call-template name="toLowerCase">
   <xsl:with-param name="string" select="@inherits"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:text>   inherit </xsl:text><xsl:value-of select="$lcinherits"/>
 <xsl:text>
</xsl:text>
</xsl:if>
<xsl:if test="attribute">
 <xsl:text>
   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

</xsl:text>
</xsl:if>
<xsl:apply-templates select="attribute">
 <xsl:with-param name="interface" select="@name"/>
 <xsl:with-param name="prefix" select="$prefix"/>
 <xsl:sort select="@name"/>
</xsl:apply-templates>
<xsl:if test="method">
 <xsl:text>
   (*** DOM METHODS ***)

</xsl:text>
</xsl:if>
<xsl:if test="@name = 'Node'">
   (*** Checks whether the argument is physically the same node as this node. ***)
   method isSameNode : node -> bool
   method equals : node -> bool

</xsl:if>
<xsl:apply-templates select="method">
 <xsl:with-param name="interface" select="@name"/>
 <xsl:with-param name="prefix" select="$prefix"/>
 <xsl:sort select="@name"/>
</xsl:apply-templates>
<xsl:text>
   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
</xsl:text>
 <xsl:text>   method as_</xsl:text><xsl:value-of select="@name"/>
 <xsl:text> : T</xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:text>.t
</xsl:text>
<xsl:text> end</xsl:text>
</xsl:template>

<xsl:template match="attribute">
  <xsl:param name="interface" select="''"/>
  <xsl:param name="prefix" select="''"/>
  <xsl:variable name="name" select="@name"/>
  <xsl:apply-templates select="descr" mode="indent3"/>
  <xsl:if test="getraises">
   <xsl:apply-templates select="getraises" mode="indent3"/>
  </xsl:if>
  <xsl:text>   method get_</xsl:text><xsl:value-of select="$name"/>
  <xsl:text> : </xsl:text>
  <xsl:variable name="isNullable" select="document($annotations)/Annotations/Attribute[@name = $name]/@nullable = 'yes'"/>
  <xsl:variable name="ocamlTypeOfType">
   <xsl:call-template name="ocamlTypeOfType">
    <xsl:with-param name="type" select="@type"/>
    <xsl:with-param name="isNullable" select="$isNullable"/>
   </xsl:call-template>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="@name = 'nodeType' and $interface = 'Node'">GdomeNodeTypeT.t</xsl:when>
    <xsl:otherwise><xsl:value-of select="$ocamlTypeOfType"/></xsl:otherwise>
  </xsl:choose>
  <xsl:text>
</xsl:text>
  <xsl:if test="setraises">
   <xsl:apply-templates select="setraises" mode="indent3"/>
  </xsl:if>
  <xsl:if test="not(@readonly)">
   <xsl:text>   method set_</xsl:text><xsl:value-of select="@name"/>
   <xsl:text> : value:</xsl:text>
   <xsl:value-of select="$ocamlTypeOfType"/>
   <xsl:text> -&gt; unit
</xsl:text>
  </xsl:if>
  <xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="method" >
  <xsl:param name="interface" select="''"/>
  <xsl:param name="prefix" select="''"/>
  <xsl:param name="name" select="@name"/>
  <xsl:apply-templates select="descr" mode="indent3"/>
  <xsl:if test="parameters/param">
   <xsl:apply-templates select="parameters" mode="indent3"/>
  </xsl:if>
  <xsl:if test="returns/@type != 'void'">
   <xsl:apply-templates select="returns" mode="indent3"/>
  </xsl:if>
  <xsl:if test="raises/exception">
   <xsl:apply-templates select="raises" mode="indent3"/>
  </xsl:if>
  <xsl:text>   method </xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates mode="right" select="parameters">
   <xsl:with-param name="name" select="@name"/>
  </xsl:apply-templates>
  <xsl:call-template name="ocamlTypeOfType">
   <xsl:with-param name="type" select="returns/@type"/>
   <xsl:with-param name="isNullable" select="document($annotations)/Annotations/Method[@name = $name]/@nullable = 'yes'"/>
  </xsl:call-template>
  <xsl:text>

</xsl:text>
</xsl:template>

<xsl:template match="parameters" mode="right">
  <xsl:param name="name" select="''"/>
  <xsl:apply-templates select="param" mode="right">
    <xsl:with-param name="methodName" select="$name"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="param" mode="right">
 <xsl:param name="methodName" select="''"/>
 <xsl:variable name="name" select="@name"/>
 <xsl:call-template name="ocamlify-param-name">
  <xsl:with-param name="name" select="$name"/>
 </xsl:call-template>
 <xsl:text>:</xsl:text>
 <xsl:call-template name="ocamlTypeOfType">
  <xsl:with-param name="type" select="@type"/>
  <xsl:with-param name="isNullable" select="document($annotations)/Annotations/Method[@name = $methodName]/Param[@name = $name]/@nullable = 'yes'"/>
 </xsl:call-template>
 <xsl:text> -&gt; </xsl:text>
</xsl:template>

<xsl:template name="ocamlify-param-name">
  <xsl:param name="name" select="''"/>
  <xsl:choose>
    <xsl:when test="$name = 'type'">typ</xsl:when>
    <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
