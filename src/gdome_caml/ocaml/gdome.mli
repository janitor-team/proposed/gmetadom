(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *)


(********** Core Module: hand-written class definitions **********)

class domString :
 TDOMString.t ->
  (*** (new domString) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   (*** DOM METHODS ***)
   method to_string : string
   method equals : domString -> bool

   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_DOMString : TDOMString.t
  end
;;

(* Comparison operator for objects with 'equals' method *)
val gdome_obj_eq : (< equals : 'a -> bool; .. > as 'a) -> 'a -> bool;;
val (===) : (< equals : 'a -> bool; .. > as 'a) -> 'a -> bool;;

(* The [domImplementation] interface provides a number of methods for    *)
(* performing operations that are independent of any particular instance *)
(* of the document object model.                                         *)
class domImplementation :
 TDOMImplementation.t ->
  (*** (new domImplementation) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   (* Test if the DOM implementation implements a specific feature.          *)
   (* Parameters:                                                            *)
   (*  feature: The name of the feature to test (case-insensitive). The      *)
   (*    values used by DOM features are defined throughout the DOM Level 2  *)
   (*    specifications and listed in the ID-Compliance section. The name    *)
   (*    must be an XML name. To avoid possible conflicts, as a convention,  *)
   (*    names referring to features defined outside the DOM specification   *)
   (*    should be made unique by reversing the name of the Internet domain  *)
   (*    name of the person (or the organization that the person belongs to) *)
   (*    who defines the feature, component by component, and using this as  *)
   (*    a prefix. For instance, the W3C SVG Working Group defines the       *)
   (*    feature "org.w3c.dom.svg".                                          *)
   (*  version: This is the version number of the feature to test. In Level  *)
   (*       2, the string can be either "2.0" or "1.0". If the version is    *)
   (*       not specified, supporting any version of the feature causes the  *)
   (*       method to return [true].                                         *)
   (* Result:                                                                *)
   (*  [true] if the feature is implemented in the specified version,        *)
   (*  [false] otherwise.                                                    *)
   method hasFeature : feature:domString -> version:domString -> bool

   (* Creates an empty [documentType] node. Entity declarations and          *)
   (* notations are not made available. Entity reference expansions and      *)
   (* default attribute additions do not occur. It is expected that a future *)
   (* version of the DOM will provide a way for populating a [documentType]. *)
   (* HTML-only DOM implementations do not need to implement this method.    *)
   (* Parameters:                                                            *)
   (*  qualifiedName: The qualified name of the document type to be created. *)
   (*  publicId: The external subset public identifier.                      *)
   (*  systemId: The external subset system identifier.                      *)
   (* Result:                                                                *)
   (*  A new [DocumentType] node with [Node.ownerDocument] set to None.      *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified qualified name       *)
   (*    contains an illegal character.                                      *)
   (*    NAMESPACE_ERR: Raised if the [qualifiedName] is malformed.          *)
   method createDocumentType : qualifiedName:domString -> publicId:domString -> systemId:domString -> documentType

   (* Creates an XML [document] object of the specified type with its        *)
   (* document element. HTML-only DOM implementations do not need to         *)
   (* implement this method.                                                 *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the document element to create.    *)
   (*  qualifiedName: The qualified name of the document element to be       *)
   (*    created.                                                            *)
   (*  documentType: The type of document to be created or None. When        *)
   (*    [doctype] is not None, its [Node.ownerDocument] attribute is set to *)
   (*    the document being created.                                         *)
   (* Result:                                                                *)
   (*  A new [Document] object.                                              *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified qualified name       *)
   (*    contains an illegal character.                                      *)
   (*    NAMESPACE_ERR: Raised if the [qualifiedName] is malformed, if the   *)
   (*    [qualifiedName] has a prefix and the [namespaceURI] is None, or if  *)
   (*    the [qualifiedName] has a prefix that is "xml" and the              *)
   (*    [namespaceURI] is different from "xml-ns;".                         *)
   (*    WRONG_DOCUMENT_ERR: Raised if [doctype] has already been used with  *)
   (*    a different document or was created from a different implementation.*)
   method createDocument : namespaceURI:domString option -> qualifiedName:domString -> doctype:documentType option -> document

   (* Creates an XML [document] object loading an XML document from the    *)
   (* specified URI.                                                       *)
   (* Parameters:                                                          *)
   (*  uri: The URI of the XML document to be parsed.                      *)
   (*  validatingMode: Can be Parsing, Validating or Recovering; if not    *)
   (*   specified, it defaults to Parsing.                                 *)
   (*  keepEntities: if true [entityReference] nodes are explicitly        *)
   (*   inserted in the DOM tree. Otherwise, they are substituted with     *)
   (*   the entity expansion. The latest is the default.                   *)
   (* Result:                                                              *)
   (*  A new [document] object.                                            *)
   (* Exceptions Raised:                                                   *)
   (*  DOMImplException: if an error occurs during parsing.                *)
   method createDocumentFromURI : uri:string -> ?validatingMode:IDOMImplementation.validatingMode -> ?keepEntities:bool -> unit -> document

   (* Creates an XML [document] object parsing an XML document from the    *)
   (* specified buffer.                                                    *)
   (* Parameters:                                                          *)
   (*  doc: A string buffer with the XML document to be parsed.            *)
   (*  validatingMode: Can be Parsing, Validating or Recovering; if not    *)
   (*   specified, it defaults to Parsing.                                 *)
   (*  keepEntities: if true [entityReference] nodes are explicitly        *)
   (*   inserted in the DOM tree. Otherwise, they are substituted with     *)
   (*   the entity expansion. The latest is the default.                   *)
   (* Result:                                                              *)
   (*  A new [document] object.                                            *)
   (* Exceptions Raised:                                                   *)
   (*  DOMImplException: if an error occurs during parsing.                *)
   method createDocumentFromMemory : doc:domString -> ?validatingMode:IDOMImplementation.validatingMode -> ?keepEntities:bool -> unit -> document

   (* Saves an XML [document] to disk.                                     *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document] to be saved.                                *)
   (*  name: The name of the destination file.                             *)
   (*  indent: if true, the document is pretty-printed indenting lines.    *)
   (*   The default is to not indent the document.                         *)
   (* Result:                                                              *)
   (*  [true] if the operation was succesful, [false] otherwise.           *)
   method saveDocumentToFile : doc:document -> name:string -> ?indent:bool -> unit -> bool

   (* Saves an XML [document] to disk.                                     *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document] to be saved.                                *)
   (*  name: The name of the destination file.                             *)
   (*  encoding: The name of the encoding.                                 *) 
   (*  indent: if true, the document is pretty-printed indenting lines.    *)
   (*   The default is to not indent the document.                         *)
   (* Result:                                                              *)
   (*  [true] if the operation was succesful, [false] otherwise.           *)
   method saveDocumentToFileEnc : doc:document -> name:string -> encoding:string -> ?indent:bool -> unit -> bool

   (* Saves an XML [document] to a string.                                 *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document] to be saved.                                *)
   (*  indent: if true, the document is pretty-printed indenting lines.    *)
   (*   The default is to not indent the document.                         *)
   (* Result:                                                              *)
   (*  A new [string] containing the document.                             *)
   (* Exceptions Raised:                                                   *)
   (*  DOMImplException: if an error occurs during parsing.                *)
   method saveDocumentToMemory : doc:document -> ?indent:bool -> unit -> string

   (* Saves an XML [document] to a string.                                 *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document] to be saved.                                *)
   (*  encoding: The name of the encoding.                                 *) 
   (*  indent: if true, the document is pretty-printed indenting lines.    *)
   (*   The default is to not indent the document.                         *)
   (* Result:                                                              *)
   (*  A new [string] containing the document.                             *)
   (* Exceptions Raised:                                                   *)
   (*  DOMImplException: if an error occurs during parsing.                *)
   method saveDocumentToMemoryEnc : doc:document -> encoding:string -> ?indent:bool -> unit -> string

   (* Enables a DOM Event.                                                 *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document]                                             *)
   (*  name: The name of the DOM Event to be enabled. "*" means all the    *)
   (*   events.                                                            *)
   method enableEvent : doc:document -> name:string -> unit

   (* Disables a DOM Event.                                                *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document]                                             *)
   (*  name: The name of the DOM Event to be disabled. "*" means all the   *)
   (*   events.                                                            *)
   method disableEvent : doc:document -> name:string -> unit

   (* Tests whether a DOM Event is enabled or disabled.                    *)
   (* Parameters:                                                          *)
   (*  doc: The XML [document]                                             *)
   (*  name: The name of the DOM Event to test.                            *)
   method eventIsEnabled : doc:document -> name:string -> bool

  end

(********** Events Module: generated class definitions **********)



(* The [eventTarget] interface is implemented by all [nodes] in an        *)
(* implementation which supports the DOM Event Model. Therefore, this     *)
(* interface can be obtained by using binding-specific casting methods    *)
(* on an instance of the [node] interface. The interface allows           *)
(* registration and removal of [eventListeners] on an [eventTarget] and   *)
(* dispatch of events to that [eventTarget].                              *)
and eventTarget : TEventTarget.t ->
  (*** (new eventTarget) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object

   (*** DOM METHODS ***)

   (* This method allows the registration of event listeners on the event    *)
   (* target. If an [eventListener] is added to an [eventTarget] while it    *)
   (* is processing an event, it will not be triggered by the current        *)
   (* actions but may be triggered during a later stage of event flow, such  *)
   (* as the bubbling phase. If multiple identical [eventListener]s are      *)
   (* registered on the same [eventTarget] with the same parameters the      *)
   (* duplicate instances are discarded. They do not cause the               *)
   (* [eventListener] to be called twice and since they are discarded they   *)
   (* do not need to be removed with the [removeEventListener] method.       *)
   (* Parameters:                                                            *)
   (*  type: The event type for which the user is registering                *)
   (*  listener: The [listener] parameter takes an interface implemented by  *)
   (*    the user which contains the methods to be called when the event     *)
   (*    occurs.                                                             *)
   (*  useCapture: If true, [useCapture] indicates that the user wishes to   *)
   (*    initiate capture. After initiating capture, all events of the       *)
   (*    specified type will be dispatched to the registered                 *)
   (*    [eventListener] before being dispatched to any [eventTargets]       *)
   (*    beneath them in the tree. Events which are bubbling upward through  *)
   (*    the tree will not trigger an [eventListener] designated to use      *)
   (*    capture.                                                            *)
   method addEventListener : typ:domString -> listener:eventListener -> useCapture:bool -> unit

   (* This method allows the dispatch of events into the implementations     *)
   (* event model. Events dispatched in this manner will have the same       *)
   (* capturing and bubbling behavior as events dispatched directly by the   *)
   (* implementation. The target of the event is the [ EventTarget] on       *)
   (* which [dispatchEvent] is called.                                       *)
   (* Parameters:                                                            *)
   (*  evt: Specifies the event type, behavior, and contextual information   *)
   (*    to be used in processing the event.                                 *)
   (* Result:                                                                *)
   (*  The return value of [dispatchEvent] indicates whether any of the      *)
   (*  listeners which handled the event called [preventDefault]. If         *)
   (*  [preventDefault] was called the value is false, else the value is     *)
   (*  true.                                                                 *)
   (* Exceptions Raised:                                                     *)
   (*  EventException:                                                       *)
   (*    UNSPECIFIED_EVENT_TYPE_ERR: Raised if the [event]'s type was not    *)
   (*    specified by initializing the event before [dispatchEvent] was      *)
   (*    called. Specification of the [event]'s type as [None] or an empty   *)
   (*    string will also trigger this exception.                            *)
   method dispatchEvent : evt:event -> bool

   (* This method allows the removal of event listeners from the event       *)
   (* target. If an [eventListener] is removed from an [eventTarget] while   *)
   (* it is processing an event, it will not be triggered by the current     *)
   (* actions. [eventListener]s can never be invoked after being removed.    *)
   (* Calling [removeEventListener] with arguments which do not identify     *)
   (* any currently registered [eventListener] on the [eventTarget] has no   *)
   (* effect.                                                                *)
   (* Parameters:                                                            *)
   (*  type: Specifies the event type of the [eventListener] being removed.  *)
   (*  listener: The [eventListener] parameter indicates the [eventListener  *)
   (*    ] to be removed.                                                    *)
   (*  useCapture: Specifies whether the [eventListener] being removed was   *)
   (*    registered as a capturing listener or not. If a listener was        *)
   (*    registered twice, one with capture and one without, each must be    *)
   (*    removed separately. Removal of a capturing listener does not        *)
   (*    affect a non-capturing version of the same listener, and vice       *)
   (*    versa.                                                              *)
   method removeEventListener : typ:domString -> listener:eventListener -> useCapture:bool -> unit


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_EventTarget : TEventTarget.t
 end


(* The [event] interface is used to provide contextual information about  *)
(* an event to the handler processing the event. An object which          *)
(* implements the [event] interface is generally passed as the first      *)
(* parameter to an event handler. More specific context information is    *)
(* passed to event handlers by deriving additional interfaces from        *)
(* [event] which contain information directly relating to the type of     *)
(* event they accompany. These derived interfaces are also implemented    *)
(* by the object passed to the event listener.                            *)
and event : TEvent.t ->
  (*** (new event) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* Used to indicate whether or not an event is a bubbling event. If the   *)
   (* event can bubble the value is true, else the value is false.           *)
   method get_bubbles : bool

   (* Used to indicate whether or not an event can have its default action   *)
   (* prevented. If the default action can be prevented the value is true,   *)
   (* else the value is false.                                               *)
   method get_cancelable : bool

   (* Used to indicate the [eventTarget] whose [eventListeners] are          *)
   (* currently being processed. This is particularly useful during          *)
   (* capturing and bubbling.                                                *)
   method get_currentTarget : eventTarget

   (* Used to indicate which phase of event flow is currently being          *)
   (* evaluated.                                                             *)
   method get_eventPhase : int

   (* Used to indicate the [eventTarget] to which the event was originally   *)
   (* dispatched.                                                            *)
   method get_target : eventTarget

   (* Used to specify the time (in milliseconds relative to the epoch) at    *)
   (* which the event was created. Due to the fact that some systems may     *)
   (* not provide this information the value of [timeStamp] may be not       *)
   (* available for all events. When not available, a value of 0 will be     *)
   (* returned. Examples of epoch time are the time of the system start or   *)
   (* 0:0:0 UTC 1st January 1970.                                            *)
   method get_timeStamp : TDOMTimeStamp.t

   (* The name of the event (case-insensitive). The name must be an XML      *)
   (* name.                                                                  *)
   method get_type : domString


   (*** DOM METHODS ***)

   (* The [initEvent] method is used to initialize the value of an [event]   *)
   (* created through the [documentEvent] interface. This method may only    *)
   (* be called before the [event] has been dispatched via the               *)
   (* [dispatchEvent] method, though it may be called multiple times during  *)
   (* that phase if necessary. If called multiple times the final            *)
   (* invocation takes precedence. If called from a subclass of [event]      *)
   (* interface only the values specified in the [initEvent] method are      *)
   (* modified, all other attributes are left unchanged.                     *)
   (* Parameters:                                                            *)
   (*  eventTypeArg: Specifies the event type. This type may be any event    *)
   (*    type currently defined in this specification or a new event type..  *)
   (*    The string must be an XML name. Any new event type must not begin   *)
   (*    with any upper, lower, or mixed case version of the string "DOM".   *)
   (*    This prefix is reserved for future DOM event sets. It is also       *)
   (*    strongly recommended that third parties adding their own events     *)
   (*    use their own prefix to avoid confusion and lessen the probability  *)
   (*    of conflicts with other new events.                                 *)
   (*  canBubbleArg: Specifies whether or not the event can bubble.          *)
   (*  cancelableArg: Specifies whether or not the event's default action    *)
   (*    can be prevented.                                                   *)
   method initEvent : eventTypeArg:domString -> canBubbleArg:bool -> cancelableArg:bool -> unit

   (* If an event is cancelable, the [preventDefault] method is used to      *)
   (* signify that the event is to be canceled, meaning any default action   *)
   (* normally taken by the implementation as a result of the event will     *)
   (* not occur. If, during any stage of event flow, the [preventDefault]    *)
   (* method is called the event is canceled. Any default action associated  *)
   (* with the event will not occur. Calling this method for a               *)
   (* non-cancelable event has no effect. Once [preventDefault] has been     *)
   (* called it will remain in effect throughout the remainder of the        *)
   (* event's propagation. This method may be used during any stage of       *)
   (* event flow.                                                            *)
   method preventDefault : unit

   (* The [stopPropagation] method is used prevent further propagation of    *)
   (* an event during event flow. If this method is called by any            *)
   (* [eventListener] the event will cease propagating through the tree.     *)
   (* The event will complete dispatch to all listeners on the current       *)
   (* [eventTarget] before event flow stops. This method may be used during  *)
   (* any stage of event flow.                                               *)
   method stopPropagation : unit


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Event : TEvent.t
 end


(* The [mutationEvent] interface provides specific contextual             *)
(* information associated with Mutation events.                           *)
and mutationEvent : TMutationEvent.t ->
  (*** (new mutationEvent) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit event

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* [attrChange] indicates the type of change which triggered the          *)
   (* DOMAttrModified event. The values can be [mODIFICATION], [aDDITION],   *)
   (* or [rEMOVAL].                                                          *)
   method get_attrChange : int

   (* [attrName] indicates the name of the changed [attr] node in a          *)
   (* DOMAttrModified event.                                                 *)
   method get_attrName : domString option

   (* [newValue] indicates the new value of the [attr] node in               *)
   (* DOMAttrModified events, and of the [characterData] node in             *)
   (* DOMCharDataModified events.                                            *)
   method get_newValue : domString option

   (* [prevValue] indicates the previous value of the [attr] node in         *)
   (* DOMAttrModified events, and of the [characterData] node in             *)
   (* DOMCharDataModified events.                                            *)
   method get_prevValue : domString option

   (* [relatedNode] is used to identify a secondary node related to a        *)
   (* mutation event. For example, if a mutation event is dispatched to a    *)
   (* node indicating that its parent has changed, the [relatedNode] is the  *)
   (* changed parent. If an event is instead dispatched to a subtree         *)
   (* indicating a node was changed within it, the [relatedNode] is the      *)
   (* changed node. In the case of the DOMAttrModified event it indicates    *)
   (* the [attr] node which was modified, added, or removed.                 *)
   method get_relatedNode : node option


   (*** DOM METHODS ***)

   (* The [initMutationEvent] method is used to initialize the value of a    *)
   (* [mutationEvent] created through the [documentEvent] interface. This    *)
   (* method may only be called before the [mutationEvent] has been          *)
   (* dispatched via the [dispatchEvent] method, though it may be called     *)
   (* multiple times during that phase if necessary. If called multiple      *)
   (* times, the final invocation takes precedence.                          *)
   (* Parameters:                                                            *)
   (*  typeArg: Specifies the event type.                                    *)
   (*  canBubbleArg: Specifies whether or not the event can bubble.          *)
   (*  cancelableArg: Specifies whether or not the event's default action    *)
   (*    can be prevented.                                                   *)
   (*  relatedNodeArg: Specifies the [event]'s related Node                  *)
   (*  prevValueArg: Specifies the [event]'s [prevValue] attribute           *)
   (*  newValueArg: Specifies the [event]'s [newValue] attribute             *)
   (*  attrNameArg: Specifies the [event]'s [attrName] attribute             *)
   (*  attrChangeArg: Specifies the [event]'s [attrName] attribute           *)
   method initMutationEvent : typeArg:domString -> canBubbleArg:bool -> cancelableArg:bool -> relatedNodeArg:node option -> prevValueArg:domString option -> newValueArg:domString option -> attrNameArg:domString option -> attrChangeArg:int option -> unit


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_MutationEvent : TMutationEvent.t
 end

(********** Events Module: hand-written class definitions **********)

and eventListener : TEventListener.t ->
 object
  (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
  (*** required to implement bindings to other libraries ***)
  method as_EventListener : TEventListener.t
 end

(********** Core Module: generated class definitions **********)



(* The [node] interface is the primary datatype for the entire Document   *)
(* Object Model. It represents a single node in the document tree. While  *)
(* all objects implementing the [node] interface expose methods for       *)
(* dealing with children, not all objects implementing the [node]         *)
(* interface may have children. For example, [text] nodes may not have    *)
(* children, and adding children to such nodes results in a               *)
(* [domException] being raised. The attributes [nodeName], [nodeValue]    *)
(* and [attributes] are included as a mechanism to get at node            *)
(* information without casting down to the specific derived interface.    *)
(* In cases where there is no obvious mapping of these attributes for a   *)
(* specific [nodeType] (e.g., [nodeValue] for an [element] or             *)
(* [attributes] for a [comment]), this returns [None]. Note that the      *)
(* specialized interfaces may contain additional and more convenient      *)
(* mechanisms to get and set the relevant information.                    *)
and node : TNode.t ->
  (*** (new node) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit eventTarget

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* A [namedNodeMap] containing the attributes of this node (if it is an   *)
   (* [element]) or [None] otherwise.                                        *)
   method get_attributes : namedNodeMap option

   (* A [nodeList] that contains all children of this node. If there are no  *)
   (* children, this is a [nodeList] containing no nodes.                    *)
   method get_childNodes : nodeList

   (* The first child of this node. If there is no such node, this returns   *)
   (* [None].                                                                *)
   method get_firstChild : node option

   (* The last child of this node. If there is no such node, this returns    *)
   (* [None].                                                                *)
   method get_lastChild : node option

   (* Returns the local part of the qualified name of this node. For nodes   *)
   (* of any type other than [eLEMENT_NODE] and [aTTRIBUTE_NODE] and nodes   *)
   (* created with a DOM Level 1 method, such as [createElement] from the    *)
   (* [document] interface, this is always [None].                           *)
   method get_localName : domString option

   (* The namespace URI of this node, or [None] if it is unspecified. This   *)
   (* is not a computed value that is the result of a namespace lookup       *)
   (* based on an examination of the namespace declarations in scope. It is  *)
   (* merely the namespace URI given at creation time. For nodes of any      *)
   (* type other than [eLEMENT_NODE] and [aTTRIBUTE_NODE] and nodes created  *)
   (* with a DOM Level 1 method, such as [createElement] from the            *)
   (* [document] interface, this is always [None].                           *)
   method get_namespaceURI : domString option

   (* The node immediately following this node. If there is no such node,    *)
   (* this returns [None].                                                   *)
   method get_nextSibling : node option

   (* The name of this node, depending on its type; see the table above.     *)
   method get_nodeName : domString

   (* A code representing the type of the underlying object, as defined      *)
   (* above.                                                                 *)
   method get_nodeType : GdomeNodeTypeT.t

   (* The value of this node, depending on its type; see the table above.    *)
   (* When it is defined to be [None], setting it has no effect.             *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    DOMSTRING_SIZE_ERR: Raised when it would return more characters     *)
   (*    than fit in a [domString] variable on the implementation platform.  *)
   method get_nodeValue : domString option
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised when the node is readonly.      *)
   method set_nodeValue : value:domString option -> unit

   (* The [document] object associated with this node. This is also the      *)
   (* [document] object used to create new nodes. When this node is a        *)
   (* [document] or a [documentType] which is not used with any [document]   *)
   (* yet, this is [None].                                                   *)
   method get_ownerDocument : document option

   (* The parent of this node. All nodes, except [attr], [document],         *)
   (* [documentFragment], [entity], and [notation] may have a parent.        *)
   (* However, if a node has just been created and not yet added to the      *)
   (* tree, or if it has been removed from the tree, this is [None].         *)
   method get_parentNode : node option

   (* The namespace prefix of this node, or [None] if it is unspecified.     *)
   (* Note that setting this attribute, when permitted, changes the          *)
   (* [nodeName] attribute, which holds the qualified name, as well as the   *)
   (* [tagName] and [name] attributes of the [element] and [attr]            *)
   (* interfaces, when applicable. Note also that changing the prefix of an  *)
   (* attribute that is known to have a default value, does not make a new   *)
   (* attribute with the default value and the original prefix appear,       *)
   (* since the [namespaceURI] and [localName] do not change. For nodes of   *)
   (* any type other than [eLEMENT_NODE] and [aTTRIBUTE_NODE] and nodes      *)
   (* created with a DOM Level 1 method, such as [createElement] from the    *)
   (* [document] interface, this is always [None].                           *)
   method get_prefix : domString option
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified prefix contains an   *)
   (*    illegal character. NO_MODIFICATION_ALLOWED_ERR: Raised if this      *)
   (*    node is readonly. NAMESPACE_ERR: Raised if the specified [prefix]   *)
   (*    is malformed, if the [namespaceURI] of this node is [None], if the  *)
   (*    specified prefix is "xml" and the [namespaceURI] of this node is    *)
   (*    different from "xml-ns;", if this node is an attribute and the      *)
   (*    specified prefix is "xmlns" and the [namespaceURI] of this node is  *)
   (*    different from "xmlns-ns;", or if this node is an attribute and     *)
   (*    the [qualifiedName] of this node is "xmlns" .                       *)
   method set_prefix : value:domString option -> unit

   (* The node immediately preceding this node. If there is no such node,    *)
   (* this returns [None].                                                   *)
   method get_previousSibling : node option


   (*** DOM METHODS ***)


   (*** Checks whether the argument is physically the same node as this node. ***)
   method isSameNode : node -> bool
   method equals : node -> bool

   (* Adds the node [newChild] to the end of the list of children of this    *)
   (* node. If the [newChild] is already in the tree, it is first removed.   *)
   (* Parameters:                                                            *)
   (*  newChild: The node to add. If it is a [documentFragment] object, the  *)
   (*    entire contents of the document fragment are moved into the child   *)
   (*    list of this node                                                   *)
   (* Result:                                                                *)
   (*  The node added.                                                       *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    HIERARCHY_REQUEST_ERR: Raised if this node is of a type that does   *)
   (*    not allow children of the type of the [newChild] node, or if the    *)
   (*    node to append is one of this node's ancestors.                     *)
   (*    WRONG_DOCUMENT_ERR: Raised if [newChild] was created from a         *)
   (*    different document than the one that created this node.             *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   method appendChild : newChild:node -> node

   (* Returns a duplicate of this node, i.e., serves as a generic copy       *)
   (* constructor for nodes. The duplicate node has no parent;               *)
   (* ([parentNode] is [None].). Cloning an [element] copies all attributes  *)
   (* and their values, including those generated by the XML processor to    *)
   (* represent defaulted attributes, but this method does not copy any      *)
   (* text it contains unless it is a deep clone, since the text is          *)
   (* contained in a child [text] node. Cloning an [attribute] directly, as  *)
   (* opposed to be cloned as part of an [element] cloning operation,        *)
   (* returns a specified attribute ([specified] is [true]). Cloning any     *)
   (* other type of node simply returns a copy of this node. Note that       *)
   (* cloning an immutable subtree results in a mutable copy, but the        *)
   (* children of an [entityReference] clone are readonly. In addition,      *)
   (* clones of unspecified [attr] nodes are specified. And, cloning         *)
   (* [document], [documentType], [entity], and [notation] nodes is          *)
   (* implementation dependent.                                              *)
   (* Parameters:                                                            *)
   (*  deep: If [true], recursively clone the subtree under the specified    *)
   (*    node; if [false], clone only the node itself (and its attributes,   *)
   (*    if it is an [element]).                                             *)
   (* Result:                                                                *)
   (*  The duplicate node.                                                   *)
   method cloneNode : deep:bool -> node

   (* Returns whether this node (if it is an element) has any attributes.    *)
   (* Result:                                                                *)
   (*  [true] if this node has any attributes, [false] otherwise.            *)
   method hasAttributes : bool

   (* Returns whether this node has any children.                            *)
   (* Result:                                                                *)
   (*  [true] if this node has any children, [false] otherwise.              *)
   method hasChildNodes : bool

   (* Inserts the node [newChild] before the existing child node             *)
   (* [refChild]. If [refChild] is [None], insert [newChild] at the end of   *)
   (* the list of children. If [newChild] is a [documentFragment] object,    *)
   (* all of its children are inserted, in the same order, before            *)
   (* [refChild]. If the [newChild] is already in the tree, it is first      *)
   (* removed.                                                               *)
   (* Parameters:                                                            *)
   (*  newChild: The node to insert.                                         *)
   (*  refChild: The reference node, i.e., the node before which the new     *)
   (*    node must be inserted.                                              *)
   (* Result:                                                                *)
   (*  The node being inserted.                                              *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    HIERARCHY_REQUEST_ERR: Raised if this node is of a type that does   *)
   (*    not allow children of the type of the [newChild] node, or if the    *)
   (*    node to insert is one of this node's ancestors.                     *)
   (*    WRONG_DOCUMENT_ERR: Raised if [newChild] was created from a         *)
   (*    different document than the one that created this node.             *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly or if  *)
   (*    the parent of the node being inserted is readonly. NOT_FOUND_ERR:   *)
   (*    Raised if [refChild] is not a child of this node.                   *)
   method insertBefore : newChild:node -> refChild:node option -> node

   (* Tests whether the DOM implementation implements a specific feature     *)
   (* and that feature is supported by this node.                            *)
   (* Parameters:                                                            *)
   (*  feature: The name of the feature to test. This is the same name       *)
   (*    which can be passed to the method [hasFeature] on                   *)
   (*    [domImplementation].                                                *)
   (*  version: This is the version number of the feature to test. In Level  *)
   (*    2, version 1, this is the string "2.0". If the version is not       *)
   (*    specified, supporting any version of the feature will cause the     *)
   (*    method to return [true].                                            *)
   (* Result:                                                                *)
   (*  Returns [true] if the specified feature is supported on this node,    *)
   (*  [false] otherwise.                                                    *)
   method isSupported : feature:domString -> version:domString -> bool

   (* Puts all [text] nodes in the full depth of the sub-tree underneath     *)
   (* this [node], including attribute nodes, into a "normal" form where     *)
   (* only structure (e.g., elements, comments, processing instructions,     *)
   (* CDATA sections, and entity references) separates [text] nodes, i.e.,   *)
   (* there are neither adjacent [text] nodes nor empty [text] nodes. This   *)
   (* can be used to ensure that the DOM view of a document is the same as   *)
   (* if it were saved and re-loaded, and is useful when operations (such    *)
   (* as XPointer lookups) that depend on a particular document tree         *)
   (* structure are to be used.                                              *)
   method normalize : unit

   (* Removes the child node indicated by [oldChild] from the list of        *)
   (* children, and returns it.                                              *)
   (* Parameters:                                                            *)
   (*  oldChild: The node being removed.                                     *)
   (* Result:                                                                *)
   (*  The node removed.                                                     *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   (*    NOT_FOUND_ERR: Raised if [oldChild] is not a child of this node.    *)
   method removeChild : oldChild:node -> node

   (* Replaces the child node [oldChild] with [newChild] in the list of      *)
   (* children, and returns the [oldChild] node. If [newChild] is a          *)
   (* [documentFragment] object, [oldChild] is replaced by all of the        *)
   (* [documentFragment] children, which are inserted in the same order. If  *)
   (* the [newChild] is already in the tree, it is first removed.            *)
   (* Parameters:                                                            *)
   (*  newChild: The new node to put in the child list.                      *)
   (*  oldChild: The node being replaced in the list.                        *)
   (* Result:                                                                *)
   (*  The node replaced.                                                    *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    HIERARCHY_REQUEST_ERR: Raised if this node is of a type that does   *)
   (*    not allow children of the type of the [newChild] node, or if the    *)
   (*    node to put in is one of this node's ancestors.                     *)
   (*    WRONG_DOCUMENT_ERR: Raised if [newChild] was created from a         *)
   (*    different document than the one that created this node.             *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node or the parent of   *)
   (*    the new node is readonly. NOT_FOUND_ERR: Raised if [oldChild] is    *)
   (*    not a child of this node.                                           *)
   method replaceChild : newChild:node -> oldChild:node -> node


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Node : TNode.t
 end


(* The [nodeList] interface provides the abstraction of an ordered        *)
(* collection of nodes, without defining or constraining how this         *)
(* collection is implemented. [nodeList] objects in the DOM are live.     *)
(* The items in the [nodeList] are accessible via an integral index,      *)
(* starting from 0.                                                       *)
and nodeList : TNodeList.t ->
  (*** (new nodeList) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The number of nodes in the list. The range of valid child node         *)
   (* indices is 0 to [length-1] inclusive.                                  *)
   method get_length : int


   (*** DOM METHODS ***)

   (* Returns the [index]th item in the collection. If [index] is greater    *)
   (* than or equal to the number of nodes in the list, this returns         *)
   (* [None].                                                                *)
   (* Parameters:                                                            *)
   (*  index: Index into the collection.                                     *)
   (* Result:                                                                *)
   (*  The node at the [index]th position in the [nodeList], or [None] if    *)
   (*  that is not a valid index.                                            *)
   method item : index:int -> node option


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_NodeList : TNodeList.t
 end


(* Objects implementing the [namedNodeMap] interface are used to          *)
(* represent collections of nodes that can be accessed by name. Note      *)
(* that [namedNodeMap] does not inherit from [nodeList]; [namedNodeMaps]  *)
(* are not maintained in any particular order. Objects contained in an    *)
(* object implementing [namedNodeMap] may also be accessed by an ordinal  *)
(* index, but this is simply to allow convenient enumeration of the       *)
(* contents of a [namedNodeMap], and does not imply that the DOM          *)
(* specifies an order to these Nodes. [namedNodeMap] objects in the DOM   *)
(* are live.                                                              *)
and namedNodeMap : TNamedNodeMap.t ->
  (*** (new namedNodeMap) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The number of nodes in this map. The range of valid child node         *)
   (* indices is [0] to [length-1] inclusive.                                *)
   method get_length : int


   (*** DOM METHODS ***)

   (* Retrieves a node specified by name.                                    *)
   (* Parameters:                                                            *)
   (*  name: The [nodeName] of a node to retrieve.                           *)
   (* Result:                                                                *)
   (*  A [node] (of any type) with the specified [nodeName], or [None] if    *)
   (*  it does not identify any node in this map.                            *)
   method getNamedItem : name:domString -> node option

   (* Retrieves a node specified by local name and namespace URI. HTML-only  *)
   (* DOM implementations do not need to implement this method.              *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the node to retrieve.              *)
   (*  localName: The local name of the node to retrieve.                    *)
   (* Result:                                                                *)
   (*  A [node] (of any type) with the specified local name and namespace    *)
   (*  URI, or [None] if they do not identify any node in this map.          *)
   method getNamedItemNS : namespaceURI:domString -> localName:domString -> node option

   (* Returns the [index]th item in the map. If [index] is greater than or   *)
   (* equal to the number of nodes in this map, this returns [None].         *)
   (* Parameters:                                                            *)
   (*  index: Index into this map.                                           *)
   (* Result:                                                                *)
   (*  The node at the [index]th position in the map, or [None] if that is   *)
   (*  not a valid index.                                                    *)
   method item : index:int -> node option

   (* Removes a node specified by name. When this map contains the           *)
   (* attributes attached to an element, if the removed attribute is known   *)
   (* to have a default value, an attribute immediately appears containing   *)
   (* the default value as well as the corresponding namespace URI, local    *)
   (* name, and prefix when applicable.                                      *)
   (* Parameters:                                                            *)
   (*  name: The [nodeName] of the node to remove.                           *)
   (* Result:                                                                *)
   (*  The node removed from this map if a node with such a name exists.     *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NOT_FOUND_ERR: Raised if there is no node named [name] in this      *)
   (*    map. NO_MODIFICATION_ALLOWED_ERR: Raised if this map is readonly.   *)
   method removeNamedItem : name:domString -> node

   (* Removes a node specified by local name and namespace URI. A removed    *)
   (* attribute may be known to have a default value when this map contains  *)
   (* the attributes attached to an element, as returned by the attributes   *)
   (* attribute of the [node] interface. If so, an attribute immediately     *)
   (* appears containing the default value as well as the corresponding      *)
   (* namespace URI, local name, and prefix when applicable. HTML-only DOM   *)
   (* implementations do not need to implement this method.                  *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the node to remove.                *)
   (*  localName: The local name of the node to remove.                      *)
   (* Result:                                                                *)
   (*  The node removed from this map if a node with such a local name and   *)
   (*  namespace URI exists.                                                 *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NOT_FOUND_ERR: Raised if there is no node with the specified        *)
   (*    [namespaceURI] and [localName] in this map.                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this map is readonly.        *)
   method removeNamedItemNS : namespaceURI:domString -> localName:domString -> node

   (* Adds a node using its [nodeName] attribute. If a node with that name   *)
   (* is already present in this map, it is replaced by the new one. As the  *)
   (* [nodeName] attribute is used to derive the name which the node must    *)
   (* be stored under, multiple nodes of certain types (those that have a    *)
   (* "special" string value) cannot be stored as the names would clash.     *)
   (* This is seen as preferable to allowing nodes to be aliased.            *)
   (* Parameters:                                                            *)
   (*  arg: A node to store in this map. The node will later be accessible   *)
   (*    using the value of its [nodeName] attribute.                        *)
   (* Result:                                                                *)
   (*  If the new [node] replaces an existing node the replaced [node] is    *)
   (*  returned, otherwise [None] is returned.                               *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    WRONG_DOCUMENT_ERR: Raised if [arg] was created from a different    *)
   (*    document than the one that created this map.                        *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this map is readonly.        *)
   (*    INUSE_ATTRIBUTE_ERR: Raised if [arg] is an [attr] that is already   *)
   (*    an attribute of another [element] object. The DOM user must         *)
   (*    explicitly clone [attr] nodes to re-use them in other elements.     *)
   method setNamedItem : arg:node -> node option

   (* Adds a node using its [namespaceURI] and [localName]. If a node with   *)
   (* that namespace URI and that local name is already present in this      *)
   (* map, it is replaced by the new one. HTML-only DOM implementations do   *)
   (* not need to implement this method.                                     *)
   (* Parameters:                                                            *)
   (*  arg: A node to store in this map. The node will later be accessible   *)
   (*    using the value of its [namespaceURI] and [localName] attributes.   *)
   (* Result:                                                                *)
   (*  If the new [node] replaces an existing node the replaced [node] is    *)
   (*  returned, otherwise [None] is returned.                               *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    WRONG_DOCUMENT_ERR: Raised if [arg] was created from a different    *)
   (*    document than the one that created this map.                        *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this map is readonly.        *)
   (*    INUSE_ATTRIBUTE_ERR: Raised if [arg] is an [attr] that is already   *)
   (*    an attribute of another [element] object. The DOM user must         *)
   (*    explicitly clone [attr] nodes to re-use them in other elements.     *)
   method setNamedItemNS : arg:node -> node option


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_NamedNodeMap : TNamedNodeMap.t
 end


(* The [characterData] interface extends Node with a set of attributes    *)
(* and methods for accessing character data in the DOM. For clarity this  *)
(* set is defined here rather than on each object that uses these         *)
(* attributes and methods. No DOM objects correspond directly to          *)
(* [characterData], though [text] and others do inherit the interface     *)
(* from it. All [offsets] in this interface start from [0]. As explained  *)
(* in the [domString] interface, text strings in the DOM are represented  *)
(* in UTF-16, i.e. as a sequence of 16-bit units. In the following, the   *)
(* term 16-bit units is used whenever necessary to indicate that          *)
(* indexing on CharacterData is done in 16-bit units.                     *)
and characterData : TCharacterData.t ->
  (*** (new characterData) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The character data of the node that implements this interface. The     *)
   (* DOM implementation may not put arbitrary limits on the amount of data  *)
   (* that may be stored in a [characterData] node. However, implementation  *)
   (* limits may mean that the entirety of a node's data may not fit into a  *)
   (* single [domString]. In such cases, the user may call [substringData]   *)
   (* to retrieve the data in appropriately sized pieces.                    *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    DOMSTRING_SIZE_ERR: Raised when it would return more characters     *)
   (*    than fit in a [domString] variable on the implementation platform.  *)
   method get_data : domString
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised when the node is readonly.      *)
   method set_data : value:domString -> unit

   (* The number of 16-bit units that are available through [data] and the   *)
   (* [substringData] method below. This may have the value zero, i.e.,      *)
   (* [characterData] nodes may be empty.                                    *)
   method get_length : int


   (*** DOM METHODS ***)

   (* Append the string to the end of the character data of the node. Upon   *)
   (* success, [data] provides access to the concatenation of [data] and     *)
   (* the [domString] specified.                                             *)
   (* Parameters:                                                            *)
   (*  arg: The [domString] to append.                                       *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   method appendData : arg:domString -> unit

   (* Remove a range of 16-bit units from the node. Upon success, [data]     *)
   (* and [length] reflect the change.                                       *)
   (* Parameters:                                                            *)
   (*  offset: The offset from which to start removing.                      *)
   (*  count: The number of 16-bit units to delete. If the sum of [offset]   *)
   (*    and [count] exceeds [length] then all 16-bit units from [offset]    *)
   (*    to the end of the data are deleted.                                 *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INDEX_SIZE_ERR: Raised if the specified [offset] is negative or     *)
   (*    greater than the number of 16-bit units in [data], or if the        *)
   (*    specified [count] is negative. NO_MODIFICATION_ALLOWED_ERR: Raised  *)
   (*    if this node is readonly.                                           *)
   method deleteData : offset:int -> count:int -> unit

   (* Insert a string at the specified 16-bit unit offset.                   *)
   (* Parameters:                                                            *)
   (*  offset: The character offset at which to insert.                      *)
   (*  arg: The [domString] to insert.                                       *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INDEX_SIZE_ERR: Raised if the specified [offset] is negative or     *)
   (*    greater than the number of 16-bit units in [data].                  *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   method insertData : offset:int -> arg:domString -> unit

   (* Replace the characters starting at the specified 16-bit unit offset    *)
   (* with the specified string.                                             *)
   (* Parameters:                                                            *)
   (*  offset: The offset from which to start replacing.                     *)
   (*  count: The number of 16-bit units to replace. If the sum of [offset]  *)
   (*    and [count] exceeds [length], then all 16-bit units to the end of   *)
   (*    the data are replaced; (i.e., the effect is the same as a [remove]  *)
   (*    method call with the same range, followed by an [append] method     *)
   (*    invocation).                                                        *)
   (*  arg: The [domString] with which the range must be replaced.           *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INDEX_SIZE_ERR: Raised if the specified [offset] is negative or     *)
   (*    greater than the number of 16-bit units in [data], or if the        *)
   (*    specified [count] is negative. NO_MODIFICATION_ALLOWED_ERR: Raised  *)
   (*    if this node is readonly.                                           *)
   method replaceData : offset:int -> count:int -> arg:domString -> unit

   (* Extracts a range of data from the node.                                *)
   (* Parameters:                                                            *)
   (*  offset: Start offset of substring to extract.                         *)
   (*  count: The number of 16-bit units to extract.                         *)
   (* Result:                                                                *)
   (*  The specified substring. If the sum of [offset] and [count] exceeds   *)
   (*  the [length], then all 16-bit units to the end of the data are        *)
   (*  returned.                                                             *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INDEX_SIZE_ERR: Raised if the specified [offset] is negative or     *)
   (*    greater than the number of 16-bit units in [data], or if the        *)
   (*    specified [count] is negative. DOMSTRING_SIZE_ERR: Raised if the    *)
   (*    specified range of text does not fit into a [domString].            *)
   method substringData : offset:int -> count:int -> domString


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_CharacterData : TCharacterData.t
 end


(* The [attr] interface represents an attribute in an [element] object.   *)
(* Typically the allowable values for the attribute are defined in a      *)
(* document type definition. [attr] objects inherit the [node]            *)
(* interface, but since they are not actually child nodes of the element  *)
(* they describe, the DOM does not consider them part of the document     *)
(* tree. Thus, the [node] attributes [parentNode], [previousSibling],     *)
(* and [nextSibling] have a [None] value for [attr] objects. The DOM      *)
(* takes the view that attributes are properties of elements rather than  *)
(* having a separate identity from the elements they are associated       *)
(* with; this should make it more efficient to implement such features    *)
(* as default attributes associated with all elements of a given type.    *)
(* Furthermore, [attr] nodes may not be immediate children of a           *)
(* [documentFragment]. However, they can be associated with [element]     *)
(* nodes contained within a [documentFragment]. In short, users and       *)
(* implementors of the DOM need to be aware that [attr] nodes have some   *)
(* things in common with other objects inheriting the [node] interface,   *)
(* but they also are quite distinct. The attribute's effective value is   *)
(* determined as follows: if this attribute has been explicitly assigned  *)
(* any value, that value is the attribute's effective value; otherwise,   *)
(* if there is a declaration for this attribute, and that declaration     *)
(* includes a default value, then that default value is the attribute's   *)
(* effective value; otherwise, the attribute does not exist on this       *)
(* element in the structure model until it has been explicitly added.     *)
(* Note that the [nodeValue] attribute on the [attr] instance can also    *)
(* be used to retrieve the string version of the attribute's value(s).    *)
(* In XML, where the value of an attribute can contain entity             *)
(* references, the child nodes of the [attr] node provide a               *)
(* representation in which entity references are not expanded. These      *)
(* child nodes may be either [text] or [entityReference] nodes. Because   *)
(* the DOM Core is not aware of attribute types, it treats all attribute  *)
(* values as simple strings, even if the DTD or schema declares them as   *)
(* having tokenized types.                                                *)
and attr : TAttr.t ->
  (*** (new attr) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* Returns the name of this attribute.                                    *)
   method get_name : domString

   (* The [element] node this attribute is attached to or [None] if this     *)
   (* attribute is not in use.                                               *)
   method get_ownerElement : element option

   (* If this attribute was explicitly given a value in the original         *)
   (* document, this is [true]; otherwise, it is [false]. Note that the      *)
   (* implementation is in charge of this attribute, not the user. If the    *)
   (* user changes the value of the attribute (even if it ends up having     *)
   (* the same value as the default value) then the [specified] flag is      *)
   (* automatically flipped to [true]. To re-specify the attribute as the    *)
   (* default value from the DTD, the user must delete the attribute. The    *)
   (* implementation will then make a new attribute available with           *)
   (* [specified] set to [false] and the default value (if one exists). In   *)
   (* summary: If the attribute has an assigned value in the document then   *)
   (* [specified] is [true], and the value is the assigned value. If the     *)
   (* attribute has no assigned value in the document and has a default      *)
   (* value in the DTD, then [specified] is [false], and the value is the    *)
   (* default value in the DTD. If the attribute has no assigned value in    *)
   (* the document and has a value of #IMPLIED in the DTD, then the          *)
   (* attribute does not appear in the structure model of the document. If   *)
   (* the [ownerElement] attribute is [None] (i.e. because it was just       *)
   (* created or was set to [None] by the various removal and cloning        *)
   (* operations) [specified] is [true].                                     *)
   method get_specified : bool

   (* On retrieval, the value of the attribute is returned as a string.      *)
   (* Character and general entity references are replaced with their        *)
   (* values. See also the method [getAttribute] on the [element]            *)
   (* interface. On setting, this creates a [text] node with the unparsed    *)
   (* contents of the string. I.e. any characters that an XML processor      *)
   (* would recognize as markup are instead treated as literal text. See     *)
   (* also the method [setAttribute] on the [element] interface.             *)
   method get_value : domString
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised when the node is readonly.      *)
   method set_value : value:domString -> unit


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Attr : TAttr.t
 end


(* The [element] interface represents an element in an HTML or XML        *)
(* document. Elements may have attributes associated with them; since     *)
(* the [element] interface inherits from [node], the generic [node]       *)
(* interface attribute [attributes] may be used to retrieve the set of    *)
(* all attributes for an element. There are methods on the [element]      *)
(* interface to retrieve either an [attr] object by name or an attribute  *)
(* value by name. In XML, where an attribute value may contain entity     *)
(* references, an [attr] object should be retrieved to examine the        *)
(* possibly fairly complex sub-tree representing the attribute value. On  *)
(* the other hand, in HTML, where all attributes have simple string       *)
(* values, methods to directly access an attribute value can safely be    *)
(* used as a convenience.                                                 *)
and element : TElement.t ->
  (*** (new element) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The name of the element. For example, in: <elementExample id="demo">   *)
   (* ... </elementExample> , [tagName] has the value ["elementExample"].    *)
   (* Note that this is case-preserving in XML, as are all of the            *)
   (* operations of the DOM. The HTML DOM returns the [tagName] of an HTML   *)
   (* element in the canonical uppercase form, regardless of the case in     *)
   (* the source HTML document.                                              *)
   method get_tagName : domString


   (*** DOM METHODS ***)

   (* Retrieves an attribute value by name.                                  *)
   (* Parameters:                                                            *)
   (*  name: The name of the attribute to retrieve.                          *)
   (* Result:                                                                *)
   (*  The [attr] value as a string, or the empty string if that attribute   *)
   (*  does not have a specified or default value.                           *)
   method getAttribute : name:domString -> domString

   (* Retrieves an attribute value by local name and namespace URI.          *)
   (* HTML-only DOM implementations do not need to implement this method.    *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the attribute to retrieve.         *)
   (*  localName: The local name of the attribute to retrieve.               *)
   (* Result:                                                                *)
   (*  The [attr] value as a string, or the empty string if that attribute   *)
   (*  does not have a specified or default value.                           *)
   method getAttributeNS : namespaceURI:domString -> localName:domString -> domString

   (* Retrieves an attribute node by name. To retrieve an attribute node by  *)
   (* qualified name and namespace URI, use the [getAttributeNodeNS]         *)
   (* method.                                                                *)
   (* Parameters:                                                            *)
   (*  name: The name ([nodeName]) of the attribute to retrieve.             *)
   (* Result:                                                                *)
   (*  The [attr] node with the specified name ([nodeName]) or [None] if     *)
   (*  there is no such attribute.                                           *)
   method getAttributeNode : name:domString -> attr option

   (* Retrieves an [attr] node by local name and namespace URI. HTML-only    *)
   (* DOM implementations do not need to implement this method.              *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the attribute to retrieve.         *)
   (*  localName: The local name of the attribute to retrieve.               *)
   (* Result:                                                                *)
   (*  The [attr] node with the specified attribute local name and           *)
   (*  namespace URI or [None] if there is no such attribute.                *)
   method getAttributeNodeNS : namespaceURI:domString -> localName:domString -> attr option

   (* Returns a [nodeList] of all descendant [elements] with a given tag     *)
   (* name, in the order in which they are encountered in a preorder         *)
   (* traversal of this [element] tree.                                      *)
   (* Parameters:                                                            *)
   (*  name: The name of the tag to match on. The special value "*" matches  *)
   (*    all tags.                                                           *)
   (* Result:                                                                *)
   (*  A list of matching [element] nodes.                                   *)
   method getElementsByTagName : name:domString -> nodeList

   (* Returns a [nodeList] of all the descendant [elements] with a given     *)
   (* local name and namespace URI in the order in which they are            *)
   (* encountered in a preorder traversal of this [element] tree. HTML-only  *)
   (* DOM implementations do not need to implement this method.              *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the elements to match on. The      *)
   (*    special value "*" matches all namespaces.                           *)
   (*  localName: The local name of the elements to match on. The special    *)
   (*    value "*" matches all local names.                                  *)
   (* Result:                                                                *)
   (*  A new [nodeList] object containing all the matched [elements].        *)
   method getElementsByTagNameNS : namespaceURI:domString -> localName:domString -> nodeList

   (* Returns [true] when an attribute with a given name is specified on     *)
   (* this element or has a default value, [false] otherwise.                *)
   (* Parameters:                                                            *)
   (*  name: The name of the attribute to look for.                          *)
   (* Result:                                                                *)
   (*  [true] if an attribute with the given name is specified on this       *)
   (*  element or has a default value, [false] otherwise.                    *)
   method hasAttribute : name:domString -> bool

   (* Returns [true] when an attribute with a given local name and           *)
   (* namespace URI is specified on this element or has a default value,     *)
   (* [false] otherwise. HTML-only DOM implementations do not need to        *)
   (* implement this method.                                                 *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the attribute to look for.         *)
   (*  localName: The local name of the attribute to look for.               *)
   (* Result:                                                                *)
   (*  [true] if an attribute with the given local name and namespace URI    *)
   (*  is specified or has a default value on this element, [false]          *)
   (*  otherwise.                                                            *)
   method hasAttributeNS : namespaceURI:domString -> localName:domString -> bool

   (* Removes an attribute by name. If the removed attribute is known to     *)
   (* have a default value, an attribute immediately appears containing the  *)
   (* default value as well as the corresponding namespace URI, local name,  *)
   (* and prefix when applicable. To remove an attribute by local name and   *)
   (* namespace URI, use the [removeAttributeNS] method.                     *)
   (* Parameters:                                                            *)
   (*  name: The name of the attribute to remove.                            *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   method removeAttribute : name:domString -> unit

   (* Removes an attribute by local name and namespace URI. If the removed   *)
   (* attribute has a default value it is immediately replaced. The          *)
   (* replacing attribute has the same namespace URI and local name, as      *)
   (* well as the original prefix. HTML-only DOM implementations do not      *)
   (* need to implement this method.                                         *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the attribute to remove.           *)
   (*  localName: The local name of the attribute to remove.                 *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   method removeAttributeNS : namespaceURI:domString -> localName:domString -> unit

   (* Removes the specified attribute node. If the removed [attr] has a      *)
   (* default value it is immediately replaced. The replacing attribute has  *)
   (* the same namespace URI and local name, as well as the original         *)
   (* prefix, when applicable.                                               *)
   (* Parameters:                                                            *)
   (*  oldAttr: The [attr] node to remove from the attribute list.           *)
   (* Result:                                                                *)
   (*  The [attr] node that was removed.                                     *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   (*    NOT_FOUND_ERR: Raised if [oldAttr] is not an attribute of the       *)
   (*    element.                                                            *)
   method removeAttributeNode : oldAttr:attr -> attr

   (* Adds a new attribute. If an attribute with that name is already        *)
   (* present in the element, its value is changed to be that of the value   *)
   (* parameter. This value is a simple string; it is not parsed as it is    *)
   (* being set. So any markup (such as syntax to be recognized as an        *)
   (* entity reference) is treated as literal text, and needs to be          *)
   (* appropriately escaped by the implementation when it is written out.    *)
   (* In order to assign an attribute value that contains entity             *)
   (* references, the user must create an [attr] node plus any [text] and    *)
   (* [entityReference] nodes, build the appropriate subtree, and use        *)
   (* [setAttributeNode] to assign it as the value of an attribute. To set   *)
   (* an attribute with a qualified name and namespace URI, use the          *)
   (* [setAttributeNS] method.                                               *)
   (* Parameters:                                                            *)
   (*  name: The name of the attribute to create or alter.                   *)
   (*  value: Value to set in string form.                                   *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified name contains an     *)
   (*    illegal character. NO_MODIFICATION_ALLOWED_ERR: Raised if this      *)
   (*    node is readonly.                                                   *)
   method setAttribute : name:domString -> value:domString -> unit

   (* Adds a new attribute. If an attribute with the same local name and     *)
   (* namespace URI is already present on the element, its prefix is         *)
   (* changed to be the prefix part of the [qualifiedName], and its value    *)
   (* is changed to be the [value] parameter. This value is a simple         *)
   (* string; it is not parsed as it is being set. So any markup (such as    *)
   (* syntax to be recognized as an entity reference) is treated as literal  *)
   (* text, and needs to be appropriately escaped by the implementation      *)
   (* when it is written out. In order to assign an attribute value that     *)
   (* contains entity references, the user must create an [attr] node plus   *)
   (* any [text] and [entityReference] nodes, build the appropriate          *)
   (* subtree, and use [setAttributeNodeNS] or [setAttributeNode] to assign  *)
   (* it as the value of an attribute. HTML-only DOM implementations do not  *)
   (* need to implement this method.                                         *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the attribute to create or alter.  *)
   (*  qualifiedName: The qualified name of the attribute to create or       *)
   (*    alter.                                                              *)
   (*  value: The value to set in string form.                               *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified qualified name       *)
   (*    contains an illegal character. NO_MODIFICATION_ALLOWED_ERR: Raised  *)
   (*    if this node is readonly. NAMESPACE_ERR: Raised if the              *)
   (*    [qualifiedName] is malformed, if the [qualifiedName] has a prefix   *)
   (*    and the [namespaceURI] is [None], if the [qualifiedName] has a      *)
   (*    prefix that is "xml" and the [namespaceURI] is different from       *)
   (*    "xml-ns;", or if the [qualifiedName] is "xmlns" and the             *)
   (*    [namespaceURI] is different from "xmlns-ns;".                       *)
   method setAttributeNS : namespaceURI:domString option -> qualifiedName:domString -> value:domString -> unit

   (* Adds a new attribute node. If an attribute with that name              *)
   (* ([nodeName]) is already present in the element, it is replaced by the  *)
   (* new one. To add a new attribute node with a qualified name and         *)
   (* namespace URI, use the [setAttributeNodeNS] method.                    *)
   (* Parameters:                                                            *)
   (*  newAttr: The [attr] node to add to the attribute list.                *)
   (* Result:                                                                *)
   (*  If the [newAttr] attribute replaces an existing attribute, the        *)
   (*  replaced [attr] node is returned, otherwise [None] is returned.       *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    WRONG_DOCUMENT_ERR: Raised if [newAttr] was created from a          *)
   (*    different document than the one that created the element.           *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   (*    INUSE_ATTRIBUTE_ERR: Raised if [newAttr] is already an attribute    *)
   (*    of another [element] object. The DOM user must explicitly clone     *)
   (*    [attr] nodes to re-use them in other elements.                      *)
   method setAttributeNode : newAttr:attr -> attr

   (* Adds a new attribute. If an attribute with that local name and that    *)
   (* namespace URI is already present in the element, it is replaced by     *)
   (* the new one. HTML-only DOM implementations do not need to implement    *)
   (* this method.                                                           *)
   (* Parameters:                                                            *)
   (*  newAttr: The [attr] node to add to the attribute list.                *)
   (* Result:                                                                *)
   (*  If the [newAttr] attribute replaces an existing attribute with the    *)
   (*  same local name and namespace URI, the replaced [attr] node is        *)
   (*  returned, otherwise [None] is returned.                               *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    WRONG_DOCUMENT_ERR: Raised if [newAttr] was created from a          *)
   (*    different document than the one that created the element.           *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   (*    INUSE_ATTRIBUTE_ERR: Raised if [newAttr] is already an attribute    *)
   (*    of another [element] object. The DOM user must explicitly clone     *)
   (*    [attr] nodes to re-use them in other elements.                      *)
   method setAttributeNodeNS : newAttr:attr -> attr


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Element : TElement.t
 end


(* The [text] interface inherits from [characterData] and represents the  *)
(* textual content (termed character data in XML) of an [element] or      *)
(* [attr]. If there is no markup inside an element's content, the text    *)
(* is contained in a single object implementing the [text] interface      *)
(* that is the only child of the element. If there is markup, it is       *)
(* parsed into the information items (elements, comments, etc.) and       *)
(* [text] nodes that form the list of children of the element. When a     *)
(* document is first made available via the DOM, there is only one        *)
(* [text] node for each block of text. Users may create adjacent [text]   *)
(* nodes that represent the contents of a given element without any       *)
(* intervening markup, but should be aware that there is no way to        *)
(* represent the separations between these nodes in XML or HTML, so they  *)
(* will not (in general) persist between DOM editing sessions. The        *)
(* [normalize()] method on [node] merges any such adjacent [text]         *)
(* objects into a single node for each block of text.                     *)
and text : TText.t ->
  (*** (new text) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit characterData

   (*** DOM METHODS ***)

   (* Breaks this node into two nodes at the specified [offset], keeping     *)
   (* both in the tree as siblings. After being split, this node will        *)
   (* contain all the content up to the [offset] point. A new node of the    *)
   (* same type, which contains all the content at and after the [offset]    *)
   (* point, is returned. If the original node had a parent node, the new    *)
   (* node is inserted as the next sibling of the original node. When the    *)
   (* [offset] is equal to the length of this node, the new node has no      *)
   (* data.                                                                  *)
   (* Parameters:                                                            *)
   (*  offset: The 16-bit unit offset at which to split, starting from [0].  *)
   (* Result:                                                                *)
   (*  The new node, of the same type as this node.                          *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INDEX_SIZE_ERR: Raised if the specified offset is negative or       *)
   (*    greater than the number of 16-bit units in [data].                  *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised if this node is readonly.       *)
   method splitText : offset:int -> text


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Text : TText.t
 end


(* This interface inherits from [characterData] and represents the        *)
(* content of a comment, i.e., all the characters between the starting    *)
(* '[<!--]' and ending '[-->]'. Note that this is the definition of a     *)
(* comment in XML, and, in practice, HTML, although some HTML tools may   *)
(* implement the full SGML comment structure.                             *)
and comment : TComment.t ->
  (*** (new comment) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit characterData

   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Comment : TComment.t
 end


(* CDATA sections are used to escape blocks of text containing            *)
(* characters that would otherwise be regarded as markup. The only        *)
(* delimiter that is recognized in a CDATA section is the "]]>" string    *)
(* that ends the CDATA section. CDATA sections cannot be nested. Their    *)
(* primary purpose is for including material such as XML fragments,       *)
(* without needing to escape all the delimiters. The [domString]          *)
(* attribute of the [text] node holds the text that is contained by the   *)
(* CDATA section. Note that this may contain characters that need to be   *)
(* escaped outside of CDATA sections and that, depending on the           *)
(* character encoding ("charset") chosen for serialization, it may be     *)
(* impossible to write out some characters as part of a CDATA section.    *)
(* The [cdataSection] interface inherits from the [characterData]         *)
(* interface through the [text] interface. Adjacent [cdataSection] nodes  *)
(* are not merged by use of the [normalize] method of the [node]          *)
(* interface.                                                             *)
and cdataSection : TCDATASection.t ->
  (*** (new cdataSection) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit text

   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_CDATASection : TCDATASection.t
 end


(* Each [document] has a [doctype] attribute whose value is either        *)
(* [None] or a [documentType] object. The [documentType] interface in     *)
(* the DOM Core provides an interface to the list of entities that are    *)
(* defined for the document, and little else because the effect of        *)
(* namespaces and the various XML schema efforts on DTD representation    *)
(* are not clearly understood as of this writing. The DOM Level 2         *)
(* doesn't support editing [documentType] nodes.                          *)
and documentType : TDocumentType.t ->
  (*** (new documentType) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* A [namedNodeMap] containing the general entities, both external and    *)
   (* internal, declared in the DTD. Parameter entities are not contained.   *)
   (* Duplicates are discarded. For example in: <!DOCTYPE ex SYSTEM          *)
   (* "ex.dtd" [ <!ENTITY foo "foo"> <!ENTITY bar "bar"> <!ENTITY bar        *)
   (* "bar2"> <!ENTITY % baz "baz"> ]> <ex/> the interface provides access   *)
   (* to [foo] and the first declaration of [bar] but not the second         *)
   (* declaration of [bar] or [baz]. Every node in this map also implements  *)
   (* the [entity] interface. The DOM Level 2 does not support editing       *)
   (* entities, therefore [entities] cannot be altered in any way.           *)
   method get_entities : namedNodeMap

   (* The internal subset as a string.                                       *)
   method get_internalSubset : domString

   (* The name of DTD; i.e., the name immediately following the [dOCTYPE]    *)
   (* keyword.                                                               *)
   method get_name : domString

   (* A [namedNodeMap] containing the notations declared in the DTD.         *)
   (* Duplicates are discarded. Every node in this map also implements the   *)
   (* [notation] interface. The DOM Level 2 does not support editing         *)
   (* notations, therefore [notations] cannot be altered in any way.         *)
   method get_notations : namedNodeMap

   (* The public identifier of the external subset.                          *)
   method get_publicId : domString

   (* The system identifier of the external subset.                          *)
   method get_systemId : domString


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_DocumentType : TDocumentType.t
 end


(* This interface represents a notation declared in the DTD. A notation   *)
(* either declares, by name, the format of an unparsed entity (see        *)
(* section 4.7 of the XML 1.0 specification ), or is used for formal      *)
(* declaration of processing instruction targets (see section 2.6 of the  *)
(* XML 1.0 specification ). The [nodeName] attribute inherited from       *)
(* [node] is set to the declared name of the notation. The DOM Level 1    *)
(* does not support editing [notation] nodes; they are therefore          *)
(* readonly. A [notation] node does not have any parent.                  *)
and notation : TNotation.t ->
  (*** (new notation) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The public identifier of this notation. If the public identifier was   *)
   (* not specified, this is [None].                                         *)
   method get_publicId : domString option

   (* The system identifier of this notation. If the system identifier was   *)
   (* not specified, this is [None].                                         *)
   method get_systemId : domString option


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Notation : TNotation.t
 end


(* This interface represents an entity, either parsed or unparsed, in an  *)
(* XML document. Note that this models the entity itself not the entity   *)
(* declaration. [entity] declaration modeling has been left for a later   *)
(* Level of the DOM specification. The [nodeName] attribute that is       *)
(* inherited from [node] contains the name of the entity. An XML          *)
(* processor may choose to completely expand entities before the          *)
(* structure model is passed to the DOM; in this case there will be no    *)
(* [entityReference] nodes in the document tree. XML does not mandate     *)
(* that a non-validating XML processor read and process entity            *)
(* declarations made in the external subset or declared in external       *)
(* parameter entities. This means that parsed entities declared in the    *)
(* external subset need not be expanded by some classes of applications,  *)
(* and that the replacement value of the entity may not be available.     *)
(* When the replacement value is available, the corresponding [entity]    *)
(* node's child list represents the structure of that replacement text.   *)
(* Otherwise, the child list is empty. The DOM Level 2 does not support   *)
(* editing [entity] nodes; if a user wants to make changes to the         *)
(* contents of an [entity], every related [entityReference] node has to   *)
(* be replaced in the structure model by a clone of the [entity]'s        *)
(* contents, and then the desired changes must be made to each of those   *)
(* clones instead. [entity] nodes and all their descendants are           *)
(* readonly. An [entity] node does not have any parent.                   *)
and entity : TEntity.t ->
  (*** (new entity) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* For unparsed entities, the name of the notation for the entity. For    *)
   (* parsed entities, this is [None].                                       *)
   method get_notationName : domString option

   (* The public identifier associated with the entity, if specified. If     *)
   (* the public identifier was not specified, this is [None].               *)
   method get_publicId : domString option

   (* The system identifier associated with the entity, if specified. If     *)
   (* the system identifier was not specified, this is [None].               *)
   method get_systemId : domString option


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Entity : TEntity.t
 end


(* [entityReference] objects may be inserted into the structure model     *)
(* when an entity reference is in the source document, or when the user   *)
(* wishes to insert an entity reference. Note that character references   *)
(* and references to predefined entities are considered to be expanded    *)
(* by the HTML or XML processor so that characters are represented by     *)
(* their Unicode equivalent rather than by an entity reference.           *)
(* Moreover, the XML processor may completely expand references to        *)
(* entities while building the structure model, instead of providing      *)
(* [entityReference] objects. If it does provide such objects, then for   *)
(* a given [entityReference] node, it may be that there is no [entity]    *)
(* node representing the referenced entity. If such an [entity] exists,   *)
(* then the subtree of the [entityReference] node is in general a copy    *)
(* of the [entity] node subtree. However, this may not be true when an    *)
(* entity contains an unbound namespace prefix. In such a case, because   *)
(* the namespace prefix resolution depends on where the entity reference  *)
(* is, the descendants of the [entityReference] node may be bound to      *)
(* different namespace URIs. As for [entity] nodes, [entityReference]     *)
(* nodes and all their descendants are readonly.                          *)
and entityReference : TEntityReference.t ->
  (*** (new entityReference) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_EntityReference : TEntityReference.t
 end


(* The [processingInstruction] interface represents a "processing         *)
(* instruction", used in XML as a way to keep processor-specific          *)
(* information in the text of the document.                               *)
and processingInstruction : TProcessingInstruction.t ->
  (*** (new processingInstruction) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The content of this processing instruction. This is from the first     *)
   (* non white space character after the target to the character            *)
   (* immediately preceding the [?>].                                        *)
   method get_data : domString
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NO_MODIFICATION_ALLOWED_ERR: Raised when the node is readonly.      *)
   method set_data : value:domString -> unit

   (* The target of this processing instruction. XML defines this as being   *)
   (* the first token following the markup that begins the processing        *)
   (* instruction.                                                           *)
   method get_target : domString


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_ProcessingInstruction : TProcessingInstruction.t
 end


(* [documentFragment] is a "lightweight" or "minimal" [document] object.  *)
(* It is very common to want to be able to extract a portion of a         *)
(* document's tree or to create a new fragment of a document. Imagine     *)
(* implementing a user command like cut or rearranging a document by      *)
(* moving fragments around. It is desirable to have an object which can   *)
(* hold such fragments and it is quite natural to use a Node for this     *)
(* purpose. While it is true that a [document] object could fulfill this  *)
(* role, a [document] object can potentially be a heavyweight object,     *)
(* depending on the underlying implementation. What is really needed for  *)
(* this is a very lightweight object. [documentFragment] is such an       *)
(* object. Furthermore, various operations -- such as inserting nodes as  *)
(* children of another [node] -- may take [documentFragment] objects as   *)
(* arguments; this results in all the child nodes of the                  *)
(* [documentFragment] being moved to the child list of this node. The     *)
(* children of a [documentFragment] node are zero or more nodes           *)
(* representing the tops of any sub-trees defining the structure of the   *)
(* document. [documentFragment] nodes do not need to be well-formed XML   *)
(* documents (although they do need to follow the rules imposed upon      *)
(* well-formed XML parsed entities, which can have multiple top nodes).   *)
(* For example, a [documentFragment] might have only one child and that   *)
(* child node could be a [text] node. Such a structure model represents   *)
(* neither an HTML document nor a well-formed XML document. When a        *)
(* [documentFragment] is inserted into a [document] (or indeed any other  *)
(* [node] that may take children) the children of the [documentFragment]  *)
(* and not the [documentFragment] itself are inserted into the [node].    *)
(* This makes the [documentFragment] very useful when the user wishes to  *)
(* create nodes that are siblings; the [documentFragment] acts as the     *)
(* parent of these nodes so that the user can use the standard methods    *)
(* from the [node] interface, such as [insertBefore] and [appendChild].   *)
and documentFragment : TDocumentFragment.t ->
  (*** (new documentFragment) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_DocumentFragment : TDocumentFragment.t
 end


(* The [document] interface represents the entire HTML or XML document.   *)
(* Conceptually, it is the root of the document tree, and provides the    *)
(* primary access to the document's data. Since elements, text nodes,     *)
(* comments, processing instructions, etc. cannot exist outside the       *)
(* context of a [document], the [document] interface also contains the    *)
(* factory methods needed to create these objects. The [node] objects     *)
(* created have a [ownerDocument] attribute which associates them with    *)
(* the [document] within whose context they were created.                 *)
and document : TDocument.t ->
  (*** (new document) IS A RESERVED CONSTRUCTOR ; DO NOT USE IN USER CODE ***)
  object
   inherit node

   (*** DOM ATTRIBUTES AS GET/SET METHODS ***)

   (* The Document Type Declaration (see [documentType]) associated with     *)
   (* this document. For HTML documents as well as XML documents without a   *)
   (* document type declaration this returns [None]. The DOM Level 2 does    *)
   (* not support editing the Document Type Declaration. [docType] cannot    *)
   (* be altered in any way, including through the use of methods inherited  *)
   (* from the [node] interface, such as [insertNode] or [removeNode].       *)
   method get_doctype : documentType option

   (* This is a convenience attribute that allows direct access to the       *)
   (* child node that is the root element of the document. For HTML          *)
   (* documents, this is the element with the tagName "HTML".                *)
   method get_documentElement : element

   (* The [domImplementation] object that handles this document. A DOM       *)
   (* application may use objects from multiple implementations.             *)
   method get_implementation : domImplementation


   (*** DOM METHODS ***)

   (* Creates an [attr] of the given name. Note that the [attr] instance     *)
   (* can then be set on an [element] using the [setAttributeNode] method.   *)
   (* To create an attribute with a qualified name and namespace URI, use    *)
   (* the [createAttributeNS] method.                                        *)
   (* Parameters:                                                            *)
   (*  name: The name of the attribute.                                      *)
   (* Result:                                                                *)
   (*  A new [attr] object with the [nodeName] attribute set to [name], and  *)
   (*  [localName], [prefix], and [namespaceURI] set to [None]. The value    *)
   (*  of the attribute is the empty string.                                 *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified name contains an     *)
   (*    illegal character.                                                  *)
   method createAttribute : name:domString -> attr

   (* Creates an attribute of the given qualified name and namespace URI.    *)
   (* HTML-only DOM implementations do not need to implement this method.    *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the attribute to create.           *)
   (*  qualifiedName: The qualified name of the attribute to instantiate.    *)
   (* Result:                                                                *)
   (*  A new [attr] object with the following attributes:                    *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified qualified name       *)
   (*    contains an illegal character. NAMESPACE_ERR: Raised if the         *)
   (*    [qualifiedName] is malformed, if the [qualifiedName] has a prefix   *)
   (*    and the [namespaceURI] is [None], if the [qualifiedName] has a      *)
   (*    prefix that is "xml" and the [namespaceURI] is different from       *)
   (*    "xml-ns;", or if the [qualifiedName] is "xmlns" and the             *)
   (*    [namespaceURI] is different from "xmlns-ns;".                       *)
   method createAttributeNS : namespaceURI:domString option -> qualifiedName:domString -> attr

   (* Creates a [cdataSection] node whose value is the specified string.     *)
   (* Parameters:                                                            *)
   (*  data: The data for the [cdataSection] contents.                       *)
   (* Result:                                                                *)
   (*  The new [cdataSection] object.                                        *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NOT_SUPPORTED_ERR: Raised if this document is an HTML document.     *)
   method createCDATASection : data:domString -> cdataSection

   (* Creates a [comment] node given the specified string.                   *)
   (* Parameters:                                                            *)
   (*  data: The data for the node.                                          *)
   (* Result:                                                                *)
   (*  The new [comment] object.                                             *)
   method createComment : data:domString -> comment

   (* Creates an empty [documentFragment] object.                            *)
   (* Result:                                                                *)
   (*  A new [documentFragment].                                             *)
   method createDocumentFragment : documentFragment

   (* Creates an element of the type specified. Note that the instance       *)
   (* returned implements the [element] interface, so attributes can be      *)
   (* specified directly on the returned object. In addition, if there are   *)
   (* known attributes with default values, [attr] nodes representing them   *)
   (* are automatically created and attached to the element. To create an    *)
   (* element with a qualified name and namespace URI, use the               *)
   (* [createElementNS] method.                                              *)
   (* Parameters:                                                            *)
   (*  tagName: The name of the element type to instantiate. For XML, this   *)
   (*    is case-sensitive. For HTML, the [tagName] parameter may be         *)
   (*    provided in any case, but it must be mapped to the canonical        *)
   (*    uppercase form by the DOM implementation.                           *)
   (* Result:                                                                *)
   (*  A new [element] object with the [nodeName] attribute set to           *)
   (*  [tagName], and [localName], [prefix], and [namespaceURI] set to       *)
   (*  [None].                                                               *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified name contains an     *)
   (*    illegal character.                                                  *)
   method createElement : tagName:domString -> element

   (* Creates an element of the given qualified name and namespace URI.      *)
   (* HTML-only DOM implementations do not need to implement this method.    *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the element to create.             *)
   (*  qualifiedName: The qualified name of the element type to              *)
   (*    instantiate.                                                        *)
   (* Result:                                                                *)
   (*  A new [element] object with the following attributes:                 *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified qualified name       *)
   (*    contains an illegal character. NAMESPACE_ERR: Raised if the         *)
   (*    [qualifiedName] is malformed, if the [qualifiedName] has a prefix   *)
   (*    and the [namespaceURI] is [None], or if the [qualifiedName] has a   *)
   (*    prefix that is "xml" and the [namespaceURI] is different from       *)
   (*    "xml-ns;" .                                                         *)
   method createElementNS : namespaceURI:domString option -> qualifiedName:domString -> element

   (* Creates an [entityReference] object. In addition, if the referenced    *)
   (* entity is known, the child list of the [entityReference] node is made  *)
   (* the same as that of the corresponding [entity] node.                   *)
   (* Parameters:                                                            *)
   (*  name: The name of the entity to reference.                            *)
   (* Result:                                                                *)
   (*  The new [entityReference] object.                                     *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified name contains an     *)
   (*    illegal character. NOT_SUPPORTED_ERR: Raised if this document is    *)
   (*    an HTML document.                                                   *)
   method createEntityReference : name:domString -> entityReference

   (* Creates a [processingInstruction] node given the specified name and    *)
   (* data strings.                                                          *)
   (* Parameters:                                                            *)
   (*  target: The target part of the processing instruction.                *)
   (*  data: The data for the node.                                          *)
   (* Result:                                                                *)
   (*  The new [processingInstruction] object.                               *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    INVALID_CHARACTER_ERR: Raised if the specified target contains an   *)
   (*    illegal character. NOT_SUPPORTED_ERR: Raised if this document is    *)
   (*    an HTML document.                                                   *)
   method createProcessingInstruction : target:domString -> data:domString -> processingInstruction

   (* Creates a [text] node given the specified string.                      *)
   (* Parameters:                                                            *)
   (*  data: The data for the node.                                          *)
   (* Result:                                                                *)
   (*  The new [text] object.                                                *)
   method createTextNode : data:domString -> text

   (* Returns the [element] whose [iD] is given by [elementId]. If no such   *)
   (* element exists, returns [None]. Behavior is not defined if more than   *)
   (* one element has this [iD]. The DOM implementation must have            *)
   (* information that says which attributes are of type ID. Attributes      *)
   (* with the name "ID" are not of type ID unless so defined.               *)
   (* Implementations that do not know whether attributes are of type ID or  *)
   (* not are expected to return [None].                                     *)
   (* Parameters:                                                            *)
   (*  elementId: The unique [id] value for an element.                      *)
   (* Result:                                                                *)
   (*  The matching element.                                                 *)
   method getElementById : elementId:domString -> element option

   (* Returns a [nodeList] of all the [elements] with a given tag name in    *)
   (* the order in which they are encountered in a preorder traversal of     *)
   (* the [document] tree.                                                   *)
   (* Parameters:                                                            *)
   (*  tagname: The name of the tag to match on. The special value "*"       *)
   (*    matches all tags.                                                   *)
   (* Result:                                                                *)
   (*  A new [nodeList] object containing all the matched [elements].        *)
   method getElementsByTagName : tagname:domString -> nodeList

   (* Returns a [nodeList] of all the [elements] with a given local name     *)
   (* and namespace URI in the order in which they are encountered in a      *)
   (* preorder traversal of the [document] tree.                             *)
   (* Parameters:                                                            *)
   (*  namespaceURI: The namespace URI of the elements to match on. The      *)
   (*    special value "*" matches all namespaces.                           *)
   (*  localName: The local name of the elements to match on. The special    *)
   (*    value "*" matches all local names.                                  *)
   (* Result:                                                                *)
   (*  A new [nodeList] object containing all the matched [elements].        *)
   method getElementsByTagNameNS : namespaceURI:domString -> localName:domString -> nodeList

   (* Imports a node from another document to this document. The returned    *)
   (* node has no parent; ([parentNode] is [None]). The source node is not   *)
   (* altered or removed from the original document; this method creates a   *)
   (* new copy of the source node. For all nodes, importing a node creates   *)
   (* a node object owned by the importing document, with attribute values   *)
   (* identical to the source node's [nodeName] and [nodeType], plus the     *)
   (* attributes related to namespaces ([prefix], [localName], and           *)
   (* [namespaceURI]). As in the [cloneNode] operation on a [node], the      *)
   (* source node is not altered. Additional information is copied as        *)
   (* appropriate to the [nodeType], attempting to mirror the behavior       *)
   (* expected if a fragment of XML or HTML source was copied from one       *)
   (* document to another, recognizing that the two documents may have       *)
   (* different DTDs in the XML case. The following list describes the       *)
   (* specifics for each type of node. ATTRIBUTE_NODE The [ownerElement]     *)
   (* attribute is set to [None] and the [specified] flag is set to [true]   *)
   (* on the generated [attr]. The descendants of the source [attr] are      *)
   (* recursively imported and the resulting nodes reassembled to form the   *)
   (* corresponding subtree. Note that the [deep] parameter has no effect    *)
   (* on [attr] nodes; they always carry their children with them when       *)
   (* imported. DOCUMENT_FRAGMENT_NODE If the [deep] option was set to       *)
   (* [true], the descendants of the source element are recursively          *)
   (* imported and the resulting nodes reassembled to form the               *)
   (* corresponding subtree. Otherwise, this simply generates an empty       *)
   (* [documentFragment]. DOCUMENT_NODE [document] nodes cannot be           *)
   (* imported. DOCUMENT_TYPE_NODE [documentType] nodes cannot be imported.  *)
   (* ELEMENT_NODE Specified attribute nodes of the source element are       *)
   (* imported, and the generated [attr] nodes are attached to the           *)
   (* generated [element]. Default attributes are not copied, though if the  *)
   (* document being imported into defines default attributes for this       *)
   (* element name, those are assigned. If the [importNode] [deep]           *)
   (* parameter was set to [true], the descendants of the source element     *)
   (* are recursively imported and the resulting nodes reassembled to form   *)
   (* the corresponding subtree. ENTITY_NODE [entity] nodes can be           *)
   (* imported, however in the current release of the DOM the                *)
   (* [documentType] is readonly. Ability to add these imported nodes to a   *)
   (* [documentType] will be considered for addition to a future release of  *)
   (* the DOM. On import, the [publicId], [systemId], and [notationName]     *)
   (* attributes are copied. If a [deep] import is requested, the            *)
   (* descendants of the the source [entity] are recursively imported and    *)
   (* the resulting nodes reassembled to form the corresponding subtree.     *)
   (* ENTITY_REFERENCE_NODE Only the [entityReference] itself is copied,     *)
   (* even if a [deep] import is requested, since the source and             *)
   (* destination documents might have defined the entity differently. If    *)
   (* the document being imported into provides a definition for this        *)
   (* entity name, its value is assigned. NOTATION_NODE [notation] nodes     *)
   (* can be imported, however in the current release of the DOM the         *)
   (* [documentType] is readonly. Ability to add these imported nodes to a   *)
   (* [documentType] will be considered for addition to a future release of  *)
   (* the DOM. On import, the [publicId] and [systemId] attributes are       *)
   (* copied. Note that the [deep] parameter has no effect on [notation]     *)
   (* nodes since they never have any children. PROCESSING_INSTRUCTION_NODE  *)
   (* The imported node copies its [target] and [data] values from those of  *)
   (* the source node. TEXT_NODE, CDATA_SECTION_NODE, COMMENT_NODE These     *)
   (* three types of nodes inheriting from [characterData] copy their        *)
   (* [data] and [length] attributes from those of the source node.          *)
   (* Parameters:                                                            *)
   (*  importedNode: The node to import.                                     *)
   (*  deep: If [true], recursively import the subtree under the specified   *)
   (*    node; if [false], import only the node itself, as explained above.  *)
   (*    This has no effect on [attr], [entityReference], and [notation]     *)
   (*    nodes.                                                              *)
   (* Result:                                                                *)
   (*  The imported node that belongs to this [document].                    *)
   (* Exceptions Raised:                                                     *)
   (*  DOMException:                                                         *)
   (*    NOT_SUPPORTED_ERR: Raised if the type of node being imported is     *)
   (*    not supported.                                                      *)
   method importNode : importedNode:node -> deep:bool -> node


   (***     RESERVED METHODS, DO NOT USE IN USER CODE     ***)
   (*** required to implement bindings to other libraries ***)
   method as_Document : TDocument.t
 end
;;

(********** Core Module: hand-written pseudo-constructors  **********)

val domString : string -> domString;;

val domImplementation : unit -> domImplementation;;

(********** Events Module: hand-written pseudo-constructors  **********)

val eventListener : callback:(event -> unit) -> eventListener;;
 
(********** Events Module: generated dynamic down-casts **********)

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [mutationEvent] class, a GdomeInit.DOMCastException is raised.  *)
class mutationEvent_of_event :
  < as_Event : [> `Event] GdomeT.t; .. > -> mutationEvent
;;
 
(********** Core Module: generated dynamic down-casts **********)

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [characterData] class, a GdomeInit.DOMCastException is raised.  *)
class characterData_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> characterData
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [attr] class, a GdomeInit.DOMCastException is raised.           *)
class attr_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> attr
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [element] class, a GdomeInit.DOMCastException is raised.        *)
class element_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> element
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [text] class, a GdomeInit.DOMCastException is raised.           *)
class text_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> text
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [comment] class, a GdomeInit.DOMCastException is raised.        *)
class comment_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> comment
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [cdataSection] class, a GdomeInit.DOMCastException is raised.   *)
class cdataSection_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> cdataSection
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [documentType] class, a GdomeInit.DOMCastException is raised.   *)
class documentType_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> documentType
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [notation] class, a GdomeInit.DOMCastException is raised.       *)
class notation_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> notation
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [entity] class, a GdomeInit.DOMCastException is raised.         *)
class entity_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> entity
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [entityReference] class, a GdomeInit.DOMCastException is        *)
(* raised.                                                                *)
class entityReference_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> entityReference
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [processingInstruction] class, a GdomeInit.DOMCastException is  *)
(* raised.                                                                *)
class processingInstruction_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> processingInstruction
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [documentFragment] class, a GdomeInit.DOMCastException is       *)
(* raised.                                                                *)
class documentFragment_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> documentFragment
;;

(* Dynamic cast: if the argument of the constructor is not an instance    *)
(* of the [document] class, a GdomeInit.DOMCastException is raised.       *)
class document_of_node :
  < as_Node : [> `Node] GdomeT.t; .. > -> document
;;
