/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project's home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMChar_hh__
#define __GdomeSmartDOMChar_hh__

namespace GdomeSmartDOM {

#if 1 == 0
#define GMETADOM_CHAR_UNSIGNED    1
#endif

#if 1 == 1
  typedef char Char8;
#else
#error "could not define type Char8"
#endif
  
#if 2 == 1
  typedef char Char16;
#elif 2 == 4
  typedef wchar_t Char16;
#elif 1 == 1
#if 2 == 2
  typedef unsigned short Char16;
#elif 2 == 4
  typedef unsigned int Char16;
#elif 2 == 4
  typedef unsigned long Char16;
#else
#error "could not define type Char16"
#endif
#else
#error "could not define type Char16"
#endif

#if 4 == 1
  typedef char Char32;
#elif 4 == 4
  typedef wchar_t Char32;
#elif 1 == 1
#if 4 == 4
  typedef unsigned int Char32;
#elif 4 == 4
  typedef unsigned long Char32;
#elif 4 == 8
  typedef unsigned long long Char32;
#else
#error "could not define type Char32"
#endif
#else
#error "could not define type Char32"
#endif

}

#if 1 == 1
#include "GdomeSmartDOMTraits.hh"
#endif

#endif // __GdomeSmartDOMChar_hh__

