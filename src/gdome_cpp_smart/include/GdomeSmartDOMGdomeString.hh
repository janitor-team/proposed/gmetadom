/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMGdomeString_hh__
#define __GdomeSmartDOMGdomeString_hh__

#include <string>
#include <iostream>

#include <gdome.h>

#include "GdomeSmartDOMChar.hh"
#include "GdomeSmartDOMUTF8String.hh"
#include "GdomeSmartDOMUTF16String.hh"
#include "GdomeSmartDOMUCS4String.hh"
#include "GdomeSmartDOMDOMString.hh"

namespace GdomeSmartDOM {

  class GdomeString
  {
  public:
    GdomeString(void);
    GdomeString(const char* s);
    GdomeString(const GdomeString& s);
    GdomeString(const UTF8String& s);
    GdomeString(const UTF16String& s);
    GdomeString(const UCS4String& s);
    explicit GdomeString(GdomeDOMString* gdome_str);
    explicit GdomeString(GdomeDOMString* gdome_str, bool) : str(gdome_str) { }
    ~GdomeString();

    bool         null(void) const { return str == 0; }
    bool         empty(void) const;
    unsigned     length(void) const;
    bool         operator==(const GdomeString& s) const;
    bool         operator!=(const GdomeString& s) const { return !(*this == s); }
    GdomeString& operator=(const GdomeString& s);
    GdomeString  operator+(const GdomeString& s) const;
                 operator UTF8String() const;
                 operator UTF16String() const;
                 operator UCS4String() const;
    GdomeDOMString* gdome_str(void) const;
    operator GdomeDOMString*() const { return str; };

    friend std::ostream& operator<<(std::ostream&, const GdomeString&);

    class NullString { };

  private:
    GdomeDOMString* str;
  };

}

#endif // __GdomeSmartDOMGdomeString_hh__
