<?xml version="1.0" encoding="UTF-8"?>

<!--
 This file is part of MetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the author's home page
 http://www.cs.unibo.it/~lpadovan
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"/>

<xsl:param name="prefix" select="''"/>

<xsl:template name="gdomeTypeOfType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'boolean'">GdomeBoolean</xsl:when>
    <xsl:when test="$type = 'unsigned short'">unsigned short</xsl:when>
    <xsl:when test="$type = 'unsigned long'">unsigned long</xsl:when>
    <xsl:when test="$type = 'DOMTimeStamp'">GdomeDOMTimeStamp</xsl:when>
    <xsl:otherwise>Gdome<xsl:value-of select="$type"/>*</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="gdomePrefixOfType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type='DOMImplementation'">di</xsl:when>
    <xsl:when test="$type='DocumentFragment'">df</xsl:when>
    <xsl:when test="$type='Document'">doc</xsl:when>
    <xsl:when test="$type='Node'">n</xsl:when>
    <xsl:when test="$type='NodeList'">nl</xsl:when>
    <xsl:when test="$type='NamedNodeMap'">nnm</xsl:when>
    <xsl:when test="$type='CharacterData'">cd</xsl:when>
    <xsl:when test="$type='Attr'">a</xsl:when>
    <xsl:when test="$type='Element'">el</xsl:when>
    <xsl:when test="$type='Text'">t</xsl:when>
    <xsl:when test="$type='Comment'">c</xsl:when>
    <xsl:when test="$type='CDATASection'">cds</xsl:when>
    <xsl:when test="$type='DocumentType'">dt</xsl:when>
    <xsl:when test="$type='Notation'">not</xsl:when>
    <xsl:when test="$type='Entity'">ent</xsl:when>
    <xsl:when test="$type='EntityReference'">er</xsl:when>
    <xsl:when test="$type='ProcessingInstruction'">pi</xsl:when>
    <xsl:when test="$type='Event'">evnt</xsl:when>
    <xsl:when test="$type='MutationEvent'">mevnt</xsl:when>
    <xsl:when test="$type='EventListener'">evntl</xsl:when>
    <xsl:when test="$type='EventTarget'">n</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="interface" mode="collect">
  <xsl:if test="@inherits">
    <xsl:apply-templates select="document(concat($prefix, concat('/', concat(@inherits, '.xml'))))" mode="inherit"/>
  </xsl:if>
</xsl:template>

<xsl:template match="interface" mode="inherit">
  <xsl:element name="interface">
    <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
  </xsl:element>
  <xsl:apply-templates select="." mode="collect"/>
</xsl:template>

</xsl:stylesheet>

