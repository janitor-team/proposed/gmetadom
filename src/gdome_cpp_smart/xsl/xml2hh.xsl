<?xml version="1.0" encoding="UTF-8"?>

<!--
 This file is part of MetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the author's home page
 http://www.cs.unibo.it/~lpadovan
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>

<xsl:output method="text"/>
<xsl:param name="uriprefix" select="''"/>

<xsl:template match="interface">
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani &lt;luca.padovani@cs.unibo.it&gt;
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to &lt;luca.padovani@cs.unibo.it&gt;
 */

#ifndef __GdomeSmartDOM<xsl:value-of select="@name"/>_hh__
#define __GdomeSmartDOM<xsl:value-of select="@name"/>_hh__
<xsl:if test="@inherits">
#include "GdomeSmartDOM<xsl:value-of select="@inherits"/>.hh"
</xsl:if>
namespace GdomeSmartDOM {

class <xsl:value-of select="@name"/>
<xsl:if test="@inherits"> : public <xsl:value-of select="@inherits"/></xsl:if>
{
protected:
  explicit <xsl:value-of select="@name"/>(Gdome<xsl:value-of select="@name"/>* obj, bool) : <xsl:choose>
  <xsl:when test="@inherits"><xsl:value-of select="@inherits"/>((Gdome<xsl:value-of select="@inherits"/>*) obj, true)</xsl:when>
  <xsl:otherwise>gdome_obj(obj)</xsl:otherwise>
</xsl:choose> { }
public:
  explicit <xsl:value-of select="@name"/>(Gdome<xsl:value-of select="@name"/>* = 0);
  <xsl:apply-templates select="." mode="cast">
    <xsl:with-param name="name" select="@name"/>
  </xsl:apply-templates>  ~<xsl:value-of select="@name"/>();
<xsl:if test="@name = 'EventTarget'">
  EventTarget(const class Node&amp;);
</xsl:if>
<xsl:if test="@name = 'Node'">
  Node(const class EventTarget&amp;);
</xsl:if>

  <xsl:value-of select="@name"/>&amp; operator=(const <xsl:value-of select="@name"/>&amp;);
  bool operator==(const <xsl:value-of select="@name"/>&amp; obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const <xsl:value-of select="@name"/>&amp; obj) const { return !(*this == obj); }
  <!--
<xsl:if test="not(@inherits)">
       operator bool() const { return gdome_obj != 0; }
</xsl:if>
  -->
<xsl:if test="not(@inherits)">
  operator Gdome<xsl:value-of select="@name"/>*() const { return reinterpret_cast&lt;Gdome<xsl:value-of select="@name"/>*&gt;(gdome_obj); }
</xsl:if>
  
<xsl:apply-templates select="group"/>
  // Attributes  
  <xsl:apply-templates select="attribute"/>
  // Methods
  <xsl:apply-templates select="method"/>
  // Friend classes
  <xsl:if test="not(@name = 'Document')">friend class Document;
  </xsl:if>
  <xsl:choose>
    <xsl:when test="@name = 'NodeList'">friend class Node;
  friend class Element;
</xsl:when>
    <xsl:when test="@name = 'Node'">friend class NodeList;
  friend class CharacterData;
  friend class Attr;
  friend class Element;
  friend class Text;
  friend class Comment;
  friend class CDATASection;
  friend class DocumentType;
  friend class Notation;
  friend class ProcessingInstruction;
  friend class Entity;
  friend class EntityReference;
  friend class DocumentFragment;
  friend class NamedNodeMap;
  friend class EventTarget;
  friend class MutationEvent;
  friend class DOMImplementation;
    </xsl:when>
    <xsl:when test="@name = 'Element'">friend class Attr;
</xsl:when>
    <xsl:when test="@name = 'Attr'">
  friend class Element;
    </xsl:when>
    <xsl:when test="@name = 'NamedNodeMap'">
  friend class Node;
  friend class DocumentType;
    </xsl:when>
    <xsl:when test="@name = 'Document'">friend class Node;
  friend class DOMImplementation;
</xsl:when>
    <xsl:when test="@name = 'DocumentType'">friend class DOMImplementation;
</xsl:when>
    <xsl:when test="@name = 'Event'">
  friend class EventTarget;
  friend class MutationEvent;
    </xsl:when>
    <xsl:when test="@name = 'EventTarget'">friend class Node;
  friend class Event;
</xsl:when>
  </xsl:choose>

  <xsl:if test="not(@inherits)">  Gdome<xsl:value-of select="@name"/>* gdome_object(void) const;
  </xsl:if>
  <xsl:if test="@name = 'Node'">  void* id(void) const;</xsl:if>

<xsl:if test="not(@inherits)">
protected:
  Gdome<xsl:value-of select="@name"/>* gdome_obj;
</xsl:if>};

}

#endif // __GdomeSmartDOM<xsl:value-of select="@name"/>_hh__

</xsl:template>

<xsl:template match="group">  enum <xsl:value-of select="@name"/> {
    <xsl:apply-templates select="constant"/>
  };
</xsl:template>

<xsl:template match="constant">
  <xsl:value-of select="@name"/> = <xsl:value-of select="@value"/>
  <xsl:if test="position() &lt; last()">,
    </xsl:if>
</xsl:template>

<xsl:template match="attribute">
  <xsl:call-template name="returnType">
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template> get_<xsl:value-of select="@name"/>(void) const<!--throw(<xsl:apply-templates select="getraises/exception"/>)-->;
  <xsl:if test="not(@readonly)">void set_<xsl:value-of select="@name"/>(const <xsl:call-template name="paramType"><xsl:with-param name="type" select="@type"/></xsl:call-template>) const<!--throw(<xsl:apply-templates select="setraises/exception"/>)-->;
  </xsl:if>
</xsl:template>

<xsl:template match="method">
  <xsl:call-template name="returnType"><xsl:with-param name="type" select="returns/@type"/></xsl:call-template><xsl:value-of select="' '"/><xsl:value-of select="@name"/>(<xsl:apply-templates select="parameters"/>) const<!--throw(<xsl:apply-templates select="raises/exception"/>)-->;
  </xsl:template>

<xsl:template match="parameters">
  <xsl:choose>
    <xsl:when test="count(param) = 0">void</xsl:when>
    <xsl:otherwise><xsl:apply-templates select="param"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="param">
  <xsl:if test="@attr = 'in'">const </xsl:if>
  <xsl:call-template name="paramType"><xsl:with-param name="type" select="@type"/></xsl:call-template>
  <xsl:value-of select="' '"/>
  <xsl:value-of select="@name"/>
  <xsl:if test="position() &lt; last()">, </xsl:if>
</xsl:template>

<xsl:template name="paramType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type='void'">void</xsl:when>
    <xsl:when test="$type='boolean'">bool</xsl:when>
    <xsl:when test="$type='unsigned short'">unsigned short</xsl:when>
    <xsl:when test="$type='unsigned long'">unsigned long</xsl:when>
    <xsl:when test="$type='DOMTimeStamp'">DOMTimeStamp</xsl:when>
    <xsl:when test="$type='DOMString'">class GdomeString&amp;</xsl:when>
    <xsl:otherwise>class <xsl:value-of select="$type"/>&amp;</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="returnType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'void'">void</xsl:when>
    <xsl:when test="$type='boolean'">bool</xsl:when>
    <xsl:when test="$type='unsigned short'">unsigned short</xsl:when>
    <xsl:when test="$type='unsigned long'">unsigned long</xsl:when>
    <xsl:when test="$type='DOMTimeStamp'">DOMTimeStamp</xsl:when>
    <xsl:when test="$type='DOMString'">class GdomeString</xsl:when>
    <xsl:otherwise>class <xsl:value-of select="$type"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="exception">
  <xsl:value-of select="@name"/>
  <xsl:if test="position() &lt; last()">, </xsl:if>
</xsl:template>

<xsl:template match="interface" mode="cast">
  <xsl:param name="name" select="''"/>
  <xsl:value-of select="'  '"/>
  <xsl:value-of select="$name"/>(const <xsl:value-of select="@name"/>&amp;);
<xsl:if test="@inherits">
    <xsl:apply-templates select="document(concat($uriprefix, concat('/', concat(@inherits, '.xml'))))/interface" mode="cast">
      <xsl:with-param name="name" select="$name"/>
    </xsl:apply-templates>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>

