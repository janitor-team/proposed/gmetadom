
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>

#include <gdome-events.h>
#include "GdomeSmartDOMHelper.hh"


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Event::Event(GdomeEvent* obj)
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_evnt_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Event failed");
  }
}

Event::Event(const Event& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_evnt_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "Event::Event casting from Event");
  }
}

Event& Event::operator=(const Event& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_evnt_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Event failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_evnt_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Event failed");
  }

  return *this;
}

Event::~Event()
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_evnt_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Event failed");
    gdome_obj = 0;
  }

}


GdomeEvent* Event::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_evnt_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "Event::gdome_object");
  }

  return gdome_obj;
}
GdomeString Event::get_type() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_evnt_type((GdomeEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_type");
  return res_;
}

EventTarget Event::get_target() const
{
  GdomeException exc_ = 0;
  EventTarget res_(gdome_evnt_target((GdomeEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_target");
  return res_;
}

EventTarget Event::get_currentTarget() const
{
  GdomeException exc_ = 0;
  EventTarget res_(gdome_evnt_currentTarget((GdomeEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_currentTarget");
  return res_;
}

unsigned short Event::get_eventPhase() const
{
  GdomeException exc_ = 0;
  unsigned short res_ = gdome_evnt_eventPhase((GdomeEvent*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_eventPhase");
  return res_;
}

bool Event::get_bubbles() const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_evnt_bubbles((GdomeEvent*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_bubbles");
  return res_;
}

bool Event::get_cancelable() const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_evnt_cancelable((GdomeEvent*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_cancelable");
  return res_;
}

DOMTimeStamp Event::get_timeStamp() const
{
  GdomeException exc_ = 0;
  DOMTimeStamp res_ = gdome_evnt_timeStamp((GdomeEvent*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Event::get_timeStamp");
  return res_;
}

void Event::stopPropagation() const
{
  GdomeException exc_ = 0;
  gdome_evnt_stopPropagation((GdomeEvent*) gdome_obj, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Event::stopPropagation");
  
}

void Event::preventDefault() const
{
  GdomeException exc_ = 0;
  gdome_evnt_preventDefault((GdomeEvent*) gdome_obj, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Event::preventDefault");
  
}

void Event::initEvent(const GdomeString& eventTypeArg, const bool canBubbleArg, const bool cancelableArg) const
{
  GdomeException exc_ = 0;
  gdome_evnt_initEvent((GdomeEvent*) gdome_obj, static_cast<GdomeDOMString*>(eventTypeArg), canBubbleArg, cancelableArg, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Event::initEvent");
  
}



}

