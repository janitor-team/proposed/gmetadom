
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Node::Node(GdomeNode* obj)
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Node failed");
  }
}


Node::Node(const EventTarget& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Node failed");
  }
}
Node::Node(const Node& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "Node::Node casting from Node");
  }
}

Node& Node::operator=(const Node& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Node failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Node failed");
  }

  return *this;
}

Node::~Node()
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Node failed");
    gdome_obj = 0;
  }

}


GdomeNode* Node::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "Node::gdome_object");
  }

  return gdome_obj;
}

extern "C" void* gdome_xml_n_get_xmlNode(GdomeNode*);
void* Node::id() const
{
  if (gdome_obj != 0) return gdome_xml_n_get_xmlNode(gdome_obj);
  else return 0;
}
GdomeString Node::get_nodeName() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_n_nodeName((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_nodeName");
  return res_;
}

GdomeString Node::get_nodeValue() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_n_nodeValue((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_nodeValue");
  return res_;
}

void
Node::set_nodeValue(const GdomeString& v) const
{
  GdomeException exc_ = 0;
  gdome_n_set_nodeValue((GdomeNode*) gdome_obj, static_cast<GdomeDOMString*>(v), &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Node::set_nodeValue");
}

unsigned short Node::get_nodeType() const
{
  GdomeException exc_ = 0;
  unsigned short res_ = gdome_n_nodeType((GdomeNode*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_nodeType");
  return res_;
}

Node Node::get_parentNode() const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_parentNode((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_parentNode");
  return res_;
}

NodeList Node::get_childNodes() const
{
  GdomeException exc_ = 0;
  NodeList res_(gdome_n_childNodes((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_childNodes");
  return res_;
}

Node Node::get_firstChild() const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_firstChild((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_firstChild");
  return res_;
}

Node Node::get_lastChild() const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_lastChild((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_lastChild");
  return res_;
}

Node Node::get_previousSibling() const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_previousSibling((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_previousSibling");
  return res_;
}

Node Node::get_nextSibling() const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_nextSibling((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_nextSibling");
  return res_;
}

NamedNodeMap Node::get_attributes() const
{
  GdomeException exc_ = 0;
  NamedNodeMap res_(gdome_n_attributes((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_attributes");
  return res_;
}

Document Node::get_ownerDocument() const
{
  GdomeException exc_ = 0;
  Document res_(gdome_n_ownerDocument((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_ownerDocument");
  return res_;
}

GdomeString Node::get_namespaceURI() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_n_namespaceURI((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_namespaceURI");
  return res_;
}

GdomeString Node::get_prefix() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_n_prefix((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_prefix");
  return res_;
}

void
Node::set_prefix(const GdomeString& v) const
{
  GdomeException exc_ = 0;
  gdome_n_set_prefix((GdomeNode*) gdome_obj, static_cast<GdomeDOMString*>(v), &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Node::set_prefix");
}

GdomeString Node::get_localName() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_n_localName((GdomeNode*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Node::get_localName");
  return res_;
}

Node Node::insertBefore(const Node& newChild, const Node& refChild) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_insertBefore((GdomeNode*) gdome_obj, (GdomeNode*) newChild.gdome_obj, (GdomeNode*) refChild.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::insertBefore");
  return res_;
}

Node Node::replaceChild(const Node& newChild, const Node& oldChild) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_replaceChild((GdomeNode*) gdome_obj, (GdomeNode*) newChild.gdome_obj, (GdomeNode*) oldChild.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::replaceChild");
  return res_;
}

Node Node::removeChild(const Node& oldChild) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_removeChild((GdomeNode*) gdome_obj, (GdomeNode*) oldChild.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::removeChild");
  return res_;
}

Node Node::appendChild(const Node& newChild) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_appendChild((GdomeNode*) gdome_obj, (GdomeNode*) newChild.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::appendChild");
  return res_;
}

bool Node::hasChildNodes() const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_n_hasChildNodes((GdomeNode*) gdome_obj, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::hasChildNodes");
  return res_;
}

Node Node::cloneNode(const bool deep) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_n_cloneNode((GdomeNode*) gdome_obj, deep, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::cloneNode");
  return res_;
}

void Node::normalize() const
{
  GdomeException exc_ = 0;
  gdome_n_normalize((GdomeNode*) gdome_obj, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::normalize");
  
}

bool Node::isSupported(const GdomeString& feature, const GdomeString& version) const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_n_isSupported((GdomeNode*) gdome_obj, static_cast<GdomeDOMString*>(feature), static_cast<GdomeDOMString*>(version), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::isSupported");
  return res_;
}

bool Node::hasAttributes() const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_n_hasAttributes((GdomeNode*) gdome_obj, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Node::hasAttributes");
  return res_;
}



}

