
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Text::Text(GdomeText* obj) : CharacterData((GdomeCharacterData*) obj) { }

Text::Text(const Text& obj) : CharacterData(obj) { }

Text::Text(const CharacterData& obj) : CharacterData((GdomeCharacterData*) gdome_cast_t(obj.gdome_obj)) { }

Text::Text(const Node& obj) : CharacterData((GdomeCharacterData*) gdome_cast_t(obj.gdome_obj)) { }

Text& Text::operator=(const Text& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_t_unref((GdomeText*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Text failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_t_ref((GdomeText*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Text failed");
  }

  return *this;
}

Text::~Text()
{
}

Text Text::splitText(const unsigned long offset) const
{
  GdomeException exc_ = 0;
  Text res_(gdome_t_splitText((GdomeText*) gdome_obj, offset, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Text::splitText");
  return res_;
}



}

