
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMMutationEvent_hh__
#define __GdomeSmartDOMMutationEvent_hh__

#include "GdomeSmartDOMEvent.hh"

namespace GdomeSmartDOM {

class MutationEvent : public Event
{
protected:
  explicit MutationEvent(GdomeMutationEvent* obj, bool) : Event((GdomeEvent*) obj, true) { }
public:
  explicit MutationEvent(GdomeMutationEvent* = 0);
    MutationEvent(const MutationEvent&);
  MutationEvent(const Event&);
  ~MutationEvent();
MutationEvent& operator=(const MutationEvent&);
  bool operator==(const MutationEvent& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const MutationEvent& obj) const { return !(*this == obj); }
    enum attrChangeType {
    MODIFICATION = 1,
    ADDITION = 2,
    REMOVAL = 3
  };

  // Attributes  
  class Node get_relatedNode(void) const;
  class GdomeString get_prevValue(void) const;
  class GdomeString get_newValue(void) const;
  class GdomeString get_attrName(void) const;
  unsigned short get_attrChange(void) const;
  
  // Methods
  void initMutationEvent(const class GdomeString& typeArg, const bool canBubbleArg, const bool cancelableArg, const class Node& relatedNodeArg, const class GdomeString& prevValueArg, const class GdomeString& newValueArg, const class GdomeString& attrNameArg, const unsigned short attrChangeArg) const;
  
  // Friend classes
  friend class Document;
  };

}

#endif // __GdomeSmartDOMMutationEvent_hh__

