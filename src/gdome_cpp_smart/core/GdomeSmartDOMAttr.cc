
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Attr::Attr(GdomeAttr* obj) : Node((GdomeNode*) obj) { }

Attr::Attr(const Attr& obj) : Node(obj) { }

Attr::Attr(const Node& obj) : Node((GdomeNode*) gdome_cast_a(obj.gdome_obj)) { }

Attr& Attr::operator=(const Attr& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_a_unref((GdomeAttr*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Attr failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_a_ref((GdomeAttr*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Attr failed");
  }

  return *this;
}

Attr::~Attr()
{
}

GdomeString Attr::get_name() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_a_name((GdomeAttr*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Attr::get_name");
  return res_;
}

bool Attr::get_specified() const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_a_specified((GdomeAttr*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Attr::get_specified");
  return res_;
}

GdomeString Attr::get_value() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_a_value((GdomeAttr*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Attr::get_value");
  return res_;
}

void
Attr::set_value(const GdomeString& v) const
{
  GdomeException exc_ = 0;
  gdome_a_set_value((GdomeAttr*) gdome_obj, static_cast<GdomeDOMString*>(v), &exc_);
  if (exc_ != 0) throw DOMException(exc_, "Attr::set_value");
}

Element Attr::get_ownerElement() const
{
  GdomeException exc_ = 0;
  Element res_(gdome_a_ownerElement((GdomeAttr*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Attr::get_ownerElement");
  return res_;
}



}

