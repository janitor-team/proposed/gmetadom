/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMDOMException_hh__
#define __GdomeSmartDOMDOMException_hh__

#include "GdomeSmartDOMGdomeString.hh"

namespace GdomeSmartDOM {

class DOMException {
public:
  enum DOMExceptionCode {
    INDEX_SIZE_ERR            = 1,
    DOMSTRING_SIZE_ERR        = 2,
    HIERARCHY_REQUEST_ERR     = 3,
    WRONG_DOCUMENT_ERR        = 4,
    INVALID_CHARACTER_ERR     = 5,
    NO_DATA_ALLOWED_ERR       = 6,
    NO_MODIFICATION_ALLOWED_ERR = 7,
    NOT_FOUND_ERR             = 8,
    NOT_SUPPORTED_ERR         = 9,
    INUSE_ATTRIBUTE_ERR       = 10,
    INVALID_STATE_ERR         = 11,
    SYNTAX_ERR                = 12,
    INVALID_MODIFICATION_ERR  = 13,
    NAMESPACE_ERR             = 14,
    INVALID_ACCESS_ERR        = 15 
  };

  DOMException(unsigned short c);
  DOMException(unsigned short c, const GdomeString& m);
  DOMException(const DOMException&);

  DOMException& operator=(const DOMException&);

  unsigned short code;
  GdomeString    msg;
};

}

#endif // __GdomeSmartDOMDOMException_hh__

