
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

DocumentType::DocumentType(GdomeDocumentType* obj) : Node((GdomeNode*) obj) { }

DocumentType::DocumentType(const DocumentType& obj) : Node(obj) { }

DocumentType::DocumentType(const Node& obj) : Node((GdomeNode*) gdome_cast_dt(obj.gdome_obj)) { }

DocumentType& DocumentType::operator=(const DocumentType& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_dt_unref((GdomeDocumentType*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to DocumentType failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_dt_ref((GdomeDocumentType*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to DocumentType failed");
  }

  return *this;
}

DocumentType::~DocumentType()
{
}

GdomeString DocumentType::get_name() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_dt_name((GdomeDocumentType*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "DocumentType::get_name");
  return res_;
}

NamedNodeMap DocumentType::get_entities() const
{
  GdomeException exc_ = 0;
  NamedNodeMap res_(gdome_dt_entities((GdomeDocumentType*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "DocumentType::get_entities");
  return res_;
}

NamedNodeMap DocumentType::get_notations() const
{
  GdomeException exc_ = 0;
  NamedNodeMap res_(gdome_dt_notations((GdomeDocumentType*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "DocumentType::get_notations");
  return res_;
}

GdomeString DocumentType::get_publicId() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_dt_publicId((GdomeDocumentType*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "DocumentType::get_publicId");
  return res_;
}

GdomeString DocumentType::get_systemId() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_dt_systemId((GdomeDocumentType*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "DocumentType::get_systemId");
  return res_;
}

GdomeString DocumentType::get_internalSubset() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_dt_internalSubset((GdomeDocumentType*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "DocumentType::get_internalSubset");
  return res_;
}



}

