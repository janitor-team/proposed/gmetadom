/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <config.h>

#include <gdome.h>
#include <iostream>

#include "GdomeSmartDOM.hh"

using namespace GdomeSmartDOM;

#if 0
std::ostream& operator<<(std::ostream& o, const GdomeSmartDOM::GdomeString& s)
{
  if (s.null()) o << "(null)";
  else o << (char*) s.buffer();
  return o;
}
#endif

int
main(int argc, char* argv[])
{
  GdomeString hello("hello");
  std::cout << hello << std::endl;

  Document doc;
  UTF8String utf8_hi = hello;
  UCS4String ucs4_hi = hello;

  std::string c_hi1 = utf8_hi;

  if (doc) ;
  if (!doc) ;
  if (doc == 0) ;
  if (doc != 0) ;

  GdomeString ns;
  if (ns) std::cout << "WARNING: GdomeString: null != null" << std::endl;
  if (!ns) std::cout << "OK: GdomeString: null == null" << std::endl;

  return 0;
}

