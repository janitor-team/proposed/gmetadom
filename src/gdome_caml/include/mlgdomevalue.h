/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __mlgdomevalue_h__
#define __mlgdomevalue_h__

#include <gdome.h>

#define         Val_option_ptr(p,f) (((p) == NULL) ? Val_unit : mlgdome_some(f(p)))
#define         Val_unsigned_short(n) Val_int(n)
#define         Val_unsigned_long(n) Val_int(n)
value           Val_DOMString(GdomeDOMString*);
value           Val_DOMImplementation(GdomeDOMImplementation*);
/* from the Core module */
value           Val_Attr(GdomeAttr*);
value           Val_CDATASection(GdomeCDATASection*);
value           Val_CharacterData(GdomeCharacterData*);
value           Val_Comment(GdomeComment*);
value           Val_Document(GdomeDocument*);
value           Val_DocumentFragment(GdomeDocumentFragment*);
value           Val_DocumentType(GdomeDocumentType*);
value           Val_Element(GdomeElement*);
value           Val_Entity(GdomeEntity*);
value           Val_EntityReference(GdomeEntityReference*);
value           Val_NamedNodeMap(GdomeNamedNodeMap*);
value           Val_Node(GdomeNode*);
value           Val_NodeList(GdomeNodeList*);
value           Val_Notation(GdomeNotation*);
value           Val_ProcessingInstruction(GdomeProcessingInstruction*);
value           Val_Text(GdomeText*);
/* from the Events module */
value           Val_Event(GdomeEvent*);
value           Val_MutationEvent(GdomeMutationEvent*);
value           Val_EventTarget(GdomeEventTarget*);
/* from ml_misc.c */
value           mlgdome_some(value v);
void            throw_exception(int exc, const char* msg);
void            throw_impl_exception(const char* msg);
void            throw_cast_exception(const char* msg);

#define         ptr_val_option(v,f) (((v) == Val_unit) ? NULL : f(Field(v,0)))
#define         unsigned_short_val(v) Int_val(v)
#define         unsigned_long_val(v) Int_val(v)
GdomeDOMString* DOMString_val(value);
GdomeDOMImplementation*	DOMImplementation_val(value);
/* from the Core module */
GdomeAttr*      Attr_val(value);
GdomeCDATASection* CDATASection_val(value);
GdomeCharacterData* CharacterData_val(value);
GdomeComment*   Comment_val(value);
GdomeDocument*  Document_val(value);
GdomeDocumentType* DocumentType_val(value);
GdomeDocumentFragment* DocumentFragment_val(value);
GdomeElement*   Element_val(value);
GdomeEntity*    Entity_val(value);
GdomeEntityReference* EntityReference_val(value);
GdomeNamedNodeMap* NamedNodeMap_val(value);
GdomeNode*      Node_val(value);
GdomeNodeList*  NodeList_val(value);
GdomeNotation*  Notation_val(value);
GdomeProcessingInstruction* ProcessingInstruction_val(value);
GdomeText*      Text_val(value);
/* from the Events module */
GdomeEvent*     Event_val(value);
GdomeMutationEvent* MutationEvent_val(value);
GdomeEventTarget* EventTarget_val(value);
GdomeEventListener* EventListener_val(value);

#endif /* __mlgdomevalue_h__ */
