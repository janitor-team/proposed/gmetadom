dnl Process this file with autoconf to produce a configure script.
AC_INIT
AC_CONFIG_SRCDIR([src/shared/DOMString.hh.in])

PACKAGE=gmetadom
VERSION=0.2.6
GMETADOM_VERSION_INFO=`echo $VERSION | awk -F. '{ printf "%d:%d:%d", $1+$2, $3, $2 }'`
AC_SUBST(GMETADOM_VERSION_INFO)

AC_ARG_ENABLE(
	profile,
	[  --enable-profile[=ARG]  include profiling information [default=no]],
	profile=$enableval,
	profile=no
)

AC_ARG_ENABLE(
	debug,
	[  --enable-debug[=ARG]    include debugging debug [default=yes]],
	enable_debug=$enableval,
	enable_debug=yes
)

if test "x$enable_debug" = "xyes"; then
   AC_DEFINE(ENABLE_DEBUG,,[Define to 1 if you want to enable validity checks while running])
fi

AC_ARG_WITH(modules,
	[  --with-modules=[module list|all]   Specify the set of modules to be built [default=all]],
	GMETADOM_MODULES=$withval,
	GMETADOM_MODULES=all
)

LIBXML_PREFIX=""
AC_ARG_WITH(libxml-prefix,
        [  --with-libxml-prefix=[PFX]         Specify location of libxml],
	LIBXML_PREFIX=$withval
)
        
AC_ARG_WITH(libxml-include-prefix,
        [  --with-libxml-include-prefix=[PFX] Specify location of libxml headers],
        LIBXML_CFLAGS="-I$withval"
)

AC_ARG_WITH(libxml-libs-prefix,
        [  --with-libxml-libs-prefix=[PFX]    Specify location of libxml libs],
        LIBXML_LIBS="-L$withval -lxml2 -lz"
)

AC_ARG_WITH(ocaml-include-prefix,
	[  --with-ocaml-include-prefix=[PFX]  Specify location of Ocaml include files],
	OCAML_INCLUDE_PREFIX="-I$withval",
	OCAML_INCLUDE_PREFIX=no
)
	
AC_ARG_WITH(ocaml-lib-prefix,
	[ --with-ocaml-with-prefix=[PFX]      Specify location of Ocaml libraries],
	OCAML_LIB_PREFIX=$withval,
	OCAML_LIB_PREFIX=no
)

# AC_CONFIG_HEADERS([config.h])
AM_INIT_AUTOMAKE($PACKAGE, $VERSION)
AM_CONFIG_HEADER(config.h)

AH_TOP([
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef config_h
#define config_h
])

AH_BOTTOM([
#endif /* config_h */
])

AC_PROG_CC
AC_PROG_CXX
AC_PROG_INSTALL
AC_ISC_POSIX
AC_HEADER_STDC([])
dnl AC_ARG_PROGRAM
AC_C_BIGENDIAN
AC_C_CHAR_UNSIGNED
AC_CHECK_SIZEOF(unsigned char, 1)
AC_CHECK_SIZEOF(unsigned short, 2)
AC_CHECK_SIZEOF(unsigned int, 4)
AC_CHECK_SIZEOF(unsigned long, 4)
AC_CHECK_SIZEOF(unsigned long long, 8)
AC_CHECK_SIZEOF(wchar_t, 8)

if test "x$ac_cv_c_char_unsigned" = "xyes"; then
   AC_SUBST(CHAR_UNSIGNED, "1")
else
   AC_SUBST(CHAR_UNSIGNED, "0")
fi
AC_SUBST(GMETADOM_SIZEOF_CHAR, "$ac_cv_sizeof_unsigned_char")
AC_SUBST(GMETADOM_SIZEOF_SHORT, "$ac_cv_sizeof_unsigned_short")
AC_SUBST(GMETADOM_SIZEOF_INT, "$ac_cv_sizeof_unsigned_int")
AC_SUBST(GMETADOM_SIZEOF_LONG, "$ac_cv_sizeof_unsigned_long")
AC_SUBST(GMETADOM_SIZEOF_LONG_LONG, "$ac_cv_sizeof_unsigned_long_long")
AC_SUBST(GMETADOM_SIZEOF_WCHAR_T, "$ac_cv_sizeof_wchar_t")

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

AM_PROG_LIBTOOL

dnl PKG_CHECK_MODULES(GLIB, glib)
dnl AC_SUBST(GLIB_CFLAGS)
dnl AC_SUBST(GLIB_LIBS)

PKG_CHECK_MODULES(GDOME, gdome2 >= 0.8.0)
AC_SUBST(GDOME_CFLAGS)
AC_SUBST(GDOME_LIBS)

AC_CHECK_PROG(HAVE_SED, sed, yes, no)
if test $HAVE_SED = no; then
	AC_MSG_WARN(sed is needed to generate some source files, if you delete them)
else
	SED=sed
	AC_SUBST(SED)
fi
AM_CONDITIONAL(HAVE_SED_COND, test $HAVE_SED = yes)

AC_CHECK_PROG(HAVE_XSLTPROC, xsltproc, yes, no)
if test $HAVE_XSLTPROC = no; then
	AC_MSG_WARN(xsltproc is needed to generate many source files, if you delete them)
else
	XSLTPROC=xsltproc
	AC_SUBST(XSLTPROC)
fi
AM_CONDITIONAL(HAVE_XSLTPROC_COND, test $HAVE_XSLTPROC = yes)

AC_LANG_PUSH([C++])
AC_MSG_CHECKING(whether iconv has a const argument)
AC_TRY_COMPILE(
        [#include <iconv.h>],
        [
                const char* inbuf;
                iconv_t     cd;
                iconv(cd, &inbuf, 0, 0, 0);
        ],
        [
		AC_DEFINE(ICONV_CONST,,[Define to 1 if iconv has a const second argument])
                AC_MSG_RESULT(yes)
        ],
        [
                AC_MSG_RESULT(no)
        ]
)

AC_MSG_CHECKING(for the name of ios class)
AC_TRY_COMPILE(
	[#include <iostream>],
	[
		ios::fmtflags a;
	],
	[
		AC_DEFINE(HAVE_IOS_CLASS,,[Define to 1 if fmtflags is inside ios class])
		AC_MSG_RESULT(apparently ios)
	],
	[
		AC_MSG_RESULT(hopefully ios_base with explicit std namespace)
	]
)

AC_MSG_CHECKING([whether the C++ compiler supports the standard character traits])
AC_TRY_COMPILE(
	[#include <string>],
	[
		std::basic_string<char> s1;
		std::basic_string<wchar_t> s2;
	],
	[
		AC_MSG_RESULT(yes)
		STD_TRAITS="yes"
	],
	[
		AC_MSG_RESULT(no)
		STD_TRAITS="no"
	]
)
AC_SUBST(STD_TRAITS)

AC_MSG_CHECKING([whether the C++ compiler supports non-standard character traits])
AC_TRY_COMPILE(
	[#include <string>],
	[
		std::basic_string<unsigned short> s1;
		std::basic_string<unsigned int> s2;
		std::basic_string<unsigned long> s3;
	],
	[
		AC_MSG_RESULT(yes)
		ALL_TRAITS="1"
	],
	[
		AC_MSG_RESULT(no)
		ALL_TRAITS="0"
	]
)
AC_SUBST(ALL_TRAITS)

AC_MSG_CHECKING([whether the C++ compiler supports template specialization])
AC_TRY_COMPILE(
	[
		#include <string>

		namespace std
		{
			template<> struct char_traits<unsigned short> { };
		}
	],
	[ ],
	[
		AC_MSG_RESULT(yes)
		TRAIT_SPEC="1"
	],
	[
		AC_MSG_RESULT(no)
		TRAIT_SPEC="0"
	]
)
AC_SUBST(TRAIT_SPEC)

AC_LANG_POP([C++])

AC_CHECK_PROG(HAVE_ICONV, iconv, yes, no)
if test $HAVE_ICONV = "yes"; then
	if iconv -t UTF-8 -f UTF-8 </dev/null >/dev/null 2>&1; then
		ICONV_UTF8="UTF-8"
	else
		AC_MSG_WARN([iconv UTF-8 is not supported, this may result in failed assertions])
		ICONV_UTF8="UTF-8"
	fi

	if iconv -t UTF-16BE -f UTF-16BE </dev/null >/dev/null 2>&1; then
		ICONV_UTF16BE="UTF-16BE"
	elif iconv -t UTF-16 -f UTF-16 </dev/null >/dev/null 2>&1; then
		AC_MSG_WARN(assuming that iconv UTF-16 is bigendian)
		ICONV_UTF16BE="UTF-16"
	else
		AC_MSG_WARN([iconv UTF-16 is not supported, this may result in failed assertions])
		ICONV_UTF16BE="UTF-16"
	fi

	if iconv -t UTF-16LE -f UTF-16LE </dev/null >/dev/null 2>&1; then
		ICONV_UTF16LE="UTF-16LE"
	else
		ICONV_UTF16LE="no"
	fi

	if iconv -t UCS-4BE -f UCS-4BE </dev/null >/dev/null 2>&1; then
		ICONV_UCS4BE="UCS-4BE"
	elif iconv -t UCS-4 -f UCS-4 </dev/null >/dev/null 2>&1; then
		AC_MSG_WARN(assuming that iconv UCS-4 is bigendian)
		ICONV_UCS4BE="UCS-4"
	else
		AC_MSG_WARN([iconv UCS-4 is not supported, this may result in failed assertions])
		ICONV_UCS4BE="UCS-4"
	fi

	if iconv -t UCS-4LE -f UCS-4LE </dev/null >/dev/null 2>&1; then
		ICONV_UCS4LE="UCS-4LE"
	else
		ICONV_UCS4LE="no"
	fi
else
	AC_MSG_WARN([iconv not found, assuming that UTF-8, UTF-16, UCS-4 exist, this may result in failed assertions])
	ICONV_UTF8="UTF-8"
	ICONV_UTF16BE="UTF-16"
	ICONV_UTF16LE="no"
	ICONV_UCS4BE="UCS-4"
	ICONV_UCS4LE="no"
fi
AC_DEFINE_UNQUOTED(ICONV_UTF8, "$ICONV_UTF8", iconv UTF-8 encoding)
AC_DEFINE_UNQUOTED(ICONV_UTF16BE, "$ICONV_UTF16BE", iconv UTF-16 encoding big endian)
if test $ICONV_UTF16LE != "no"; then
	AC_DEFINE_UNQUOTED(ICONV_UTF16LE, "$ICONV_UTF16LE", iconv UTF-16 encoding little endian)
fi
AC_DEFINE_UNQUOTED(ICONV_UCS4BE, "$ICONV_UCS4BE", iconv UCS-4 encoding big endian)
if test $ICONV_UCS4LE != "no"; then
	AC_DEFINE_UNQUOTED(ICONV_UCS4LE, "$ICONV_UCS4LE", iconv UCS-4 encoding little endian)
fi

AC_CHECK_PROGS(OCAMLC, ocamlc.opt ocamlc, no)
if test "$OCAMLC" = no; then
	AC_MSG_WARN(Ocaml related modules won't be compiled since ocamlc was not found)
	HAVE_OCAMLC=no
else
	HAVE_OCAMLC=yes
	OCAML_LIB=`$OCAMLC -v | tail -1 | cut -d ':' -f2 | tr -d ' '`

	if test $OCAML_INCLUDE_PREFIX = no; then
		OCAML_CFLAGS=-I$OCAML_LIB
	else
		OCAML_CFLAGS=$OCAML_INCLUDE_PREFIX
	fi

	if test $OCAML_LIB_PREFIX = no; then
		if test "x$prefix" != "xNONE" -a "x$srcdir" = "x.."; then
			OCAML_LIB_PREFIX=$prefix/lib/ocaml
			AC_MSG_WARN(if you are not doing a distcheck Ocaml files will be installed in $OCAML_LIB_PREFIX)
		else
			OCAML_LIB_PREFIX=$OCAML_LIB
		fi
	fi

	AC_SUBST(OCAML_CFLAGS)
	AC_SUBST(OCAML_LIB_PREFIX)
fi
AC_SUBST(HAVE_OCAMLC)

AC_CHECK_PROGS(OCAMLOPT, ocamlopt.opt ocamlopt, no)
if test "$OCAMLOPT" = no; then
	AC_MSG_WARN(ocaml native libraries won't be compiled since ocamlopt was not found)
	HAVE_OCAMLOPT=no
else
	HAVE_OCAMLOPT=yes
fi
AC_SUBST(HAVE_OCAMLOPT)

AM_CONDITIONAL(HAVE_OCAMLOPT_COND, test x$HAVE_OCAMLOPT = xyes)

AC_CHECK_PROG(HAVE_OCAMLDEP, ocamldep, yes, no)
if test $HAVE_OCAMLDEP = no; then
	AC_MSG_WARN(ocaml related modules won't be compiled since ocamldep was not found)
else
	OCAMLDEP=ocamldep
	AC_SUBST(OCAMLDEP)
fi

AC_CHECK_PROG(HAVE_OCAMLFIND, ocamlfind, yes, no)
if test $HAVE_OCAMLFIND = no; then
	AC_MSG_WARN(findlib not found. The META file will be installed anyway but /usr/lib/ocaml/ld.conf will not be updated automatically)
else
	OCAMLFIND=ocamlfind
	AC_SUBST(OCAMLFIND)
fi

if test "$GMETADOM_MODULES" = all; then
	GMETADOM_MODULES="gdome_cpp_smart"
	
	if test $HAVE_OCAMLC = yes -a $HAVE_OCAMLOPT = yes -a $HAVE_OCAMLDEP = yes; then
		GMETADOM_MODULES="$GMETADOM_MODULES gdome_caml"
	fi
fi
AC_SUBST(GMETADOM_MODULES)

AC_CONFIG_FILES([Makefile 
 xml/Makefile
 src/Makefile
 src/shared/Makefile
 src/shared/Char.hh.in
 src/gdome_cpp_smart/Makefile
 src/gdome_cpp_smart/xsl/Makefile
 src/gdome_cpp_smart/include/Makefile
 src/gdome_cpp_smart/basic/Makefile
 src/gdome_cpp_smart/core/Makefile
 src/gdome_cpp_smart/events/Makefile
 src/gdome_cpp_smart/test/Makefile
 src/gdome_caml/META
 src/gdome_caml/Makefile
 src/gdome_caml/xsl/Makefile
 src/gdome_caml/xml/Makefile
 src/gdome_caml/xml/Core/Makefile
 src/gdome_caml/xml/Events/Makefile
 src/gdome_caml/include/Makefile
 src/gdome_caml/types/Makefile
 src/gdome_caml/basic/Makefile
 src/gdome_caml/core/Makefile
 src/gdome_caml/events/Makefile
 src/gdome_caml/test/Makefile
 src/gdome_caml/ocaml/Makefile
 gdome2-cpp-smart.pc
 libgdome2.spec
])
dnl src/gdome_cpp_smart/doc/Makefile
dnl src/abstract_cpp_heap/Makefile
dnl src/abstract_cpp_heap/abstract_cpp_heap.conf
dnl src/abstract_cpp_heap/xsl/Makefile
dnl src/abstract_cpp_heap/include/Makefile
dnl src/abstract_cpp_heap/basic/Makefile
dnl src/abstract_gdome_cpp_heap/Makefile
dnl src/abstract_gdome_cpp_heap/xsl/Makefile
dnl src/abstract_gdome_cpp_heap/include/Makefile
dnl src/abstract_gdome_cpp_heap/core/Makefile
dnl AC_CONFIG_COMMANDS([default])
AC_OUTPUT

