
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <gdome-events.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_evnt_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeEvent* obj_ = Event_val(v);
  g_assert(obj_ != NULL);
  gdome_evnt_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_evnt_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeEvent* obj1_ = Event_val(v1);
  GdomeEvent* obj2_ = Event_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeEvent*
Event_val(value v)
{
  GdomeEvent* res_ = *((GdomeEvent**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_Event(GdomeEvent* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Events/Event",
    ml_gdome_evnt_finalize,
    ml_gdome_evnt_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeEvent*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeEvent**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_evnt_get_type(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_evnt_type(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_type");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_evnt_get_target(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeEventTarget* res_;
  
  res_ = gdome_evnt_target(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_target");
  g_assert(res_ != NULL);
  CAMLreturn(Val_EventTarget(res_));
}


value
ml_gdome_evnt_get_currentTarget(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeEventTarget* res_;
  
  res_ = gdome_evnt_currentTarget(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_currentTarget");
  g_assert(res_ != NULL);
  CAMLreturn(Val_EventTarget(res_));
}


value
ml_gdome_evnt_get_eventPhase(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  unsigned short res_;
  
  res_ = gdome_evnt_eventPhase(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_eventPhase");
  CAMLreturn(Val_int(res_));
}


value
ml_gdome_evnt_get_bubbles(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeBoolean res_;
  
  res_ = gdome_evnt_bubbles(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_bubbles");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_evnt_get_cancelable(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeBoolean res_;
  
  res_ = gdome_evnt_cancelable(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_cancelable");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_evnt_get_timeStamp(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMTimeStamp res_;
  
  res_ = gdome_evnt_timeStamp(Event_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Event.get_timeStamp");
  CAMLreturn(Val_long(res_));
}


value
ml_gdome_evnt_stopPropagation(value self)
{
  CAMLparam1(self);
  
  GdomeException exc_;
  gdome_evnt_stopPropagation(Event_val(self), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Event.stopPropagation");
  CAMLreturn(Val_unit);
}


value
ml_gdome_evnt_preventDefault(value self)
{
  CAMLparam1(self);
  
  GdomeException exc_;
  gdome_evnt_preventDefault(Event_val(self), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Event.preventDefault");
  CAMLreturn(Val_unit);
}


value
ml_gdome_evnt_initEvent(value self, value p_eventTypeArg, value p_canBubbleArg, value p_cancelableArg)
{
  CAMLparam4(self, p_eventTypeArg, p_canBubbleArg, p_cancelableArg);
  
  GdomeException exc_;
  
  GdomeDOMString* p_eventTypeArg_ = DOMString_val(p_eventTypeArg);gdome_evnt_initEvent(Event_val(self), p_eventTypeArg_, Bool_val(p_canBubbleArg), Bool_val(p_cancelableArg), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Event.initEvent");
  CAMLreturn(Val_unit);
}

