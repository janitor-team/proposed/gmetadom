(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *)


(********** Core Module: hand-written class definitions **********)

class domString (obj : TDOMString.t) =
 object (self)
  method as_DOMString = (obj :> TDOMString.t)
  method to_string = IDOMString.to_string obj
  method equals (o2 : domString) = IDOMString.equals obj o2#as_DOMString
 end
;;

let gdome_obj_eq o1 o2 = o1#equals o2;;
let (===) = gdome_obj_eq;;

class domImplementation obj =
 object
  method hasFeature ~feature ~version = IDOMImplementation.hasFeature ~this:obj ~feature:((feature : domString)#as_DOMString) ~version:((version : domString)#as_DOMString)
  method createDocumentType ~qualifiedName ~publicId ~systemId = let obj = IDOMImplementation.createDocumentType ~this:obj ~qualifiedName:((qualifiedName : domString)#as_DOMString) ~publicId:((publicId : domString)#as_DOMString) ~systemId:((systemId : domString)#as_DOMString) in new documentType obj
  method createDocument ~namespaceURI ~qualifiedName ~doctype:doctype = let obj = IDOMImplementation.createDocument ~this:obj ~namespaceURI:(match namespaceURI with None -> None | Some namespaceURI -> Some (namespaceURI : domString)#as_DOMString) ~qualifiedName:((qualifiedName : domString)#as_DOMString) ~doctype:(match doctype with None -> None | Some doctype -> Some ((doctype : documentType)#as_DocumentType)) in new document obj
  method createDocumentFromURI ~uri ?validatingMode ?keepEntities () = let obj = IDOMImplementation.createDocumentFromURI ~this:obj ~uri ?validatingMode ?keepEntities () in new document obj
  method createDocumentFromMemory ~doc ?validatingMode ?keepEntities () = new document (IDOMImplementation.createDocumentFromMemory ~this:obj ~doc:((doc : domString)#as_DOMString) ?validatingMode ?keepEntities ())
  method saveDocumentToFile ~(doc : document) =
   IDOMImplementation.saveDocumentToFile ~this:obj ~doc:((doc : document)#as_Document)
  method saveDocumentToFileEnc ~(doc : document) =
   IDOMImplementation.saveDocumentToFileEnc ~this:obj ~doc:((doc : document)#as_Document)
 method saveDocumentToMemory ~(doc : document) =
  IDOMImplementation.saveDocumentToMemory ~this:obj ~doc:((doc : document)#as_Document)
 method saveDocumentToMemoryEnc ~(doc : document) =
  IDOMImplementation.saveDocumentToMemoryEnc ~this:obj ~doc:((doc : document)#as_Document)
 method enableEvent ~doc ~name = IDOMImplementation.enableEvent ~this:obj ~doc:((doc : document)#as_Document) ~name
 method disableEvent ~doc ~name = IDOMImplementation.disableEvent ~this:obj ~doc:((doc : document)#as_Document) ~name
 method eventIsEnabled ~doc ~name = IDOMImplementation.eventIsEnabled ~this:obj ~doc:((doc : document)#as_Document) ~name
 end

(********** Events Module: generated class definitions **********)


and eventTarget (obj : TEventTarget.t) =
 object
  method as_EventTarget = obj
  method addEventListener ~typ ~listener ~useCapture = IEventTarget.addEventListener ~this:obj  ~typ:((typ : domString)#as_DOMString) ~listener:((listener : eventListener)#as_EventListener) ~useCapture
  method removeEventListener ~typ ~listener ~useCapture = IEventTarget.removeEventListener ~this:obj  ~typ:((typ : domString)#as_DOMString) ~listener:((listener : eventListener)#as_EventListener) ~useCapture
  method dispatchEvent ~evt = IEventTarget.dispatchEvent ~this:obj  ~evt:((evt : event)#as_Event)
 end

and event (obj : TEvent.t) =
 object
  method as_Event = obj
  method get_type = let obj = IEvent.get_type ~this:obj in new domString obj
  method get_target = let obj = IEvent.get_target ~this:obj in new eventTarget obj
  method get_currentTarget = let obj = IEvent.get_currentTarget ~this:obj in new eventTarget obj
  method get_eventPhase = IEvent.get_eventPhase ~this:obj
  method get_bubbles = IEvent.get_bubbles ~this:obj
  method get_cancelable = IEvent.get_cancelable ~this:obj
  method get_timeStamp = IEvent.get_timeStamp ~this:obj
  method stopPropagation = IEvent.stopPropagation ~this:obj 
  method preventDefault = IEvent.preventDefault ~this:obj 
  method initEvent ~eventTypeArg ~canBubbleArg ~cancelableArg = IEvent.initEvent ~this:obj  ~eventTypeArg:((eventTypeArg : domString)#as_DOMString) ~canBubbleArg ~cancelableArg
 end

and mutationEvent (obj : TMutationEvent.t) =
 object
  inherit (event (obj :> TEvent.t))
  method as_MutationEvent = obj
  method get_relatedNode = let obj = IMutationEvent.get_relatedNode ~this:obj in match obj with None -> None | Some obj -> Some (new node obj)
  method get_prevValue = let obj = IMutationEvent.get_prevValue ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_newValue = let obj = IMutationEvent.get_newValue ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_attrName = let obj = IMutationEvent.get_attrName ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_attrChange = IMutationEvent.get_attrChange ~this:obj
  method initMutationEvent ~typeArg ~canBubbleArg ~cancelableArg ~relatedNodeArg ~prevValueArg ~newValueArg ~attrNameArg ~attrChangeArg = IMutationEvent.initMutationEvent ~this:obj  ~typeArg:((typeArg : domString)#as_DOMString) ~canBubbleArg ~cancelableArg ~relatedNodeArg:(match relatedNodeArg with None -> None | Some relatedNodeArg -> Some (relatedNodeArg : node)#as_Node) ~prevValueArg:(match prevValueArg with None -> None | Some prevValueArg -> Some (prevValueArg : domString)#as_DOMString) ~newValueArg:(match newValueArg with None -> None | Some newValueArg -> Some (newValueArg : domString)#as_DOMString) ~attrNameArg:(match attrNameArg with None -> None | Some attrNameArg -> Some (attrNameArg : domString)#as_DOMString) ~attrChangeArg
 end

(********** Events Module: hand-written class definitions **********)

and eventListener obj =
 object
  method as_EventListener = (obj :> TEventListener.t)
 end

(********** Core Module: generated class definitions **********)


and node (obj : TNode.t) =
 object
  inherit (eventTarget (obj :> TEventTarget.t))
  method as_Node = obj
  method get_nodeName = let obj = INode.get_nodeName ~this:obj in new domString obj
  method get_nodeValue = let obj = INode.get_nodeValue ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method set_nodeValue ~value = INode.set_nodeValue ~this:obj ~value:(match value with None -> None | Some value -> Some (value : domString)#as_DOMString)
  method get_nodeType = INode.get_nodeType ~this:obj
  method get_parentNode = let obj = INode.get_parentNode ~this:obj in match obj with None -> None | Some obj -> Some (new node obj)
  method get_childNodes = let obj = INode.get_childNodes ~this:obj in new nodeList obj
  method get_firstChild = let obj = INode.get_firstChild ~this:obj in match obj with None -> None | Some obj -> Some (new node obj)
  method get_lastChild = let obj = INode.get_lastChild ~this:obj in match obj with None -> None | Some obj -> Some (new node obj)
  method get_previousSibling = let obj = INode.get_previousSibling ~this:obj in match obj with None -> None | Some obj -> Some (new node obj)
  method get_nextSibling = let obj = INode.get_nextSibling ~this:obj in match obj with None -> None | Some obj -> Some (new node obj)
  method get_attributes = let obj = INode.get_attributes ~this:obj in match obj with None -> None | Some obj -> Some (new namedNodeMap obj)
  method get_ownerDocument = let obj = INode.get_ownerDocument ~this:obj in match obj with None -> None | Some obj -> Some (new document obj)
  method get_namespaceURI = let obj = INode.get_namespaceURI ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_prefix = let obj = INode.get_prefix ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method set_prefix ~value = INode.set_prefix ~this:obj ~value:(match value with None -> None | Some value -> Some (value : domString)#as_DOMString)
  method get_localName = let obj = INode.get_localName ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)

  method isSameNode other = INode.isSameNode ~this:obj ((other : node)#as_Node)
  method equals other = INode.isSameNode ~this:obj ((other : node)#as_Node)
  method insertBefore ~newChild ~refChild = let obj = INode.insertBefore ~this:obj  ~newChild:((newChild : node)#as_Node) ~refChild:(match refChild with None -> None | Some refChild -> Some (refChild : node)#as_Node) in new node obj
  method replaceChild ~newChild ~oldChild = let obj = INode.replaceChild ~this:obj  ~newChild:((newChild : node)#as_Node) ~oldChild:((oldChild : node)#as_Node) in new node obj
  method removeChild ~oldChild = let obj = INode.removeChild ~this:obj  ~oldChild:((oldChild : node)#as_Node) in new node obj
  method appendChild ~newChild = let obj = INode.appendChild ~this:obj  ~newChild:((newChild : node)#as_Node) in new node obj
  method hasChildNodes = INode.hasChildNodes ~this:obj 
  method cloneNode ~deep = let obj = INode.cloneNode ~this:obj  ~deep in new node obj
  method normalize = INode.normalize ~this:obj 
  method isSupported ~feature ~version = INode.isSupported ~this:obj  ~feature:((feature : domString)#as_DOMString) ~version:((version : domString)#as_DOMString)
  method hasAttributes = INode.hasAttributes ~this:obj 
 end

and nodeList (obj : TNodeList.t) =
 object
  method as_NodeList = obj
  method get_length = INodeList.get_length ~this:obj
  method item ~index = let obj = INodeList.item ~this:obj  ~index in match obj with None -> None | Some obj -> Some (new node obj)
 end

and namedNodeMap (obj : TNamedNodeMap.t) =
 object
  method as_NamedNodeMap = obj
  method get_length = INamedNodeMap.get_length ~this:obj
  method getNamedItem ~name = let obj = INamedNodeMap.getNamedItem ~this:obj  ~name:((name : domString)#as_DOMString) in match obj with None -> None | Some obj -> Some (new node obj)
  method setNamedItem ~arg = let obj = INamedNodeMap.setNamedItem ~this:obj  ~arg:((arg : node)#as_Node) in match obj with None -> None | Some obj -> Some (new node obj)
  method removeNamedItem ~name = let obj = INamedNodeMap.removeNamedItem ~this:obj  ~name:((name : domString)#as_DOMString) in new node obj
  method item ~index = let obj = INamedNodeMap.item ~this:obj  ~index in match obj with None -> None | Some obj -> Some (new node obj)
  method getNamedItemNS ~namespaceURI ~localName = let obj = INamedNodeMap.getNamedItemNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString) in match obj with None -> None | Some obj -> Some (new node obj)
  method setNamedItemNS ~arg = let obj = INamedNodeMap.setNamedItemNS ~this:obj  ~arg:((arg : node)#as_Node) in match obj with None -> None | Some obj -> Some (new node obj)
  method removeNamedItemNS ~namespaceURI ~localName = let obj = INamedNodeMap.removeNamedItemNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString) in new node obj
 end

and characterData (obj : TCharacterData.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_CharacterData = obj
  method get_data = let obj = ICharacterData.get_data ~this:obj in new domString obj
  method set_data ~value = ICharacterData.set_data ~this:obj ~value:((value : domString)#as_DOMString)
  method get_length = ICharacterData.get_length ~this:obj
  method substringData ~offset ~count = let obj = ICharacterData.substringData ~this:obj  ~offset ~count in new domString obj
  method appendData ~arg = ICharacterData.appendData ~this:obj  ~arg:((arg : domString)#as_DOMString)
  method insertData ~offset ~arg = ICharacterData.insertData ~this:obj  ~offset ~arg:((arg : domString)#as_DOMString)
  method deleteData ~offset ~count = ICharacterData.deleteData ~this:obj  ~offset ~count
  method replaceData ~offset ~count ~arg = ICharacterData.replaceData ~this:obj  ~offset ~count ~arg:((arg : domString)#as_DOMString)
 end

and attr (obj : TAttr.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_Attr = obj
  method get_name = let obj = IAttr.get_name ~this:obj in new domString obj
  method get_specified = IAttr.get_specified ~this:obj
  method get_value = let obj = IAttr.get_value ~this:obj in new domString obj
  method set_value ~value = IAttr.set_value ~this:obj ~value:((value : domString)#as_DOMString)
  method get_ownerElement = let obj = IAttr.get_ownerElement ~this:obj in match obj with None -> None | Some obj -> Some (new element obj)
 end

and element (obj : TElement.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_Element = obj
  method get_tagName = let obj = IElement.get_tagName ~this:obj in new domString obj
  method getAttribute ~name = let obj = IElement.getAttribute ~this:obj  ~name:((name : domString)#as_DOMString) in new domString obj
  method setAttribute ~name ~value = IElement.setAttribute ~this:obj  ~name:((name : domString)#as_DOMString) ~value:((value : domString)#as_DOMString)
  method removeAttribute ~name = IElement.removeAttribute ~this:obj  ~name:((name : domString)#as_DOMString)
  method getAttributeNode ~name = let obj = IElement.getAttributeNode ~this:obj  ~name:((name : domString)#as_DOMString) in match obj with None -> None | Some obj -> Some (new attr obj)
  method setAttributeNode ~newAttr = let obj = IElement.setAttributeNode ~this:obj  ~newAttr:((newAttr : attr)#as_Attr) in new attr obj
  method removeAttributeNode ~oldAttr = let obj = IElement.removeAttributeNode ~this:obj  ~oldAttr:((oldAttr : attr)#as_Attr) in new attr obj
  method getElementsByTagName ~name = let obj = IElement.getElementsByTagName ~this:obj  ~name:((name : domString)#as_DOMString) in new nodeList obj
  method getAttributeNS ~namespaceURI ~localName = let obj = IElement.getAttributeNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString) in new domString obj
  method setAttributeNS ~namespaceURI ~qualifiedName ~value = IElement.setAttributeNS ~this:obj  ~namespaceURI:(match namespaceURI with None -> None | Some namespaceURI -> Some (namespaceURI : domString)#as_DOMString) ~qualifiedName:((qualifiedName : domString)#as_DOMString) ~value:((value : domString)#as_DOMString)
  method removeAttributeNS ~namespaceURI ~localName = IElement.removeAttributeNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString)
  method getAttributeNodeNS ~namespaceURI ~localName = let obj = IElement.getAttributeNodeNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString) in match obj with None -> None | Some obj -> Some (new attr obj)
  method setAttributeNodeNS ~newAttr = let obj = IElement.setAttributeNodeNS ~this:obj  ~newAttr:((newAttr : attr)#as_Attr) in new attr obj
  method getElementsByTagNameNS ~namespaceURI ~localName = let obj = IElement.getElementsByTagNameNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString) in new nodeList obj
  method hasAttribute ~name = IElement.hasAttribute ~this:obj  ~name:((name : domString)#as_DOMString)
  method hasAttributeNS ~namespaceURI ~localName = IElement.hasAttributeNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString)
 end

and text (obj : TText.t) =
 object
  inherit (characterData (obj :> TCharacterData.t))
  method as_Text = obj
  method splitText ~offset = let obj = IText.splitText ~this:obj  ~offset in new text obj
 end

and comment (obj : TComment.t) =
 object
  inherit (characterData (obj :> TCharacterData.t))
  method as_Comment = obj
 end

and cdataSection (obj : TCDATASection.t) =
 object
  inherit (text (obj :> TText.t))
  method as_CDATASection = obj
 end

and documentType (obj : TDocumentType.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_DocumentType = obj
  method get_name = let obj = IDocumentType.get_name ~this:obj in new domString obj
  method get_entities = let obj = IDocumentType.get_entities ~this:obj in new namedNodeMap obj
  method get_notations = let obj = IDocumentType.get_notations ~this:obj in new namedNodeMap obj
  method get_publicId = let obj = IDocumentType.get_publicId ~this:obj in new domString obj
  method get_systemId = let obj = IDocumentType.get_systemId ~this:obj in new domString obj
  method get_internalSubset = let obj = IDocumentType.get_internalSubset ~this:obj in new domString obj
 end

and notation (obj : TNotation.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_Notation = obj
  method get_publicId = let obj = INotation.get_publicId ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_systemId = let obj = INotation.get_systemId ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
 end

and entity (obj : TEntity.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_Entity = obj
  method get_publicId = let obj = IEntity.get_publicId ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_systemId = let obj = IEntity.get_systemId ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
  method get_notationName = let obj = IEntity.get_notationName ~this:obj in match obj with None -> None | Some obj -> Some (new domString obj)
 end

and entityReference (obj : TEntityReference.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_EntityReference = obj
 end

and processingInstruction (obj : TProcessingInstruction.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_ProcessingInstruction = obj
  method get_target = let obj = IProcessingInstruction.get_target ~this:obj in new domString obj
  method get_data = let obj = IProcessingInstruction.get_data ~this:obj in new domString obj
  method set_data ~value = IProcessingInstruction.set_data ~this:obj ~value:((value : domString)#as_DOMString)
 end

and documentFragment (obj : TDocumentFragment.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_DocumentFragment = obj
 end

and document (obj : TDocument.t) =
 object
  inherit (node (obj :> TNode.t))
  method as_Document = obj
  method get_doctype = let obj = IDocument.get_doctype ~this:obj in match obj with None -> None | Some obj -> Some (new documentType obj)
  method get_implementation = let obj = IDocument.get_implementation ~this:obj in new domImplementation obj
  method get_documentElement = let obj = IDocument.get_documentElement ~this:obj in new element obj
  method createElement ~tagName = let obj = IDocument.createElement ~this:obj  ~tagName:((tagName : domString)#as_DOMString) in new element obj
  method createDocumentFragment = let obj = IDocument.createDocumentFragment ~this:obj  in new documentFragment obj
  method createTextNode ~data = let obj = IDocument.createTextNode ~this:obj  ~data:((data : domString)#as_DOMString) in new text obj
  method createComment ~data = let obj = IDocument.createComment ~this:obj  ~data:((data : domString)#as_DOMString) in new comment obj
  method createCDATASection ~data = let obj = IDocument.createCDATASection ~this:obj  ~data:((data : domString)#as_DOMString) in new cdataSection obj
  method createProcessingInstruction ~target ~data = let obj = IDocument.createProcessingInstruction ~this:obj  ~target:((target : domString)#as_DOMString) ~data:((data : domString)#as_DOMString) in new processingInstruction obj
  method createAttribute ~name = let obj = IDocument.createAttribute ~this:obj  ~name:((name : domString)#as_DOMString) in new attr obj
  method createEntityReference ~name = let obj = IDocument.createEntityReference ~this:obj  ~name:((name : domString)#as_DOMString) in new entityReference obj
  method getElementsByTagName ~tagname = let obj = IDocument.getElementsByTagName ~this:obj  ~tagname:((tagname : domString)#as_DOMString) in new nodeList obj
  method importNode ~importedNode ~deep = let obj = IDocument.importNode ~this:obj  ~importedNode:((importedNode : node)#as_Node) ~deep in new node obj
  method createElementNS ~namespaceURI ~qualifiedName = let obj = IDocument.createElementNS ~this:obj  ~namespaceURI:(match namespaceURI with None -> None | Some namespaceURI -> Some (namespaceURI : domString)#as_DOMString) ~qualifiedName:((qualifiedName : domString)#as_DOMString) in new element obj
  method createAttributeNS ~namespaceURI ~qualifiedName = let obj = IDocument.createAttributeNS ~this:obj  ~namespaceURI:(match namespaceURI with None -> None | Some namespaceURI -> Some (namespaceURI : domString)#as_DOMString) ~qualifiedName:((qualifiedName : domString)#as_DOMString) in new attr obj
  method getElementsByTagNameNS ~namespaceURI ~localName = let obj = IDocument.getElementsByTagNameNS ~this:obj  ~namespaceURI:((namespaceURI : domString)#as_DOMString) ~localName:((localName : domString)#as_DOMString) in new nodeList obj
  method getElementById ~elementId = let obj = IDocument.getElementById ~this:obj  ~elementId:((elementId : domString)#as_DOMString) in match obj with None -> None | Some obj -> Some (new element obj)
 end
;;

(********** Core Module: hand-written pseudo-constructors  **********)

let domString str = new domString (IDOMString.of_string str);;

let domImplementation () = new domImplementation (IDOMImplementation.create ());;

(********** Events Module: hand-written pseudo-constructors  **********)

let eventListener ~callback =
 let callback' =
  function event -> callback (new event event)
 in
  new eventListener (IEventListener.create ~callback:callback')
;;
 
(********** Events Module: generated dynamic down-casts **********)

class mutationEvent_of_event obj =
 mutationEvent (IMutationEvent.of_Event obj#as_Event)
;;
 
(********** Core Module: generated dynamic down-casts **********)

class characterData_of_node obj =
 characterData (ICharacterData.of_Node obj#as_Node)
;;

class attr_of_node obj =
 attr (IAttr.of_Node obj#as_Node)
;;

class element_of_node obj =
 element (IElement.of_Node obj#as_Node)
;;

class text_of_node obj =
 text (IText.of_Node obj#as_Node)
;;

class comment_of_node obj =
 comment (IComment.of_Node obj#as_Node)
;;

class cdataSection_of_node obj =
 cdataSection (ICDATASection.of_Node obj#as_Node)
;;

class documentType_of_node obj =
 documentType (IDocumentType.of_Node obj#as_Node)
;;

class notation_of_node obj =
 notation (INotation.of_Node obj#as_Node)
;;

class entity_of_node obj =
 entity (IEntity.of_Node obj#as_Node)
;;

class entityReference_of_node obj =
 entityReference (IEntityReference.of_Node obj#as_Node)
;;

class processingInstruction_of_node obj =
 processingInstruction (IProcessingInstruction.of_Node obj#as_Node)
;;

class documentFragment_of_node obj =
 documentFragment (IDocumentFragment.of_Node obj#as_Node)
;;

class document_of_node obj =
 document (IDocument.of_Node obj#as_Node)
;;
