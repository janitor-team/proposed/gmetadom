
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_a_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeAttr* obj_ = Attr_val(v);
  g_assert(obj_ != NULL);
  gdome_a_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_a_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeAttr* obj1_ = Attr_val(v1);
  GdomeAttr* obj2_ = Attr_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeAttr*
Attr_val(value v)
{
  GdomeAttr* res_ = *((GdomeAttr**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_Attr(GdomeAttr* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/Attr",
    ml_gdome_a_finalize,
    ml_gdome_a_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeAttr*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeAttr**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_a_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeAttr* obj_;
  
  obj_ = gdome_cast_a((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("Attr");
  gdome_a_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "Attr casting from Node");
  CAMLreturn(Val_Attr(obj_));
}

    
value
ml_gdome_a_get_name(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_a_name(Attr_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Attr.get_name");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_a_get_specified(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeBoolean res_;
  
  res_ = gdome_a_specified(Attr_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Attr.get_specified");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_a_get_value(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_a_value(Attr_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Attr.get_value");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}

value
ml_gdome_a_set_value(value self, value p_v)
{
  CAMLparam2(self, p_v);
  GdomeException exc_;
  
  GdomeDOMString* p_v_ = DOMString_val(p_v);
  gdome_a_set_value(Attr_val(self), p_v_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Attr.set_value");
  CAMLreturn(Val_unit);
}


value
ml_gdome_a_get_ownerElement(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeElement* res_;
  
  res_ = gdome_a_ownerElement(Attr_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Attr.get_ownerElement");
  CAMLreturn(Val_option_ptr(res_,Val_Element));
}

