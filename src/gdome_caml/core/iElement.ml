
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external of_Node : [> `Node] GdomeT.t -> TElement.t = "ml_gdome_el_of_n"

  
external get_tagName : this:[> `Element] GdomeT.t -> TDOMString.t = "ml_gdome_el_get_tagName"


external getAttribute : this:[> `Element] GdomeT.t -> name:TDOMString.t -> TDOMString.t = "ml_gdome_el_getAttribute"
external setAttribute : this:[> `Element] GdomeT.t -> name:TDOMString.t -> value:TDOMString.t -> unit = "ml_gdome_el_setAttribute"
external removeAttribute : this:[> `Element] GdomeT.t -> name:TDOMString.t -> unit = "ml_gdome_el_removeAttribute"
external getAttributeNode : this:[> `Element] GdomeT.t -> name:TDOMString.t -> TAttr.t option = "ml_gdome_el_getAttributeNode"
external setAttributeNode : this:[> `Element] GdomeT.t -> newAttr:[> `Attr] GdomeT.t -> TAttr.t = "ml_gdome_el_setAttributeNode"
external removeAttributeNode : this:[> `Element] GdomeT.t -> oldAttr:[> `Attr] GdomeT.t -> TAttr.t = "ml_gdome_el_removeAttributeNode"
external getElementsByTagName : this:[> `Element] GdomeT.t -> name:TDOMString.t -> TNodeList.t = "ml_gdome_el_getElementsByTagName"
external getAttributeNS : this:[> `Element] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> TDOMString.t = "ml_gdome_el_getAttributeNS"
external setAttributeNS : this:[> `Element] GdomeT.t -> namespaceURI:TDOMString.t option -> qualifiedName:TDOMString.t -> value:TDOMString.t -> unit = "ml_gdome_el_setAttributeNS"
external removeAttributeNS : this:[> `Element] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> unit = "ml_gdome_el_removeAttributeNS"
external getAttributeNodeNS : this:[> `Element] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> TAttr.t option = "ml_gdome_el_getAttributeNodeNS"
external setAttributeNodeNS : this:[> `Element] GdomeT.t -> newAttr:[> `Attr] GdomeT.t -> TAttr.t = "ml_gdome_el_setAttributeNodeNS"
external getElementsByTagNameNS : this:[> `Element] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> TNodeList.t = "ml_gdome_el_getElementsByTagNameNS"
external hasAttribute : this:[> `Element] GdomeT.t -> name:TDOMString.t -> bool = "ml_gdome_el_hasAttribute"
external hasAttributeNS : this:[> `Element] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> bool = "ml_gdome_el_hasAttributeNS"