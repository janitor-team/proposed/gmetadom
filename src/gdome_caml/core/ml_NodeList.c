
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_nl_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeNodeList* obj_ = NodeList_val(v);
  g_assert(obj_ != NULL);
  gdome_nl_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_nl_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeNodeList* obj1_ = NodeList_val(v1);
  GdomeNodeList* obj2_ = NodeList_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeNodeList*
NodeList_val(value v)
{
  GdomeNodeList* res_ = *((GdomeNodeList**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_NodeList(GdomeNodeList* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/NodeList",
    ml_gdome_nl_finalize,
    ml_gdome_nl_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeNodeList*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeNodeList**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_nl_get_length(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  unsigned long res_;
  
  res_ = gdome_nl_length(NodeList_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "NodeList.get_length");
  CAMLreturn(Val_int(res_));
}


value
ml_gdome_nl_item(value self, value p_index)
{
  CAMLparam2(self, p_index);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_nl_item(NodeList_val(self), Int_val(p_index), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NodeList.item");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}

