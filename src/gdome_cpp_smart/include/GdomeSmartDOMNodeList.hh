
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMNodeList_hh__
#define __GdomeSmartDOMNodeList_hh__

namespace GdomeSmartDOM {

class NodeList
{
protected:
  explicit NodeList(GdomeNodeList* obj, bool) : gdome_obj(obj) { }
public:
  explicit NodeList(GdomeNodeList* = 0);
    NodeList(const NodeList&);
  ~NodeList();
NodeList& operator=(const NodeList&);
  bool operator==(const NodeList& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const NodeList& obj) const { return !(*this == obj); }
  
  operator GdomeNodeList*() const { return reinterpret_cast<GdomeNodeList*>(gdome_obj); }

  // Attributes  
  unsigned long get_length(void) const;
  
  // Methods
  class Node item(const unsigned long index) const;
  
  // Friend classes
  friend class Document;
  friend class Node;
  friend class Element;
  GdomeNodeList* gdome_object(void) const;
  
protected:
  GdomeNodeList* gdome_obj;
};

}

#endif // __GdomeSmartDOMNodeList_hh__

