
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

NodeList::NodeList(GdomeNodeList* obj)
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nl_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to NodeList failed");
  }
}

NodeList::NodeList(const NodeList& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nl_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "NodeList::NodeList casting from NodeList");
  }
}

NodeList& NodeList::operator=(const NodeList& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nl_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to NodeList failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nl_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to NodeList failed");
  }

  return *this;
}

NodeList::~NodeList()
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nl_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to NodeList failed");
    gdome_obj = 0;
  }

}


GdomeNodeList* NodeList::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nl_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "NodeList::gdome_object");
  }

  return gdome_obj;
}
unsigned long NodeList::get_length() const
{
  GdomeException exc_ = 0;
  unsigned long res_ = gdome_nl_length((GdomeNodeList*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "NodeList::get_length");
  return res_;
}

Node NodeList::item(const unsigned long index) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nl_item((GdomeNodeList*) gdome_obj, index, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NodeList::item");
  return res_;
}



}

